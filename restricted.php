<?php
session_name('tzLogin');
session_set_cookie_params(2*7*24*60*60);
session_start();
if(!isset($_SESSION['id']))
{
	header("Location: login.php");
	exit;
};
if($_SESSION['perms'] < 5)
{
	header("Location: login.php");
	exit;
};
?>


<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="ie ie6 lte9 lte8 lte7 no-js"> <![endif]-->
<!--[if IE 7]>     <html class="ie ie7 lte9 lte8 lte7 no-js"> <![endif]-->
<!--[if IE 8]>     <html class="ie ie8 lte9 lte8 no-js">      <![endif]-->
<!--[if IE 9]>     <html class="ie ie9 lte9 no-js">           <![endif]-->
<!--[if gt IE 9]>  <html class="no-js">                       <![endif]-->
<!--[if !IE]><!--> <html class="no-js">                       <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>ELITE - A Powerfull Responsive Admin Theme</title>
       
    <!-- // Mobile meta/files // -->

	<!-- For third-generation iPad with high-resolution Retina display: -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png">
    <!-- For iPhone 4with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/mobile/apple-touch-icon-114x114.png" />
    <!-- For first-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/mobile/apple-touch-icon-72x72.png" />
    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" href="images/mobile/apple-touch-icon.png" />
    <!-- For nokia devices: -->
    <link rel="shortcut icon" href="images/apple-touch-icon.png" />
    <!-- 320x460 for iPhone 3GS -->
    <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and not (-webkit-min-device-pixel-ratio: 2)" href="images/mobile/splash-320x460.png" />
    <!-- 640x920 for retina display -->
    <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" href="images/mobile/splash-640x920-retina.png" />
    <!-- iPad Portrait 768x1004 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 768px) and (orientation: portrait)" href="images/mobile/splash-768x1004.png" />
    <!-- iPad Landscape 1024x748 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 768px) and (orientation: landscape)" href="images/mobile/splash-1024x748.png" />
    <!-- iPad 3 Portrait 1536x2008 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 1536px) and (orientation: portrait)" href="images/mobile/splash-1536x2008-retina.png" />
    <!-- iPad 3 Landscape 2048x1536 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 2048px) and (orientation: landscape)" href="images/mobile/splash-2048x1496-retina.png" />
    <!-- Transform to webapp: -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Fullscreen mode: -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Viewport for older phones - http://davidbcalhoun.com/tag/handheldfriendly -->
    <meta name="HandheldFriendly" content="true"/>   
    <!-- Viewport - http://davidbcalhoun.com/tag/handheldfriendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> 
    <!-- This file contains some fixes, splash screen and web app code --> 
    <script src="js/mobiledevices.js"></script>
    
    <!-- // Internet Explore // -->
    
    <!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
    <meta name="application-name" content="Elite Admin Skin">
    <meta name="msapplication-tooltip" content="Cross-platform admin skin.">
    <meta name="msapplication-starturl" content="http://themes.creativemilk.net/elite/html/index.php">
    <!-- These custom tasks are examples, you need to edit them to show actual pages -->
    <meta name="msapplication-task" content="name=Home;action-uri=http://themes.creativemilk.net/elite/html/index.php;icon-uri=http://themes.creativemilk.net/elite/html/images/favicons/favicon.ico">
    <meta http-equiv="cleartype" content="on" /> 
    
    <!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->
            
    <!-- // Stylesheets // -->

    <!-- Framework -->
    <link rel="stylesheet" href="css/framework.css"/>
    <!-- Main -->
    <link rel="stylesheet" href="css/style.css"/>
    <!-- jQuery UI --> 
    <link rel="stylesheet" href="css/ui/jquery.ui.base.css"/>
    <!-- Styling -->
    <link rel="stylesheet" href="css/theme/darkblue.css" id="themesheet"/>
	<!--[if IE 7]>
	<link rel="stylesheet" href="css/destroy-ie6-ie7.css"/>
    <![endif]-->  
        <style type="text/css">
    	field3Sub1 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 16px;
	color: #000;
			}

		field3Sub2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	color: #000;
			}
		field3Sub2 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12pt;
	font-weight: normal;
	color: #000;
	margin: 0px;
	padding-top: 1px;
			}
    </style>
    
    
      
    <!-- // Misc // -->
    
    <link rel="shortcut icon" href="images/favicons/favicon.ico" />
    
    <!-- // jQuery/UI core // -->
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script>!window.jQuery && document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')</script>
    <script src="http://code.jquery.com/ui/1.8.22/jquery-ui.min.js"></script>
    <script>!window.jQueryUI && document.write('<script src="js/jquery-ui-1.8.22.min.js"><\/script>')</script>
    
    <!-- // Thirdparty plugins // -->
    
    <!-- Touch helper -->  
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <!-- MouseWheel -->  
    <script src="js/jquery.mousewheel.min.js"></script>
    <!-- UI Spinner -->
	<script src="js/jquery.ui.spinner.js"></script>
    <!-- Tooltip -->               
    <script src="js/tipsy.js"></script>
    <!-- Treeview -->                         
    <script src="js/treeview.js"></script>
    <!-- Calendar -->                         
    <script src="js/fullcalendar.min.js"></script> 
    <!-- selectToUISlider -->                
    <script src="js/selectToUISlider.jQuery.js"></script> 
    <!-- context Menu -->         
    <script src="js/jquery.contextMenu.js"></script> 
    <!-- File Explore -->              
    <script src="js/elfinder.min.js"></script> 
    <!-- AutoGrow Textarea -->                   
    <script src="js/autogrow-textarea.js"></script> 
    <!-- Resizable Textarea -->               
    <script src="js/textarearesizer.min.js"></script>
    <!-- HTML5 WYSIWYG -->  
    <script src="wysiwyghtml5/parser_rules/advanced.js"></script>
	<script src="wysiwyghtml5/dist/wysihtml5-0.3.0.js"></script>    
    <!-- Lightbox -->                      
    <script src="js/jquery.colorbox-min.js"></script>
    <!-- DataTables -->
    <script src="js/jquery.dataTables.min.js"></script>            
    <!-- Masked inputs -->
    <script src="js/jquery.maskedinput-1.3.min.js"></script> 
    <!-- IE7 JSON FIX -->
    <script src="js/json2.js"></script>
    <!-- HTML5 audio player -->
    <script src="audiojs/audiojs/audio.min.js"></script> 
             
    <!-- // Custom theme plugins // -->

    <!-- JQuery Cookie--> 
    <script src="js/jquery.cookie.js"></script> 
    
    <!-- Stylesheet switcher --> 
    <script src="js/e_styleswitcher.1.1.js"></script>                 
    <!-- Widgets -->
    <script src="js/powerwidgets.1.2.min.js"></script>
    <!-- Widgets panel -->
    <script src="js/powerwidgetspanel.1.2.min.js"></script>
    <!-- Select styling -->
    <script src="js/e_select.1.1.min.js"></script>    
    <!-- Checkbox solution -->
    <script src="js/e_checkbox.1.0.min.js"></script>
    <!-- Radio button replacement -->
    <script src="js/e_radio.1.0.min.js"></script>    
    <!-- Tabs -->
    <script src="js/e_tabs.1.1.min.js"></script>
    <!-- File styling -->
    <script src="js/e_file.1.0.min.js"></script>    
    <!-- MainMenu -->
    <script src="js/e_mainmenu.1.0.min.js"></script>
    <!-- Menu -->
    <script src="js/e_menu.1.1.min.js"></script>
    <!-- Input popup box -->
    <script src="js/e_inputexpand.1.0.min.js"></script>
    <!-- Progressbar -->
    <script src="js/e_progressbar.1.0.min.js"></script>
    <!-- Scrollbar replacemt -->
    <script src="js/e_scrollbar.1.0.min.js"></script> 
    <!-- Onscreen keyboard -->
    <script src="js/e_oskeyboard.1.0.min.js"></script>
    <!-- Textarea limiter -->
    <script src="js/e_textarealimiter.1.0.min.js"></script>
    <!-- Contact form with validation -->
    <script src="js/e_contactform.1.1.min.js"></script>
    <!-- Responsive table helper -->
    <script src="js/e_responsivetable.1.0.min.js"></script>
    <!-- Gallery -->
    <script src="js/e_gallery.1.0.min.js"></script>
    <!-- Live search -->
    <script src="js/e_livesearch.1.0.min.js"></script>
    <!-- Notify -->
    <script src="js/e_notify.1.0.min.js"></script>  
    <!-- Countdown -->  
    <script src="js/e_countdown.1.0.min.js"></script> 
    <!-- Clone script -->
    <script src="js/e_clone.1.0.min.js"></script> 
    <!-- Chained inputs -->
    <script src="js/e_chainedinputs.1.0.min.js"></script>
    <!-- Show password -->     
    <script src="js/e_showpassword.1.0.min.js"></script>        
    <!-- All plugins are set here -->
    <script src="js/plugins.js"></script>
    <!-- Custom code -->
    <script src="js/main.js"></script>
    
    <!-- // HTML5/CSS3 support // -->

    <script src="js/modernizr.min.js"></script>
    
    
    <!-- THEME CHECKER -->
    <script>
    	if ($.cookie('e_style')) {
  			$(function() {
    			$("#choose-styling").val($.cookie('e_style'));
			});
  			
  			$('#themesheet').attr('href', 'css/theme/'+$.cookie('e_style')+'.css');
		} else {
			$('#themesheet').attr('href', 'css/theme/strangeblue.css');
		}
    </script>
    
    
    
    
    
    
    
    
    
                
</head>
<body class="layout_fluid layout_responsive"> 

    <!-- this part can be removed, its just here 
         to let you switch between styles and layout sizes -->
    <div id="e-styleswitcher">
        <div class="e-styleswitcher-inner">
            <div class="e-styleswitcher-arrow"><img src="images/icons/plix-16/white/arrow-right-16.png" alt="" /></div>
            <div class="box">
            	<h4>Styles</h4>
                <select id="choose-styling">
                	<option value="strangeblue">Strange blue</option>
                    <option value="black">Black</option>
                    <option value="darkblue">Dark blue</option>
                    <option value="lightgrey">Light grey</option>
                </select>
            </div>    
            <div class="box">
            	<h4>Layout sizes</h4>                
                <select id="set-layout-size">
                	<option value="layout_fluid">fluid</option>
                    <option value="layout_768">768</option>
                    <option value="layout_960">960</option>
                    <option value="layout_1024">1024</option>
                    <option value="layout_1200">1200</option>
                    <option value="layout_1600">1600</option>
                </select>
            </div>                          
        </div>
    </div>
  
	<div id="container">
    
        <!-- MAIN HEADER -->
                
        <header id="header">
        	<div id="header-border">
                <div id="header-inner">
                
                    <div class="left">
                        <img class="logo_img"/>
                    </div><!-- End .left -->
                    
                    <div class="right">
                        <!-- eMenu -->
                        <nav>
                            <ul class="e-splitmenu" id="header-menu">
                                <li><span>Menu</span><a href="javascript:void(0);"><span class="arrow-down-10 plix-10"></span></a>
                                
                                     <div>
                                        <ul>
                                            <li><a href="index.php"><span class="stats2-10 plix-10"></span> Stats</a></li>
                                            <li><a href="index.php"><span class="lock-10 plix-10"></span> Security</a></li>
                                            <li><a href="index.php"><span class="download-10 plix-10"></span> Downloads</a></li>
                                        </ul>                                      
                                    </div>                               

                                </li>
                                <li><span>Settings</span><a href="javascript:void(0);"><span class="arrow-down-10 plix-10"></span></a>
                                
                                     <div>
                                        <ul>
                                            <li><a href="index.php"><span class="home-10 plix-10"></span> Basic Settings</a></li>
                                            <li><a href="index.php"><span class="settings-10 plix-10"></span> Site Settings</a></li>
                                            <li><a href="index.php"><span class="comment-10 plix-10"></span> User Settings</a></li>
                                            <li><a href="index.php"><span class="bookmark-10 plix-10"></span> Server Settings</a></li>
                                        </ul>                                      
                                    </div>
                                    
                                </li>
                        		<li class="e-menu-profile">
                                    <a href="javascript:void(0);"><span class="arrow-down-10 plix-10"></span></a> 
                                    <img src="images/avatar.jpg" alt=""/>
                                    
                                    <div>
                                        <ul>
                                            <li><a href="index.php"><span class="mail-10 plix-10"></span> Inbox</a></li>
                                            <li><a href="index.php"><span class="upload-10 plix-10"></span> Settings</a></li>
                                            <li><a href="login.php?logoff"><span class="info-10 plix-10"></span> Logout</a></li>
                                        </ul>                                      
                                    </div> 
                                            
                                </li>
                            </ul>
                        </nav>
                    </div><!-- End .right --> 
                    
                </div><!-- End #header-border --> 
            </div><!-- End #header-inner -->  
                
		</header><!-- End #header -->
        
        <!-- CONTENT -->
                 
        <div id="content">
            <div id="content-border">
            
                <!-- CONTENT HEADER -->
                
                <header id="content-header">
                    <div class="left">
                    	<a href="javascript:void(0);" id="toggle-mainmenu" class="button-icon tip-s" title="Toggle Main Menu"><span class="arrow-up-10 plix-10"></span></a>
                        
                        <!-- main search form -->
                        <form method="post" id="mainsearch">
                            <input type="text" placeholder="Live search..." name="" autocomplete="off"/>
                            <input type="submit" value="" />
                        </form>
                    </div><!-- End .left --> 
                    <div class="right">
                    	<!-- sidebar switch -->
                    	<a href="javascript:void(0);" id="toggle-sidebar" class="button-icon tip-s" title="Switch Main Menu"><span class="arrow-left-10 plix-10"></span></a>
                        
                        <!-- breadcrumbs -->
                        <nav id="main-breadcrumbs">
                            <ul>
                                <li class="bc-tab-first">
                                    <a href="index.php">Home</a>
                                </li>
                                <li class="bc-tab-last">Dashboard</li>
                            </ul>          
                        </nav>
                        
                        <!-- demo dialog button -->
                        <a href="javascript:void(0);" id="open-main-dialog" class="button-text-icon tip-w" title="Some tooltip pointing right"><span class="fullscreen-10 plix-10"></span> Dialog</a>
                        
                        <!-- the main page dialog -->
                        <div id="main-page-dialog" title="Welcome to Elite" style="display:none">
                        <img src="images/jquery-ui-logo.png" alt="" class="dummy-img-dialog"/>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sit amet est et mauris ornare lobortis dignissim quis massa. Donec ullamcorper turpis ac lectus semper commodo. Mauris tincidunt, erat et tempor interdum, dolor metus consectetur dui, eu accumsan augue augue a erat. Fusce justo nibh, tristique vitae pretium ut, venenatis nec felis. Curabitur congue tempor ultricies. Proin quis libero dignissim neque posuere pharetra vel adipiscing massa. Sed sit amet erat ac arcu sodales aliquam. Nam tellus sapien, ornare in tincidunt vel, ultricies id quam.
						</div>
                        
                        <span class="preloader"></span>
                        
                        <!-- widgets controls -->
                        <div id="widgets-controls">
                            <span class="preloader"></span>                       
                            <div class="icon-group"> 
                                <a href="javascript:void(0);" class="changeto-grid selected tip-s" title="Show grid"><span class="grid-10 plix-10"></span></a>
                                <span></span>
                                <a href="javascript:void(0);" class="changeto-rows tip-s" title="Show rows"><span class="rows-10 plix-10"></span></a>
                            </div>
                            
                            <!-- widgets management switch -->
                            <a href="javascript:void(0);" class="button-icon tip-s" title="Manage widgets" id="powerwidget-panel-switch"><span class="settings-10 plix-10"></span></a>
                        </div>
                    </div><!-- End .right -->                
                
				</header><!-- End #content-header --> 
                                
                <div id="content-inner">
                    
                    <!-- SIDEBAR -->
                   <?php include('sidebar.php'); ?>
                		<script>$("#restriced").addClass('page-active');</script>
                   	
                   
                     
                    <!-- CONTENT -->
                    
					<div id="content-main">
					
					
					
                        <div id="content-main-inner">
                        
							<?php include('restricted_content.html'); ?>
                            
                       </div><!-- End #content-main-inner --> 
                    </div><!-- End #content-main --> 
                </div><!-- End #content-inner --> 
                
                <!-- CONTENT FOOTER -->
                
                <footer id="content-footer">
                    <div class="left">
						<div class="left">
                               <a href="javascript:void(0);" class="button-icon tip-s" title="Some action">
                                  <span class="folder-10 plix-10"></span>
                              </a>                                                        
                              <a href="javascript:void(0);" class="button-icon tip-s" title="Some action">
                                  <span class="pencil-10 plix-10"></span>
                              </a>
                          </div><!-- End .left -->
                          <div class="right">
                              <a href="javascript:void(0);" class="button-icon tip-s" title="Some action">
                                  <span class="refresh-10 plix-10"></span>
                              </a> 
                          </div> <!-- End .right --> 
                    </div><!-- End .left --> 
                    <div class="right">
                    	<div class="left">
                    		
                        </div><!-- End .left -->
                        <div class="right">
                        	<div class="theme-version">Version 1.1</div>
                        </div><!-- End .right -->
                    </div><!-- End .right -->                
                </footer><!-- End #content-footer -->                 
            </div><!-- End #content-border --> 
        </div><!-- End #content --> 
            
    </div><!-- End #container -->
    
    <!-- scroll to top link -->
    <div id="scrolltotop"><span></span></div> 
    
</body>
</html>