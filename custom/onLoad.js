//////////////////////////////
// DEFINE FUNCTIONS TO RUN  //
//  AFER LOADING THE FORM	//
//  						//
//    MUST FOLLOW NAMING    //
//	 (FORM_NAME)_onLoad()	//
//////////////////////////////

function ca_request_onLoad(b) {
	$(".tipsy").remove();
	loadPOST('ca_request')
	setTimeout(function() {
		load_data(b); 
		currentId(b);
	}, 500);
};

function sp_errlog_onLoad(b) {
	$.ajax({
		type: "POST",
		url: "custom/sp_errlog/sperrlog_fetch_by_id.php",
		data: {
				'id':b, 
				},
		cache: false,
		success: function(echo)	{
				var jdata = jQuery.parseJSON(echo);
					$(".tipsy").remove();
					loadPOST('sp_errlog');
					if($jobList) {
						var tko = 500
					} else {
						var tko = 1000
					}
					setTimeout(function() {
						sperrlog_load(jdata[0]);
					}, tko);
			}
	});
};