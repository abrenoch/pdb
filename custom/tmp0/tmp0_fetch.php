<?php

   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/sql_params.php";
   include_once($path);
	
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, 'seps_check');//DB CRUDENTIALS	
	if ($mysqli->connect_error) {
		die('Connect Error (' . $mysqli->connect_errno . ') '
		. $mysqli->connect_error);
	}
	
	$dateFrom = date("Y-m-d", strtotime(mysql_real_escape_string($_POST['from'])));
	$dateTo = date("Y-m-d", strtotime(mysql_real_escape_string($_POST['to'])));
	
	$subquery = "SELECT  
				check_date as month,
				SUM(job_value) as job_total, 
				SUM(error_value) as error_total, 
				COUNT(*) as count_total, 
				SUM( IF (error_value =  0 ,1,0) ) as correct_total ,
				round((( SUM(job_value) ) - SUM(error_value) ) / SUM(job_value) , 2) AS sep_accuracy,
				round(100-((( COUNT(*) ) - SUM( IF (error_value =  0 ,1,0) ) ) / COUNT(*))*100 , 2) AS perc_without_errors
				FROM error_logs 
				WHERE sep_by IN ('danderson', 'mmorgan','dclark','tjenkins','bbowen','rkern','jflach','fbrown','jking','nwill','kphilippus')
				AND checked_by != '' AND `job_value` != 0.50 AND `job_value` != 5.00";

	if ($_POST['range'] == 'true') {
		$subquery .= "AND (`check_date` BETWEEN '".$dateFrom."' AND '".$dateTo."')";
	}
			
	$subquery .= 'GROUP BY YEAR(check_date), MONTH(check_date) ORDER BY check_date ASC';
	
	
	$query = $mysqli->query($subquery) or die(mysql_error());	//Query the database for the results we want
	
	if (mysqli_num_rows($query) > 0){
		while( $array[] = $query->fetch_object() );	//Create an array of objects for each returned row
		array_pop($array);	//Remove the blank entry at end of array
		$output = array();
		foreach($array as $option) :    
			$output[] = array("month" => $option->month, "count_total" => ($option->count_total + 0), "correct_total" => ($option->correct_total + 0), "job_total" => ($option->job_total + 0), "error_total" => ($option->error_total + 0), "sep_accuracy" => ($option->sep_accuracy * 100), "perc_without_errors" => ($option->perc_without_errors + 0) );	
		endforeach;	
		echo json_encode($output);	
	} else {
		die('ERROR: No results returned for the date range: '.$dateFrom.' - '.$dateTo.'. RANGE: '.$_POST['range']);
	}	

?>