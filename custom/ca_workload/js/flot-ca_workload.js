function ca_workload_load() { 
	var datasets = new Object();
    var fdate = $( "#flt_from_date").datepicker('getDate');
	var tdate = $( "#flt_to_date").datepicker('getDate');
	if(!tdate){
		var tdate = new Date(moment(fdate).add('d',7));
		//tdate.setDate(fdate.getDate()+7);
	};
	if(fdate > tdate){ERR_notify("'From' field should not be a later date than the 'To' field");return};
	var diff = new Date(tdate - fdate);
	var days_diff = diff/1000/60/60/24;
	days_diff = Math.ceil(days_diff * 10) / 10;
	
	mmTo = moment( tdate ).format("YYYY-MM-DD");
	mmFrom = moment( fdate ).format("YYYY-MM-DD");

	$.ajax({
    	type: "POST",
    	url: "custom/ca_workload/ca_flot_get.php",
    	data: {
                'user':"", 
                'to':mmTo,
                'from':mmFrom,
               // 'name':first+" "+last,
                },
    	cache: false,
    	success: function(echo)
        	{
        	
        	var obj = JSON.parse(echo);
        	
        	$("#chart-checkbox").find("input:checkbox:checked").prop('checked', false);    

        	if(obj.length <= 0) {	
        		$("#chart-checkbox").find("input:checkbox:not(:checked)").prop('checked', true);       
        		$("#chart-checkbox").find("input:checkbox:checked").each(function () {
					var eName = $(this).attr("name");
					obj.push( {
						user: eName,
						count: 0,
						date: mmFrom,
					}); 
				});
			}	

            var datasets = [];
            var oUsr = ''; 
              
            	for (i=0;i<obj.length;i++) {
            		//console.log(obj[i])
            		var userColor = '#'+intToARGB(hashCode(obj[i].user));
           			var oArray = {};
            			oArray.lines = { show: true, fill: 0.2};
            			oArray.data = [];
            				/////////SWAP THE LINES BELOW TO USE RANDOM COLOR OR RESULT-BASED COLORS
                		//oArray.color = get_random_color();  
                		oArray.color = userColor.substring(0,7);
            		
            		var Mdate = new Date(obj[i].date);
            			var diff = Mdate.getTimezoneOffset();
						Mdate = new Date(Mdate.getTime() + diff * 60 * 1000);
						
					//console.log("0-"+Mdate)
					
             			var diff = new Date(Mdate - fdate);
						var days_diff2 = diff/1000/60/60/24;
            			days_diff2 = Math.round(days_diff2);
					
					//IDENTIFIES NUMBER OF JOBS ASSIGNED FOR THE DATE
            		var integer = + obj[i].count;
            		
					//IDENTIFIES USER OF THESE RESULTS, 1 MAY CHANGE, THE OTHER WILL BE USED AS AN IDENTIFIER
					var cUsr = obj[i].user;
					var aUsr = obj[i].user;
					
            		$('#sel_artist option').each(function() { 	      			
            			if($(this).val()==obj[i].user){
            				cUsr = $(this).text();
            				$("#id"+obj[i].user).prop('checked', true);
            			} else {
            			
            			}           			
            		});
            		
            		var coords = [];
            		var skip = true;
            		
            		if(cUsr != oUsr){
            			oArray.label = cUsr;
            			oArray.data = coords; 
            			oArray.user = aUsr;     
            			skip = false;  
            			
            			for (k=0;k<days_diff+1;k++){
            				var Adate = new Date(moment(mmFrom).add('d',k));
								//Adate.setDate(fdate.getDate());
								Adate.setHours(0,0,0,0);
								
								//console.log("1-"+Adate)
								
            				if($.format.date(Adate, 'dd/MM/yyyy') == $.format.date(Mdate, 'dd/MM/yyyy')){
            					coords.push([Mdate,integer]); 
            					//console.log("2-"+Adate)
            				} else {
            					coords.push([Adate,0]);
            				}
            			}
            		} else {
            			for (j=0;j<datasets.length;j++){
							var a1 = datasets[j].label;
							if(a1==cUsr){	
								for (y=0;y<datasets[j].data.length;y++){
									var dchk = datasets[j].data[y][0];
									if ($.format.date(dchk, 'dd/MM/yyyy') == $.format.date(Mdate, 'dd/MM/yyyy')) {
										//alert("something happened")
										datasets[j].data[y] = [Mdate,integer]
										console.log("3-"+Mdate)
									}
								}
							}
            			}             			
            		}
                	oUsr = cUsr;
               	 	if(skip==false){datasets.push(oArray)}  
            	}
            	
            	
            	
            	
            		var eCoords = [];
            	
            	$("#chart-checkbox").find("input:checkbox:not(:checked)").each(function () {
					var eName = $(this).attr("fname");
					var eUserColor = '#'+intToARGB(hashCode($(this).attr("name")));
           			var eArray = {};
            			eArray.lines = { show: true };
            			eArray.data = [];
                		eArray.color = eUserColor.substring(0,7);
                		eArray.label = eName;
            			eArray.user = $(this).attr("name"); 
            			for (u=0;u<days_diff+1;u++){
            				var eDate = new Date();
								eDate.setDate(fdate.getDate()+u);
								eDate.setHours(0,0,0,0);					
								eCoords.push([eDate,0]);
            			};
            			eArray.data = eCoords; 
            			datasets.push(eArray);  		
            	});
               	//console.log(datasets)	
               	ca_workload_step2();
            	ca_workload_step3(datasets,fdate,tdate); 
        }
    });
}

///////FORMATTING FOR TOOL-TIP
function ca_workload_step2() {
        var tooltipto = "#flot_ca_workload0"
        function showTooltip(x, y, title, date, content) {
        $('<div id="flot-tooltip"><h3>'+ title+'</h3><p>' + content + '</p><h3>'+ date +'</h3><span><span></span></span></div>').css({
            top: y  -90,
            left: x -66
        }).appendTo("body").fadeIn(200);
    }
    var previousPoint = null;
    $(tooltipto).bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));
                if (item) {
                        if (previousPoint != item.dataIndex) {
                        	previousPoint = item.dataIndex;
                            $("#tooltip").remove();
                            var y = item.datapoint[1].toFixed(2);
                            var x = item.datapoint[0];
                            var xloc = $.format.date(x, 'MM/dd/yyyy')
                            if(parseInt(y)>0)showTooltip(item.pageX, item.pageY, item.series.label, xloc, parseInt(y)+" Assigned Job(s)");
                        }
                } else {
                	$("#flot-tooltip").remove();
                	previousPoint = null;            
                }
    });
};

///////RUNS FLOT PROCESS
function ca_workload_step3(datasets,fdate,tdate) { 
    var choiceContainer = $("#chart-checkbox");
        /* replace the class for an input element if you are not using the checkbox plugin */
    choiceContainer.find("div input").click(plotAccordingToChoices);
    
    function plotAccordingToChoices() {
        var data = [];
        choiceContainer.find("input:checked").each(function () {
            var key0 = $(this).attr("fname");
            //console.log(key0)
            for (x=0;x<datasets.length;x++){
            	var key1 = String(datasets[x].label);
            	if (key0 == key1) {
                	data.push(datasets[x]);
                }
            }   
    	});

        if (data.length > 0)
        	//var FRdate = new Date();
			//	FRdate.setDate(fdate.getDate()-1);
			//alert(FRdate)
			//fdate = fdate.setHours(0,0,0,0);
            $.plot($("#flot_ca_workload0"), data, {
                                series  :       { lines: { show: true }, points: { show: true }, curvedLines: { active: true } },
                                grid    :       { hoverable: true, clickable: true, autoHighlight: true},
                                legend  :       { show: true },
                                xaxis: { mode: "time",
          								timezone: "browser",
                                		minTickSize: [1, "day"],
                						min: fdate,
                						max: tdate
                						},
                				yaxis	:		{ min: 0, minTickSize: 1, tickDecimals: 0 }
            });
    }

    plotAccordingToChoices();

};

///////RUNS ACTION ON PPOINT CLICK
$("#flot_ca_workload0").bind("plotclick", function (event, pos, item) {
    //alert("You clicked at " + pos.x + ", " + pos.y);
    // axis coordinates for other axes, if present, are in pos.x2, pos.x3, ...
    // if you need global screen coordinates, they are pos.pageX, pos.pageY

    if (item) {
    	//console.log(item)
    	var user = item.series.user;
    		$('#sel_artist').val(user);
    	var x = item.datapoint[0];
        var xloc = $.format.date(x, 'MM/dd/yyyy');
        	$("#cur_date").datepicker('setDate', xloc);
        	artstChng('sel')
        //highlight(item.series, item.datapoint);
        //alert("You clicked "+xloc);
    }
});

///////GENERATES RANDOM HEX COLOR
function get_random_color() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}

function hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
       hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
} 

function intToARGB(i){
    return ((i>>24)&0xFF).toString(16) + 
           ((i>>16)&0xFF).toString(16) + 
           ((i>>8)&0xFF).toString(16) + 
           (i&0xFF).toString(16);
}