<?php

   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/sql_params.php";
   include_once($path);
	
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, 'pdb_users');//DB CRUDENTIALS	
	if ($mysqli->connect_error) {
		die('Connect Error (' . $mysqli->connect_errno . ') '
		. $mysqli->connect_error);
	}
	
	$query = $mysqli->query("SELECT usr,first_name,last_name,email,dep,avatar FROM `tz_members`");	//Query the database for the results we want
					
	while( $array[] = $query->fetch_object() );	//Create an array of objects for each returned row
	array_pop($array);	//Remove the blank entry at end of array
	$output = array();
    foreach($array as $option) :    
    	$output[] = array("user" => $option->usr, "first" => $option->first_name, "last" => $option->last_name, "email" => $option->email, "dep" => $option->dep, "avatar" => $option->avatar, );	
	endforeach;	
	echo json_encode($output);		

?>