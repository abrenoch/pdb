////////////
//
//	SETTINGS
//
//////
var singleUser = false;
//var sortBy = 'count_total';
//////

var data = {};
var toPush = {label:'Other Users', color:'#888', job_total:0, error_total:0, data:0};
var toPush2 = {};
var data2 = [];

function pieHover(event, pos, obj)	{
	var tpass = '';
	if($(event.currentTarget).attr('id') == 'flot_sp_statistics0') {
		tpass = '(Individual Jobs)';
	} else if ($(event.currentTarget).attr('id') == 'flot_sp_statistics1') {
		tpass = '(Job Values)';
	} else if ($(event.currentTarget).attr('id') == 'flot_sp_statistics2') {
		tpass = '(Error Values)';
	} else {
		return;
	}
	if (!obj) return;
	//if( $('#spstat_flot_type_view').val() == 'pie' ){
		//var percent = parseFloat(obj.series.percent).toFixed(2);
		//$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' - '+obj.series.data[0][1]+' ('+percent+'%)</span>');
	//} else {
		//console.log(obj)
	$("#hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' - '+obj.series.data[0][1]+' '+tpass+'</span>');
	//}
};

function pieClick(event, pos, obj) {
	if (!obj) return;
	if ($("#spstats_info_0 h2").text() == (obj.series.label+"'s Statistics") && $("#spstats_info_0 i").text() == $("#spstats_info_1 i").text()) return;
	$('#spstats_info_0').parent().slideUp('normal',function(){
		var a,b,c,f,usr;
		var d = 0;
		var e = 0;
		var g = 0;

		jQuery.grep(data2, function(obj1,loc) {
			d = d + obj1.job_total;
			e = e + obj1.error_total;
			if ( obj1.user === obj.series.label || obj1.label === obj.series.label ) {
				a = obj1.job_total;
				b = obj1.error_total;
				c = (b / a);
				f = obj1.count_total;
				g = obj1.correct_total;
				usr = obj1.user;
				console.log('FORM / sp_statistics: Loading user data for '+usr+'..');
			}
		});
		var calc = 100-(c*100);
		
		percent = parseFloat(obj.series.percent).toFixed(2);
		
		$("#spstats_info_0 h2").empty().text(obj.series.label + "'s Statistics");
		$("#spstats_info_0 h2").css('color',obj.series.color);
		$("#spstats_info_more center h2").css('color',obj.series.color);
		$("#spstats_info_0").parent().children('hr').css('background-color',obj.series.color);
		$("#spstats_info_0").parent().children('div.numHolder_t').css('border-color',obj.series.color);
		$('#spstats_info_more hr').css('background-color',obj.series.color);
		
		var grn = (calc.toFixed(2) / (100 / 255));
		grn = grn.toFixed(0);
		var red = (255 - grn)
				
		var pColor = '#'+rgbToHex(red,grn,0);
			pColor = pColor.substring(0,7);
			
			
		var Gtotal = ((g / f) * 100);
			if (!Gtotal) Gtotal = 0;				
			
		grn = (Gtotal.toFixed(2) / (100 / 255));
		grn = grn.toFixed(0);
		red = (255 - grn)			
			
		var gColor = '#'+rgbToHex(red,grn,0);
			gColor = gColor.substring(0,7);
		
		var Dtotal = ((a / d) * 100);
			if (!Dtotal) Etotal = 0;
		var Etotal = ((b / e) * 100);
			if (!Etotal) Etotal = 0;
	
		
		$('#spstats-bar0').empty().html('<div class="e-progressbar e-progressbar-small" data-progressbar-threshold=15 data-progressbar-value="'+calc.toFixed(2)+'" data-progressbar-color="'+pColor+'"></div>');
			$('#spstats-bar0').eProgressbar();
				$('#spstats-bar0 div.e-progressbar span span').css('color','#222');
				
		$('#spstats-bar1').empty().html('<div class="e-progressbar e-progressbar-small" data-progressbar-threshold=15 data-progressbar-value="'+Dtotal.toFixed(2)+'" data-progressbar-color="blue"></div>');
			$('#spstats-bar1').eProgressbar();
			
		$('#spstats-bar2').empty().html('<div class="e-progressbar e-progressbar-small" data-progressbar-threshold=15 data-progressbar-value="'+Etotal.toFixed(2)+'" data-progressbar-color="Darkorange"></div>');
			$('#spstats-bar2').eProgressbar();
		
		$('#spstats-bar3').empty().html('<div class="e-progressbar e-progressbar-small" data-progressbar-threshold=15 data-progressbar-value="'+Gtotal.toFixed(2)+'" data-progressbar-color="'+gColor+'"></div>');
			$('#spstats-bar3').eProgressbar();		
				$('#spstats-bar3 div.e-progressbar span span').css('color','#222');
		
		if( $('#spstat_shwDate').is(":checked") ){ 
			$('#spstats_info_0 i').remove();
			$('#spstats_info_0').append('<i style="opacity:0.4">'+moment($( "#spstat_from_date").datepicker('getDate')).format("MMMM Do, YYYY")+' - '+moment($( "#spstat_to_date").datepicker('getDate')).format("MMMM Do, YYYY")+'<i/>') 
		} else {
			$('#spstats_info_0 i').remove();
		}
		
		$('#spstat_info_1').text(f);//qntty
		$('#spstat_info_2').text(a);//ttl job val
		$('#spstat_info_3').text(b);//ttl err
		$('#spstat_info_4').text(g);//ttl crrct
		$('#spstat_info_5').text(f - g);
		$('#spstat_info_6').text(a - b);
		
	
		var rnge = false; 
		var mmTo = '';
		var mmFrom = '';	
		
		if( $('#spstat_shwDate').is(":checked") ){ 
			rnge = true; 
			var fdate = $( "#spstat_from_date").datepicker('getDate');
			var tdate = $( "#spstat_to_date").datepicker('getDate');		
			if(tdate){mmTo = moment( tdate ).format("YYYY-MM-DD");} else {ERR_notify("'To' date required to filter results by date range");return;}
			if(fdate){mmFrom = moment( fdate ).format("YYYY-MM-DD");} else {ERR_notify("'From' date required to filter results by date range");return;}
			if(fdate > tdate){ERR_notify("'From' field should not be a later date than the 'To' field");return};
		}
		
		if( $('#spstat_fltr0').is(":checked") ){ 
			var dspUPD = false;
		} else {
			var dspUPD = true;
		}
		if( $('#spstat_fltr1').is(":checked") ){ 
			var dspSMP = false;
		} else {
			var dspSMP = true;
		}

		$.ajax({
			type: "POST",
			url: "custom/sp_statistics/sp_statistics_fetch_more.php",
			data: {
				'f0':dspUPD,
				'f1':dspSMP,				
				'user':usr,
				'range':rnge,
				'to':mmTo,
				'from':mmFrom,
				},
			cache: false,
			success: function(echo) {
				if (echo.substring(0,3) == 'ERR') {
					$('#spstats_usr_errtable').empty().append('<tr><td>No Errors For That Range</td><td></td></tr>');
					$('#spstats_info_0').parent().slideDown('normal');
					return;
				};
				var data = '<thead style="font-size:larger"><td style="padding-bottom:5px"><b>Error Type</b></td><td><center><b>Count</b></center></td><td><center><b>Percentage</b></center></td></thead>';				
				var prc = 0;
				var dataLast = '';
				$.each(jQuery.parseJSON(echo), function(i) { prc += this.frequency});			
				$.each(jQuery.parseJSON(echo), function(i) {
					var etype = this.error_type;
					var frq = this.frequency;
					prcN = (this.frequency / prc);
					prcN = prcN * 100
					var i = 0;
					jQuery.grep($errList, function(obj1,loc) {
						if (obj1.id === etype) {
							if (obj1.indicator != 'N') {
								data += '<tr><td>'+obj1.error+'</td><td><center>'+frq+'</center></td><td><div class="usr_err_bars" ><div class="e-progressbar e-progressbar-small" data-progressbar-value='+prcN+' data-progressbar-threshold=50 data-progressbar-color="red"></div></td></tr>';
							} else {
								dataLast += '<tr><td>'+obj1.error+'</td><td><center>'+frq+'</center></td><td><div class="usr_err_bars" ><div class="e-progressbar e-progressbar-small" data-progressbar-value='+prcN+' data-progressbar-threshold=50 data-progressbar-color="orange"></div></td></tr>';
							}
						}
						i++
					});
				});
				data += '<tr><td colspan=3><hr style="background-color:white;"></td></tr>'+dataLast;
				$('#spstats_usr_errtable').empty().append(data);
				$(".usr_err_bars").eProgressbar({
					animate: false,
					showTotal: true,
				});
				$('#spstats_info_0').parent().slideDown('normal');
			}
		});
		
		
		
		
		
	});		
			
};

function sp_statistics_load() {
	
	data = {};
	toPush = {label:'Other Users', color:'#888', job_total:0, error_total:0, data:0};
	toPush2 = {};
	data2 = [];
	var rnge = false;
	var mmTo = '';
	var mmFrom = '';
	if( $('#spstat_shwDate').is(":checked") ){ 
		rnge = true; 
		var fdate = $( "#spstat_from_date").datepicker('getDate');
		var tdate = $( "#spstat_to_date").datepicker('getDate');		
		if(tdate){mmTo = moment( tdate ).format("YYYY-MM-DD");} else {ERR_notify("'To' date required to filter results by date range");return;}
		if(fdate){mmFrom = moment( fdate ).format("YYYY-MM-DD");} else {ERR_notify("'From' date required to filter results by date range");return;}
		if(fdate > tdate){ERR_notify("'From' field should not be a later date than the 'To' field");return};
	}
	if( $('#spstat_fltr0').is(":checked") ){ 
		var dspUPD = false;
	} else {
		var dspUPD = true;
	}
	if( $('#spstat_fltr1').is(":checked") ){ 
		var dspSMP = false;
	} else {
		var dspSMP = true;
	}
		
	var sortBy = $('#spstat_flot_type').val();
	
	console.log('FORM / sp_statistics: Loading chart data..');
	
	$.ajax({
		type: "POST",
		url: "custom/sp_statistics/sp_statistics_fetch.php",
		data: {
			'f0':dspUPD,
			'f1':dspSMP,
			'range':rnge,
            'to':mmTo,
            'from':mmFrom,
        	},
		cache: false,
		success: function(echo) {
			
			if (echo.substring(0,3) == 'ERR'){console.log('FORM / sp_statistics: '+echo); ERR_notify( echo.substring(7,echo.length),'' ); return; };
			
			data = jQuery.parseJSON(echo);
			
				$.each(jQuery.parseJSON(echo), function(i) {
					if (singleUser) {
						if(this.label === $crntUSR || this.user === $crntUSR){
							var newcolor = '#'+intToARGB(hashCode(this.label));
							if (newcolor.length < 7) newcolor = newcolor+"b";
							toPush2.user = this.label;
							toPush2.label = getUserFullName(this.label);			
							toPush2.color = newcolor.substring(0,7);
							toPush2.data = this[sortBy];
							toPush2.count_total = this.count_total;
							toPush2.error_total = this.error_total;
							toPush2.job_total = this.job_total;
							toPush2.max_cdate = this.max_cdate;
							toPush2.min_cdate = this.min_cdate;
							toPush2.crct_total = this.crct_total;
							delete data[i];
							data.push(toPush2)
						} else {
							toPush.data = (toPush.data + this.count_total);
							toPush.job_total = (toPush.job_total + this.job_total);
							toPush.error_total = (toPush.error_total + this.error_total);
							toPush.correct_total = (toPush.correct_total + this.correct_total);
							return delete data[i];
						}
					} else {
						var newcolor = '#'+intToARGB(hashCode(this.label));
						if (newcolor.length < 7) newcolor = newcolor+"b";
						this.user = this.label;
						this.label = getUserFullName(this.label);			
						this.color = newcolor.substring(0,7);
						this.data = this[sortBy];
						delete data[i];
						data.push(this)
					}
				});
				
				if (singleUser) {data.push(toPush)};
				var options ='';
				
					
				
				$.each(data, function(i) {
					if(this.label){ data2.push(this) }
				});
							
			
				if( $('#spstat_flot_type_view').val() == 'pie' ){
				
					options = {
						series: {
							pie: {
								show: true,
								innerRadius: .4,
								radius: 1,
								stroke: {
									color: '#000',
									width: 0.5
									},
								label: {
									show: false,
									radius: 2 / 3,
									formatter: function (label, series) {
										return '<div style="font-size:8pt;text-align:center;padding:2px;color:black; pointer-events:none"><b style="pointer-events:none">' + label + '</b>' + series.data[0][1] + '</div>';
									},
									threshold: 0.1
								},
								offset: {
									top: 0
								}
							}
						},
						legend: {
							show: true,
							position: 'nw',
							labelFormatter: function(label, series) {
								// series is the series object for the label
								//console.log(series)
								return '<p onMouseover=highlight(1,3) ">' + label + '</p>';
							}
						},
						grid: {
							hoverable: true,
							clickable: true
						},				
					};
					var plot = $.plot($("#flot_sp_statistics0"), data2, options);	
				} else {
					
					var d1, d2, d3, xaxisLabels = [], i=0;
					d1 = data2.map(function(elt){return {label: elt.label, color: elt.color, data: [[i++, elt.count_total]]};});
					i = 0;
					d2 = data2.map(function(elt){return {label: elt.label, color: elt.color, data: [[i++, elt.job_total]]};});
					i = 0;
					d3 = data2.map(function(elt){return {label: elt.label, color: elt.color, data: [[i++, elt.error_total]]};});
					
					i = 0;
					xaxisLabels = data2.map(function(elt) { return [i++, elt.label.split(" ")[0]]; });
						
					options = {
						series: {
						   bars: {
							 show       : true,
							 align      : 'center',
							 //dataLabels : true,
							 barWidth : 0.8
						   }
						 },
						xaxis: { ticks: xaxisLabels, tickLength:0 },
						yaxis: { ticks: 10 },
						legend: {
							show: false,
							position: 'ne',
							labelFormatter: function(label, series) {
								// series is the series object for the label
								//console.log(series)
								return '<p onMouseover=highlight(1,3) ">' + label + '</p>';
							}
						},
						grid: {
							hoverable: true,
							clickable: true
						},				
					};
					
					$.plot($("#flot_sp_statistics0"), d1, options);	
					$.plot($("#flot_sp_statistics1"), d2, options);	
					$.plot($("#flot_sp_statistics2"), d3, options);	
					
					$(window).resize(function() {
						$.plot($("#flot_sp_statistics0"), d1, options);	
						$.plot($("#flot_sp_statistics1"), d2, options);	
						$.plot($("#flot_sp_statistics2"), d3, options);	
					});
					
					var rnge = false; 
					var mmTo = '';
					var mmFrom = '';	
					
					if( $('#spstat_shwDate').is(":checked") ){ 
						rnge = true; 
						var fdate = $( "#spstat_from_date").datepicker('getDate');
						var tdate = $( "#spstat_to_date").datepicker('getDate');		
						if(tdate){mmTo = moment( tdate ).format("YYYY-MM-DD");} else {ERR_notify("'To' date required to filter results by date range");return;}
						if(fdate){mmFrom = moment( fdate ).format("YYYY-MM-DD");} else {ERR_notify("'From' date required to filter results by date range");return;}
						if(fdate > tdate){ERR_notify("'From' field should not be a later date than the 'To' field");return};
					}
					if( $('#spstat_fltr0').is(":checked") ){ 
						var dspUPD = false;
					} else {
						var dspUPD = true;
					}
					if( $('#spstat_fltr1').is(":checked") ){ 
						var dspSMP = false;
					} else {
						var dspSMP = true;
					}
					$.ajax({
						type: "POST",
						url: "custom/sp_statistics/sp_statistics_fetch_more2.php",
						data: {
							'f0':dspUPD,
							'f1':dspSMP,
							'range':rnge,
							'to':mmTo,
							'from':mmFrom,
							},
						cache: false,
						success: function(echo) {
							if (echo.substring(0,3) == 'ERR') {
								$('#spstats_dep_errtable').empty().append('<tr><td>No Errors For That Range</td><td></td></tr>');
								//$('#spstats_info_0').parent().slideDown('normal');
								return;
							};
							
							var data = '<thead style="font-size:larger"><td style="padding-bottom:5px"><b>Error Type</b></td><td><center><b>Count</b></center></td><td><center><b>Percentage</b></center></td></thead>';				
							var prc = 0;
							var dataLast = '';
							$.each(jQuery.parseJSON(echo), function(i) { prc += this.frequency});			
							$.each(jQuery.parseJSON(echo), function(i) {
								var etype = this.error_type;
								var frq = this.frequency;
								prcN = (this.frequency / prc);
								prcN = prcN * 100
								var i = 0;
								jQuery.grep($errList, function(obj1,loc) {
									if (obj1.id === etype) {
										if (obj1.indicator != 'N') {
											data += '<tr><td>'+obj1.error+'</td><td><center>'+frq+'</center></td><td><div class="dep_err_bars" ><div class="e-progressbar e-progressbar-small" data-progressbar-value='+prcN+' data-progressbar-threshold=50 data-progressbar-color="red"></div></td></tr>';
										} else {
											dataLast += '<tr><td>'+obj1.error+'</td><td><center>'+frq+'</center></td><td><div class="dep_err_bars" ><div class="e-progressbar e-progressbar-small" data-progressbar-value='+prcN+' data-progressbar-threshold=50 data-progressbar-color="orange"></div></td></tr>';
										}
									}
									i++
								});
							});
							data += '<tr><td colspan=3><hr style="background-color:white;"></td></tr>'+dataLast;
							$('#spstats_dep_errtable').empty().append(data);
							$(".dep_err_bars").eProgressbar({
								animate: false,
								showTotal: true,
							});
							//$('#spstats_info_0').parent().slideDown('normal');
						}
					});
					
					
					
				};		
			
			var a = 0;
			var b = 0;
			var g = 0;
			var f = 0;
				jQuery.grep(data2, function(obj1,loc) {
					a = a + obj1.job_total;
					b = b + obj1.error_total;
					g = g + obj1.count_total; 
					f = f + obj1.correct_total;
				});
			var c = (b / a);
				var calc = 100-(c*100);
			var h = (f / g);
				var calc2 = h*100;
				
			var grn = (calc.toFixed(2) / 0.39215686);
				grn = grn.toFixed(0);
			var red = (255 - grn)	
			var pColor = '#'+rgbToHex(red,grn,0);
				pColor = pColor.substring(0,7);
			
			grn = (calc2.toFixed(2) / (100 / 255));
			grn = grn.toFixed(0);
			red = (255 - grn)						
			var gColor = '#'+rgbToHex(red,grn,0);
				gColor = gColor.substring(0,7);

			$('#spstat_info_01').text(g);//qntty
			$('#spstat_info_04').text(f);//ttl crrct
			$('#spstat_info_02').text(a);//ttl job val
			$('#spstat_info_03').text(b);//ttl err
			$('#spstat_info_05').text(g - f);//ttl err
			$('#spstat_info_06').text(a - b);//ttl err
		

				
			if( $('#spstats-bar00 div.e-progressbar span span').text().split('%')[0] != calc.toFixed(2) ){
				$('#spstats-bar00').empty().html('<div class="e-progressbar e-progressbar-small" data-progressbar-value="'+calc.toFixed(2)+'" data-progressbar-threshold=0 data-progressbar-color="'+pColor+'"></div>');
					$('#spstats-bar00').eProgressbar();
						$('#spstats-bar00 div.e-progressbar span span').css('color','#222');
				
				$('#spstats-bar01').empty().html('<div class="e-progressbar e-progressbar-small" data-progressbar-value="'+calc2.toFixed(2)+'" data-progressbar-threshold=0 data-progressbar-color="'+gColor+'"></div>');
					$('#spstats-bar01').eProgressbar();
						$('#spstats-bar01 div.e-progressbar span span').css('color','#222');
				
						
			}
			
			if( $('#spstat_shwDate').is(":checked") ){ 
				$('#spstats_info_1 i').remove();
				$('#spstats_info_1').append('<i style="opacity:0.4">'+moment(fdate).format("MMMM Do, YYYY")+' - '+moment(tdate).format("MMMM Do, YYYY")+'<i/>') 
			} else {
				$('#spstats_info_1 i').remove();
			}

			/*		
			$('#spstats-bar01').empty().html('<div class="e-progressbar e-progressbar-small" data-progressbar-value="'+Dtotal.toFixed(2)+'" data-progressbar-color="blue"></div>');
				$('#spstats-bar1').eProgressbar();
				
			$('#spstats-bar02').empty().html('<div class="e-progressbar e-progressbar-small" data-progressbar-value="'+Etotal.toFixed(2)+'" data-progressbar-color="Darkorange"></div>');
				$('#spstats-bar2').eProgressbar();
			*/	
				

				
		}
	});
};




function findAndRemove(array, property, value) {
   $.each(array, function(index, result) {
      if(result[property] == value) {
          //Remove from array
          array.splice(index, 1);
      }    
   });
}

///////GENERATES RANDOM HEX COLOR
function get_random_color() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}

function hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
       hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
} 

function intToARGB(i){
    return ((i>>24)&0xFF).toString(16) + 
           ((i>>16)&0xFF).toString(16) + 
           ((i>>8)&0xFF).toString(16) + 
           (i&0xFF).toString(16);
}

function rgbToHex(R,G,B) {return toHex(R)+toHex(G)+toHex(B)};

function toHex(n) {
 n = parseInt(n,10);
 if (isNaN(n)) return "00";
 n = Math.max(0,Math.min(n,255));
 return "0123456789ABCDEF".charAt((n-n%16)/16)
      + "0123456789ABCDEF".charAt(n%16);
}