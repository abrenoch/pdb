<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/sql_params.php";
   include_once($path);

	
   /* ---------------------------------------------------- *
    * Misc settings                                        *
    * ---------------------------------------------------- *
	* These settings are some savety settings and some     *
	* text labels.                                         *
	* ---------------------------------------------------- */
	
	$noresultsfound  = 'No results returned'; // no results found text
	$readmore        = 'Read more...';         // read more text
	$maxresults      = 100;                 // maxium of results

   /* ---------------------------------------------------- *
    * Connect to the database                              *
    * ---------------------------------------------------- *
	* Create a connection to the MYSQL database. The       * 
	* settings can be found in the 'Connect settings'      * 
	* setion at the top.                                   *
	* ---------------------------------------------------- */
	
	mysql_connect(DB_HOST, DB_USER, DB_PASS) or die(mysql_error());

   /* ---------------------------------------------------- *
    * Select the right databse                             *
    * ---------------------------------------------------- *
	* Connect to the MYSQL database.                       *
	* ---------------------------------------------------- */
	
	mysql_select_db(DB_CAREQUEST_TABLE) or die(mysql_error());
	
	$dbtable = 'ca_request';       // database table name
	
   /* ---------------------------------------------------- *
    * jQuery plugin values and settings                    *
    * ---------------------------------------------------- *
	* This is where the values and settings are set as php *
	* variables. You can use up to 5 extra parameters,     *
	* which you can use as filters.                        *
	* ---------------------------------------------------- */
	
	$searchword = $_POST['value'];
	// $order      = $_POST['order'];
	$limit      = 3;
	$param1     = $_POST['param1'];
	//$param2     = $_POST['param2'];
	$param2     = $_POST['param2'];
	$param3     = $_POST['param3'];
	$param4     = $_POST['param4'];
	$param5     = $_POST['param5'];
	// $param6     = $_POST['param6'];
	// $param7     = $_POST['param7'];
	// $param8     = $_POST['param8'];
	// $param9     = $_POST['param9'];
	//$param10    = $_POST['param10'];
	$paramARRY    = $_POST['paramARRY'];
	
   /* ---------------------------------------------------- *
    * Display order                                        *
    * ---------------------------------------------------- *
	* Check and set the order, you can choose between,     *
	* ASC, DESC and random. Notice that it uses the column *
	* 'date' for the DESC and ASC order.                   *
	* ---------------------------------------------------- */
	
	//$param3 = strtoupper($param3);
	
	if($param3 == 'id'){
		$orderby = "id";

	}elseif($param3 == 'due_date'){
		$orderby = 'due_date';	

	}elseif($param3 == 'number'){
		$orderby = 't.new_art_concept_num';	
	
	}else{
		$orderby = 'id';
	}
	
	if($searchword == '') $searchword = '%%%';
	
   /* ---------------------------------------------------- *
    * Max results                                          *
    * ---------------------------------------------------- *
	* This is used as a savety filter, to prevent wrong    *
	* use of the plugin. The max has been set on 100, you  *
	* can change this in the 'Misc settings'.            *
	* ---------------------------------------------------- */
	
	if($limit <= $maxresults){
		$totalresults = $limit;
	}else{
		$totalresults = $maxresults;
	}
	
   /* ---------------------------------------------------- *
    * Database query                                       *
    * ---------------------------------------------------- *
	* Get all of the data from the database and put it in  * 
	* a array. We are going to loop the query, as we are   *
	* using the filter(this case a select element), this   *
	* isn't the best way to do this but we have to keep it *
	* simple.                                              *
	* ---------------------------------------------------- */

	
	$qryFormat = "SELECT t.* FROM 
					(SELECT `pid`, MAX(id) as id FROM ".$dbtable." GROUP BY `pid`) a  
					INNER JOIN ".$dbtable." t ON (t.id = a.id) ";
		
		if ($param1 != 'all'){
			$qryFormat .= "WHERE (t.".$param1." ".$param2." '%$searchword%')";
		} else {
			$qryFormat .= "WHERE (t.new_art_concept_num ".$param2." '%$searchword%' OR ";
			$qryFormat .= "t.name ".$param2." '%$searchword%' OR ";
			$qryFormat .= "t.customer ".$param2." '%$searchword%' OR ";
			$qryFormat .= "t.assign_to ".$param2." '%$searchword%' OR ";
			$qryFormat .= "t.cc_to ".$param2." '%$searchword%' OR ";
			$qryFormat .= "t.submitted_by ".$param2." '%$searchword%' OR ";
			$qryFormat .= "t.edit_by ".$param2." '%$searchword%' OR ";
			$qryFormat .= "t.project_name ".$param2." '%$searchword%')";
		}
	
	
	
										
	if(count($paramARRY)>0){
	//echo count($paramARRY);
		for ($i = 0; $i <= count($paramARRY)-1; $i++) {
    		if($paramARRY[$i][0]=='AND'){
    			$qryFormat .= " AND";
    		} else {
    			$qryFormat .= " OR ";
    		}
    		
    		$qryFormat .= " (";
    		
    		if ($paramARRY[$i][3] != 'all'){
				$qryFormat .= "t.".$paramARRY[$i][3];
				$qryFormat .= " ".$paramARRY[$i][2]." ";
				$qryFormat .= "'%".$paramARRY[$i][1]."%' ";
    		} else {
    			$qryFormat .= "t.new_art_concept_num ".$paramARRY[$i][2]." '%".$paramARRY[$i][1]."%' OR ";
				$qryFormat .= "t.name ".$paramARRY[$i][2]." '%".$paramARRY[$i][1]."%' OR ";
				$qryFormat .= "t.customer ".$paramARRY[$i][2]." '%".$paramARRY[$i][1]."%' OR ";
				$qryFormat .= "t.assign_to ".$paramARRY[$i][2]." '%".$paramARRY[$i][1]."%' OR ";
				$qryFormat .= "t.cc_to ".$paramARRY[$i][2]." '%".$paramARRY[$i][1]."%' OR ";
				$qryFormat .= "t.submitted_by ".$paramARRY[$i][2]." '%".$paramARRY[$i][1]."%' OR ";
				$qryFormat .= "t.edit_by ".$paramARRY[$i][2]." '%".$paramARRY[$i][1]."%' OR ";	
				$qryFormat .= "t.project_name ".$paramARRY[$i][2]." '%".$paramARRY[$i][1]."%'";	
    		}
    		$qryFormat .= ")";
    		//if($paramARRY[$i][0]=='AND'){
    			
    		//}
		};
	};

	$qryFormat .= " ORDER BY ".$orderby." ".$param5;	
							
	$qryFormat .= " LIMIT 0 , ".$maxresults;
	
	$query = mysql_query($qryFormat) or die(mysql_error());
					  
   /* ---------------------------------------------------- *
    * Output                                               **
    * ---------------------------------------------------- *
	* Get all found data from the database and wrap it in  * 
	* html tags.                                           *
	* ---------------------------------------------------- */
	
	// available values inside the loop
	// $results['id'];
	// $results['title'];
	// $results['sum'];
	// $results['content'];
	// $results['author'];
	// $results['category'];
	// $results['url'];
	// $results['thumb'];
	// $results['date'];
			
	echo  '<script>
		  $(function() {
			$( "#qry-dialog" ).dialog({
			  autoOpen: false,
			});
		 
			$( "#qry-dialog-open" ).click(function() {
			  $( "#qry-dialog" ).dialog( "open" );
			});
		  });
		  </script>';	

		$sqlcount = mysql_num_rows($query);
		
		if ($sqlcount > 0){
		
	
			echo '<h2 style="float:left; border-top:0px; margin-top:0px; padding-top:7px">'.$sqlcount.' Search Results</h2>';	
	
			echo '<a href="javascript:void(0);" class="button-icon" id="qry-dialog-open" style="float:right"><span class="code-10 plix-10"></span></a><div id="qry-dialog" title="Current SQL Query" style="display:none"><p>'.$qryFormat.'</p></div>';


				echo '<ul class="results-text" style="margin-top:0px">';
				// loop only text
				while($results = mysql_fetch_array($query)){
					if (isset($results['status'])){
						if ($results['status'] == 'in_progress' ){
							$stat = 'IN PROGRESS';	
						} else {
							$stat = strtoupper($results['status']);
						}
					}

					if ($results['assign_to']){
						$assgnto = $results['assign_to'];
					} else {
						$assgnto = '<i>(none)</i>';
					}

					if ($results['cc_to']){
						$ccto = $results['cc_to'];
					} else {
						$ccto = '<i>(none)</i>';
					}

					if ($results['customer']){
						$customer = $results['customer'];
					} else {
						$customer = '<i>(none)</i>';
					}
				
					// creating a nice format for the date output
					$date       = strtotime($results['created']);
					$date2       = strtotime($results['timestamp']);
					$date3       = strtotime($results['due_date']);
					$format_date = date("F j, Y", $date);
					$format_date2 = date("F j, Y", $date2);
					$format_date3 = date("F j, Y", $date3);

					$img = "images/icons/plix-32/darkgrey/document-transparent-32.png";

					$result = '<li>';
					
					if ($results['image'] != '') {
						$result .= '<div style="float:left; margin:5px; box-shadow: 1px 1px 5px #222222; width:85px; height:50px; margin-right:15px; border-radius:3px; background-image:url(../'.$results['image'].'); background-size:150% auto; background-repeat:no-repeat; background-position:50% 40%; cursor:pointer;" onClick=ca_request_onLoad('.$results['id'].');  >';
					} else {
						$result .= '<div style="float:left; margin:5px; box-shadow: 1px 1px 5px #222222; width:85px; height:50px; margin-right:15px; border-radius:3px; background-image:url('.$img.'); background-repeat:no-repeat; background-position:center; cursor:pointer;" onClick=ca_request_onLoad('.$results['id'].');  >';
						
					}						
						
					$result .= '</div>'
						.'<div style="float:left">';
						
							
						if($results['name']){
							$result .= '<h3><a onClick=ca_request_onLoad('.$results['id'].') href="javascript:void(0)">'.$results['new_art_concept_num'].' - '.$results['name'].'</a></h3>';	
						} else if($results['new_art_concept_num']){
							$result .= '<h3><a onClick=ca_request_onLoad('.$results['id'].') href="javascript:void(0)">'.$results['new_art_concept_num'].'</a></h3>';	
						} else if($results['project_name']){
							$result .= '<h3><a onClick=ca_request_onLoad('.$results['id'].') href="javascript:void(0)">'.$results['project_name'].'</a></h3>';	
						}
						
						
						$result .='<div class="g_1_2" style="min-width:235px; margin-right:7px">'
								.'<span><strong>Project: </strong>'.$results['project_name'].'<br/><strong>Submitted: </strong>'.$format_date.' <i>['.$results['submitted_by'].']</i> <br/><strong> Edited: </strong>'.$format_date2.' <i>['.$results['edit_by'].']</i><br/><strong>Due: </strong>'.$format_date3.'</span>'					
							.'</div>'
							
							.'<div class="g_1_2_last" style="min-width:235px">'
								.'<span><strong>Customer: </strong>'.$customer.'<br/><strong>Merchandiser: </strong>'.$assgnto.'<br/><strong>Artist: </strong>'.$ccto.'<br/><strong> Status: </strong><i>'.$stat.'</i></span>'					
							.'</div>'
	
						.'</div>'
					.'</li>'
					.'<hr/>';
					echo $result;


				}
				echo '</ul>';
			

		}else{
			echo '<h2 style="float:left; border-top:0px; margin-top:0px; padding-top:7px">Search Results</h2>';		
			echo '<a href="javascript:void(0);" class="button-icon" id="qry-dialog-open" style="float:right"><span class="code-10 plix-10"></span></a><div id="qry-dialog" title="Current SQL Query" style="display:none"><p>'.$qryFormat.'</p></div>';	
			echo '<br><br><div><i>'.$noresultsfound.'</i></div>';
		}	

?>
