<?php 
require_once('Datatables.php');

$datatables = new Datatables();  // for mysqli  =>  $datatables = new Datatables('mysqli'); 

// MYSQL configuration
$config = array(
'username' => 'root',
'password' => '',
'database' => 'artrqst',
'hostname' => 'localhost');

$datatables->connect($config);

//COUNT OF DATA SELECTED SHOULD MATCH NUMBER OF DATATABLES COLUMNS//
$datatables
->select("project_name, customer, new_art_concept_num, due_date, timestamp, id, notes_1, notes_2, style_number0, style_colors0, style_number1, style_colors1, style_number2, style_colors2, style_number3, style_colors3, created, cc_to, pid, approval, status")
->from('ca_request')
->edit_column('project_name', '$1', 'project_name')
->edit_column('new_art_concept_num', '$1', 'format_blnk(new_art_concept_num)')
->add_column('details', '$1', 'format_2(style_number0, style_colors0, style_number1, style_colors1, style_number2, style_colors2, style_number3, style_colors3)')
->edit_column('cc_to', '$1', 'format_blnk(cc_to)')
->unset_column('style_number0')
->unset_column('style_colors0')
->unset_column('style_number1')
->unset_column('style_colors1')
->unset_column('style_number2')
->unset_column('style_colors2')
->unset_column('style_number3')
->unset_column('style_colors3')
->unset_column('notes_1')
->unset_column('notes_2')
->unset_column('approval')
->unset_column('status')
->unset_column('pid');

echo $datatables->generate();

function format_blnk($a)
{
    if($a==''){
    	return '<i style="opacity:0.6">(none)</i>';
    } else {
    	return $a;
    } 
}

function format_1($b,$c)
{
    return "<p style='float:left'>".$b."</p><i style='margin-left:5px; float:left; opacity:0.6'>".$c."</i>";
}

function format_2($aa,$ab,$ba,$bb,$ca,$cb,$da,$db)
{
   	//$toReturn = '<input type="hidden" name="garmentHolder" value="';
    $toReturn       = $aa.'|'.$ab.'~';
    $toReturn       .= $ba.'|'.$bb.'~';
    $toReturn       .= $ca.'|'.$cb.'~';
    $toReturn       .= $da.'|'.$db.'~';
   // $toReturn       .= '" ><a href="javascript:void(0);" class="button-text-icon" style="float:left; margin:0 0 0 0px;">Garments<span class="tags-10 plix-10"></span></a>';
                                
    return $toReturn;
}

function format_3($a)
{
    if($a == 'January 1st, 1970' || $a == '') {
		return '<i style="opacity:0.6">(none)</i>';
	} else {
		return $a;
	}
    
}

function format_4($a)
{

	$date = explode(" ", $a, -2);
	$time = explode(" ", $a, 4);
	$dateF = implode(" ",$date);
	//$timeF = implode(" ",$time);

	return '<p style="float:left">'.$dateF.' </p><i style="opacity:0.6; float:left; margin-left:5px">'.$time[3].'</i>';

}

?>