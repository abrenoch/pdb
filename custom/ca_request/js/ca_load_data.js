///
function load_data(rowNum){
	//var $imageArea = $('#dropbox').css('background-image')
	$.post("custom/ca_request/ca_getinfo.php", {row_id : rowNum}, function(echo){

   			data = jQuery.parseJSON(echo);
   			
			console.log("FORM / ca_request: loading form - "+data[0].id+", "+data[0].project_id+", "+data[0].new_art_concept_num);
			
			/////CHECKS IMAGES
			if (data[0].image) {
				$('#dropbox').append("<img id='artImg' style='height:300px; width:auto;' src='../" + data[0].image + "'/>");
			}
			if (data[0].image2) {
				$('#dropbox2').append("<img id='artImg2' style='height:300px; width:auto;' src='../" + data[0].image2 + "'/>");
			}
			
			/////CHECKS APPROVAL
			if (data[0].approval != "") {
				$('#apprvBy1').prop('checked', false);
				$('#apprvBy2').prop('checked', false);
				$('#apprvBy3').prop('checked', false);
				$('#apprvBy4').prop('checked', false);
				var APby = data[0].approval.split("/");
					for (i=0; i<APby.length; i++) {
						var curAPby = APby[i];
							if (curAPby == "bill") {
								$('#apprvBy1').prop('checked', true);
								$("label[for='apprvBy1']").css('color','#63AB62');
							} else if (curAPby == "ed") {
								$('#apprvBy2').prop('checked', true);
								$("label[for='apprvBy2']").css('color','#63AB62');
							} else if (curAPby == "desi") {
								$('#apprvBy3').prop('checked', true);
								$("label[for='apprvBy3']").css('color','#63AB62');
							} else if (curAPby == "mitch") {
								$('#apprvBy4').prop('checked', true);
								$("label[for='apprvBy4']").css('color','#63AB62');
							};
					};
			} else {
				$('#apprvBy1').prop('checked', false);
				$('#apprvBy2').prop('checked', false);
				$('#apprvBy3').prop('checked', false);
				$('#apprvBy4').prop('checked', false);
			};	
			
			/////CHECKS DUE_DATE
			if (data[0].due_date != "1970-01-01") {
				var splitDate = data[0].due_date.split(" ")
				var parsedDate = $.datepicker.parseDate('yy-mm-dd', splitDate[0]);
				$('#due_date').datepicker('setDate', parsedDate);
			} else {
				$('#due_date').val('');
				$('#due_date').datepicker('option', {minDate: null, maxDate: null});
				
			};
							
			/////CHECKS PAGE_TYPE
			if (data[0].page_type != "") {
				if (data[0].page_type == "grp") {
					$('#pgTYP').prop('checked', true);
					$('#pgTYP2').prop('checked', false);
						
				} else if (data[0].page_type == "mkp") {
					$('#pgTYP2').prop('checked', true);
					$('#pgTYP').prop('checked', false);
						
				};
			};
			
			/////CHECKS RQST_NEEDED
			if (data[0].rqst_needed != "") {
				$('#rqBox1').prop('checked', false);
				$('#rqBox2').prop('checked', false);
				$('#rqBox3').prop('checked', false);
				var RQty = data[0].rqst_needed.split("/");
					for (i=0; i<RQty.length; i++) {
						var curRQty = RQty[i];
							if (curRQty == "fullsz") {
								$('#rqBox1').prop('checked', true);
							} else if (curRQty == "jpeg") {
								$('#rqBox2').prop('checked', true);
							} else if (curRQty == "pdf") {
								$('#rqBox3').prop('checked', true);
							};
					};
			} else {
				$('#rqBox1').prop('checked', false);
				$('#rqBox2').prop('checked', false);
				$('#rqBox3').prop('checked', false);
			};
			
			/////CHECKS HARDCOPIES
			if (data[0].hardcopies != "") {
				$( "#nmCPY").val(data[0].hardcopies);
			} else {
				$('#nmCPY').val('0');
			};
			
			/////CHECKS RETURN_TO
			if (data[0].return_to != "") {
				$( "#return_to").val(data[0].return_to);
			} else {
				$('#return_to').val('');
			};        			
			
			/////CHECKS NEW_ART_CONCEPT_NUM
			if (data[0].new_art_concept_num != "") {
				$( "#cNum").val(data[0].new_art_concept_num);
				$("#cnum_header").text(data[0].new_art_concept_num);
			} else {
				$('#cNum').val('');
			};
			
			/////CHECKS NAME
			if (data[0].name != "") {
				$( "#cName").val(data[0].name);
				$("#cnum_header").append(" - "+data[0].name);
			} else {
				$('#cName').val('');
			};
			
			/////CHECKS SUBMITTED_BY
			if (data[0].submitted_by != "") {
				$( "#subBY").val(data[0].submitted_by);
			} else {
				$('#subBY').val('');
			};   			
			
			/////CHECKS ASSIGN_TO
			if (data[0].assign_to != "") {
				$( "#assign_to").val(data[0].assign_to);
			} else {
				$('#assign_to').val('');
			};    			
			
			/////CHECKS NOTES_1
			if (data[0].notes_1 != "") {
				$( "#notesField1").val(data[0].notes_1);
			} else {
				$('#notesField1').val('');
			};       			
			
			/////CHECKS NOTES_2
			if (data[0].notes_2 != "") {
				$( "#notesField2").val(data[0].notes_2);
			} else {
				$('#notesField2').val('');
			};      			
			
			
			/////CHECKS SCRIBBLES
			if (data[0].scribbles != "") {
				miniPaint(data[0].image,data[0].scribbles,'#dropbox');
				$('input[id=hd_scribbles]').val( data[0].scribbles );	
			}; 
			if (data[0].scribbles2 != "") {
				miniPaint(data[0].image2,data[0].scribbles2,'#dropbox2');
				$('input[id=hd_scribbles2]').val( data[0].scribbles2 );	
			}; 				

			var job_id = data[0].id;
			$('input[id=hd_jobid]').val(job_id);
							
			var stN = [];
			var stC = [];
				
			stC[0] = data[0].style_colors0;
				stN[0] = data[0].style_number0;
			stC[1] = data[0].style_colors1;
				stN[1] = data[0].style_number1;
			stC[2] = data[0].style_colors2;
				stN[2] = data[0].style_number2;
			stC[3] = data[0].style_colors3;
				stN[3] = data[0].style_number3;
				
			for (var m=0; m<4; m++) {
				reload_colors(m,stC[m])
				$("#styNM"+m).val('0');
				if (stN[m] != "") {
					 $("#styNM"+m).find("option:contains("+stN[m]+")").each(function(){
						if( $(this).text() == stN[m] ) {
							$(this).attr("selected","selected");
							styChange(m);
						}
					 });
				}
			};
			 
			$('input[id=hd_submitted_by]').val(data[0].submitted_by);
			
			$('input[id=hd_edited_by]').val(data[0].edit_by);
			
			$('input[id=hd_created_date_SQL]').val(data[0].created);
				var Adate = moment(data[0].created,'YYYY-MM-DD HH:mm:ss');
				$('input[id=hd_created_date]').val( Adate.format("MMMM Do, YYYY h:mm a") );

			var Bdate = moment(data[0].timestamp,'YYYY-MM-DD HH:mm:ss');
			$('input[id=hd_edited_date]').val( Bdate.format("MMMM Do, YYYY h:mm a") ).trigger('change');
			
			if(data[0].customer_name != '') {
				if ( $("#customer_select option[value='"+data[0].customer_name+"']").length > 0 ) {
					$( "#customer_select" ).val(data[0].customer_name);
					$("#customer_input").fadeTo(0, 0.4);
					$("#customer_input").prop('disabled', 'disabled');	
				} else {
					$( "#customer_input" ).val(data[0].customer_name);
					$("#customer_select").fadeTo(0, 0.4);
					$("#customer_select").prop('disabled', 'disabled');				
				}
			}
			
			$('#crnt_status').val(data[0].status);
			
			if (data[0].status == 'in_progress') {
				$("#stat_header").css('color','blue');
				$("#stat_header").append("IN PROGRESS");
			} else if (data[0].status == 'inactive') {
				$("#stat_header").css('color','grey');
				$("#stat_header").append("INACTIVE");
			} else if (data[0].status == 'cancelled') {
				$("#stat_header").css('color','red');
				$("#stat_header").append("CANCELLED");
			} else if (data[0].status == 'completed') {
				$("#stat_header").css('color','#63AB62');
				$("#stat_header").append("COMPLETED");
			};

			/////CHECKS CC_TO
			if (data[0].cc_to != "") {
				$( "#cc_to").val(data[0].cc_to);
			} else {
				$('#cc_to').val('');
			};  
			
			/////CHECKS PROJECT_NAME
			if (data[0].project_name != "") {
				$( "#pName").val(data[0].project_name);
			} else {
				$('#pName').val('');
			};  				
			
			/////CHECKS SAMPLE_NUMBER
			if (data[0].sample_number != "") {
				$( "#cSample").val(data[0].sample_number);
			} else {
				$('#cSample').val('');
			};  
							
			/////CHECKS PROJECT_NUMBER
			$('input[id=hd_pid]').val(data[0].pid);
		
		$('#sub_button').css("display","none");
		$('#upd_button').css("display","inline");
		$('#rev_button').css("display","inline");
		
		if ($crntPRM >= 4) {
			$('#action-menu li div ul').prepend('<li id="update_status" onClick=updStatus(); style="cursor:pointer;"><a><span class="tags-10 plix-10"></span>Change Status</a></li>');	
			$('#email_button').css("display","inline");	
			$('#brc_button').css("display","inline");
		}
		
		if ($crntDEP != "crt") {
			$('#clrBTN').css("display","inline");	
		} else {
			$('#clrBTN').css("display","none");
		}
		
		if (data[0].status != 'cancelled') {    			
			if ( $('#apprvBy2').prop('checked') == true || $('#apprvBy3').prop('checked') == true ) {
				var stat = "PENDING APPROVAL"
				if ( $('#apprvBy2').prop('checked') == true && $('#apprvBy3').prop('checked') == true ) {
					var stat = "PENDING FINAL"
					if ( $('#apprvBy4').prop('checked') == true ) {
						var stat = "APPROVAL FINAL"
					}
				}
				$("#stat_header").append(" - "+stat);
			}
		}	
  	}) 
};

function miniPaint(data1,data2,loc) {
	var img = new Image();
	img.src = data1;	
	img.onload = function() {
		$imgH = this.height;
		$imgW = this.width;
		$img_fix_ratio = $imgH / 300;
		var sourceImage = new Image();
		sourceImage.src = data2;
		sourceImage.height = (($imgH + 100) / $img_fix_ratio);
		sourceImage.width = (($imgW + 100) / $img_fix_ratio);
		var sDiv = document.createElement('div');
		if (loc == '#dropbox') {
			sDiv.id = "sDiv";		
		} else if (loc == '#dropbox2') { 
			sDiv.id = "sDiv2";			
		}		
    	$(sDiv).css('position','relative').css('top',-298).css('pointer-events','none').css('overflow','hidden').css('height','300').css('width',($imgW/$img_fix_ratio)+2).append(sourceImage);  	
    	$(sourceImage).css('position','relative').css('top',(100 / $img_fix_ratio)* -0.5).css('left',((100 / $img_fix_ratio)* -0.5));	
    	$(loc+' img').css('position','relative').css('height',300).css('top',0);
		$(loc).css('height',300);
    	$(loc).append(sDiv);
	}	  
}

function miniPaint_close(data,loc) {
	if (loc == '#dropbox') {
		var noteImageData = $("#newCanvas").wPaint("image");		
	} else if (loc == '#dropbox2') { 
		var noteImageData = $("#newCanvas2").wPaint("image");	
	}
	var img = new Image();
	img.src = data;					
	img.onload = function() {
		$imgH = this.height;
		$imgW = this.width;
		$img_fix_ratio = $imgH / 300;					
		var sourceImage = new Image();
		sourceImage.src = noteImageData;
		sourceImage.height = (($imgH + 100) / $img_fix_ratio);  				
		sourceImage.width = (($imgW + 100) / $img_fix_ratio);
		if (loc == '#dropbox') {
			$('#sDiv').remove();	
			var sDiv = document.createElement('div');		
			sDiv.id = "sDiv";		
		} else if (loc == '#dropbox2') { 
			$('#sDiv2').remove();	
			var sDiv = document.createElement('div');			
			sDiv.id = "sDiv2";			
		}
    	jQuery(sDiv).html('');
    	$(sDiv).css('position','relative').css('top',-298).css('pointer-events','none').css('overflow','hidden').css('height','300').css('width',($imgW/$img_fix_ratio)+2).append(sourceImage);  	
    	$(sourceImage).css('position','relative').css('top',(100 / $img_fix_ratio)* -0.5).css('left',(100 / $img_fix_ratio)* -0.5);	
    	$(loc+' img').css('position','relative').css('height',300).css('top',0);
		$(loc).css('height',300);
    	$(loc).append(sDiv);
	}  
}