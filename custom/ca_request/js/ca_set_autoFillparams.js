function reload_colors(nmb,data) {
		
		$colorFieldID = "colors"+nmb;
		$colorFieldPRNT = "clrSPN"+nmb;

		var dataSplit = data.split("/");

			for (r=0; r<dataSplit.length; r++) {
				if (dataSplit != "") {

						var ui = {"item":{"label":dataSplit[r],"value":dataSplit[r]}}
						
						var friend = ui.item.value,
							span = $("<span>").text(friend),
							a = $("<a>").addClass("remove").attr({
								href: "javascript:",
								title: "Remove " + friend
							}).text("x").appendTo(span);
						span.insertBefore("#"+$colorFieldID); //add friend to friend div
						$("#"+$colorFieldID).val("").css("top", 2);
				}		
			};
				
				$(".remove", document.getElementById($colorFieldPRNT)).live("click", function(){ //add live handler for clicks on remove links
					$(this).parent().remove(); //remove current friend
					//correct 'to' field position
					if($("#"+$colorFieldPRNT+" span").length === 0) {
						$("#"+$colorFieldID).css("top", 0);
					}				
				});				
};
	