function fetchAll() {
	var noteImageData = $("#newCanvas").wPaint("image");	
	if ((typeof noteImageData) != 'string'){
		var noteImageData = $('input[id=hd_scribbles]').val();
	};		
	var noteImageData2 = $("#newCanvas2").wPaint("image");	
	if ((typeof noteImageData2) != 'string'){
		var noteImageData2 = $('input[id=hd_scribbles2]').val();
	};	
    if ($('#emailCHK').is(":checked")) {
		var eml = $('#assign_to').val();
		if (eml == "") {
			ERR_notify("Enter a username in the 'Assign To' field, or un-check the 'Notify' option");
			return
		};	
	};		
	var $toArray = [];
	for (var b=0; b<4; b++) {
		var $toArray_sub = [];
		var someThing = "";				
		var someThing = $('#clrSPN'+b).find('span') //SETS VARIABLE 'someThing' TO THE DIV ELEMENT AND FINDS EVER SPAN-OBJECT WITHIN 
			for (i=0;i<someThing.length;i++) { //REPEATS FOR NUMBER OF OBJECTS FOUND FROM THE ABOVE
				var value = $(someThing[i]).text(); //SETS VARIABLE 'value' TO THE TEXT OF THE OBJECT NUMBER CORRESPONDING WITH THE REPEAT FUNCTION 
				value = value.slice(0, -1); //CUTS ONE LETTER OFF THE END (FORMATTING HACK)						
				$toArray_sub.push(value); //ADDS VALUE FORMATTED FROM ABOVE TO END OF THE ARRAY
			}			
		$toArray[b] = $toArray_sub
		document.getElementById('inclColors'+b).value =  $toArray; //PUSHES 'toArray' JOINED BY ',' TO MAKE A STRING, TO THE HIDDEN FIELD 'inclColors0'	
	}		
	var allVals = [];
        $('#apprvlFM :checked').each(function() {
        allVals.push($(this).val());
    });         
    var allVals2 = [];
        $('#d_f :checked').each(function() {
        allVals2.push($(this).val());
    });
	if ($('input[name=cNum]').val() == "" && $('#pName').val() == "") {
		ERR_notify("Please complete either the 'Project Name' or 'Concept Number' field");
		return
	};
	if (!$crntUSR) {
		ERR_notify("Could not get _SESSION['usr']");
		return
	};
	var BGimage = $('#dropbox img').attr('src');
	var BGimage2 = $('#dropbox2 img').attr('src');
		
	if ($('input[name=nmCPY]').val() == "") {
		ERR_notify("Invalid Quantity of Hard Copies");
		return
	};
	if ($('#customer_select').val() != "") {
		var this_customer = $('#customer_select').val()
	} else {
		var this_customer = $('#customer_input').val()
	};	
	if ( $('#apprvBy2').prop('checked') == true || $('#apprvBy3').prop('checked') == true ) {
		if ( $('#apprvBy4').prop('checked') == true ) {
    		var stat = true
    	} else {
    		var stat = false
    	}
	} else {
		var stat = false
	}	
	var mmntdate = $( "#due_date").datepicker('getDate');
	//console.log(mmntdate);
	if( $( "#due_date").datepicker('getDate') ){
		var mmntdate = moment($( "#due_date").datepicker('getDate')).format('YYYY-MM-DD');
	} else {
		var mmntdate = ''; 
	}
	if( $('#pName').val().indexOf("@#@") !== -1 || $('#notesField1').val().indexOf("@#@") !== -1 || $('#notesField2').val().indexOf("@#@") !== -1 ){
		ERR_notify("Please do not use string '@#@' in any field for the time being");
		return;
	}	
	return {
		'PGTradio':$('input[name=PGTradio]:checked').val(), 
		'apprvby':allVals, 
		'rqstneed':allVals2,
		'bgimage':BGimage, 
		'bgimage2':BGimage2, 
		'new_art_concept_num':$('input[name=cNum]').val(), 
		'copynum':$('input[name=nmCPY]').val(), 
		'art_name':$('input[name=cName]').val(), 
		'due_date':mmntdate, 
		'submitted_by':$crntUSR, 
		'assign_to':$('#assign_to').val(), 
		'cc_to':$('#cc_to').val(), 
		'return_to':$('input[name=return_to]').val(), 
		'notes_1':$('#notesField1').val(), 
		'notes_2':$('#notesField2').val(), 
		'scrib':noteImageData,
		'scrib2':noteImageData2,
		'origin':$('#hd_submitted_by').val(),
		'id':$('#hd_jobid').val(),
		'pid':$('#hd_pid').val(),
		'customer':this_customer,
		'created':$('#hd_created_date').val(),
		'createdSQL':$('#hd_created_date_SQL').val(),		
		'stat':stat,	
		'project_name':$('#pName').val(),
		'sample_num':$('#cSample').val(),	
		'style_number0':$strUser[0], 
		'style_colors0':$toArray[0], 
		'style_number1':$strUser[1], 
		'style_colors1':$toArray[1], 
		'style_number2':$strUser[2],
		'style_colors2':$toArray[2], 
		'style_number3':$strUser[3], 
		'style_colors3':$toArray[3] 
	}; 
};

function submit_new() { 
	var fd = fetchAll();
	console.log('FORM / ca_request: creating new job entry');	
	$.ajax({
		type: "POST",
		url: "custom/ca_request/ca_submitForm.php",
		dataType: "html",
		timeout:8000, 
		data: {
			'PGTradio':fd.PGTradio, 
			'apprvby':fd.apprvby, 
			'rqstneed':fd.rqstneed,
			'bgimage':fd.bgimage, 
			'bgimage2':fd.bgimage2, 
			'new_art_concept_num':fd.new_art_concept_num, 
			'copynum':fd.copyNum, 
			'art_name':fd.art_name, 
			'due_date':fd.due_date, 
			'submitted_by':fd.submitted_by, 
			'assign_to':fd.assign_to,
			'cc_to':fd.cc_to,  
			'return_to':fd.return_to,
			'notes_1':fd.notes_1, 
			'notes_2':fd.notes_2, 
			'scrib':fd.scrib,
			'scrib2':fd.scrib2,
			'customer':fd.customer,
			'project_name':fd.project_name,
			'sample_num':fd.sample_num,
			'style_number0':fd.style_number0, 
			'style_colors0':fd.style_colors0, 
			'style_number1':fd.style_number1, 
			'style_colors1':fd.style_colors1, 
			'style_number2':fd.style_number2,
			'style_colors2':fd.style_colors2, 
			'style_number3':fd.style_number3, 
			'style_colors3':fd.style_colors3 },
		cache: false,
		success: function(echo)
			{
				if(echo){
					if ($('#emailCHK').is(":checked")) {
						var eml_Line1 = "New Creative Art Request";
						var eml_Line2 = "You have been assigned to a new concept:";
						submitEmail(echo,fd.new_art_concept_num,fd.art_name,$crntUSR,fd.due_date,eml_Line1,eml_Line2);
						$('#emailCHK').prop('checked', false);
					}
					GRWL_notify("Success","New Job Information Submitted","2500","forward","ca_request_onLoad",echo);
				}
			}
    });
    return false;
};

function branch_job() { 
	var fd = fetchAll();
	console.log('FORM / ca_request: creating new branch from job id: '+fd.id);
	$.ajax({
		type: "POST",
		url: "custom/ca_request/ca_submitForm.php",
		dataType: "html",
		timeout:8000, 
		data: {
			'PGTradio':fd.PGTradio, 
			'apprvby':fd.apprvby, 
			'rqstneed':fd.rqstneed,
			'bgimage':fd.bgimage, 
			'bgimage2':fd.bgimage2, 
			'new_art_concept_num':fd.new_art_concept_num, 
			'copynum':fd.copyNum, 
			'art_name':fd.art_name, 
			'due_date':fd.due_date, 
			'submitted_by':fd.submitted_by, 
			'assign_to':fd.assign_to,
			'cc_to':fd.cc_to,  
			'return_to':fd.return_to,
			'notes_1':fd.notes_1, 
			'notes_2':fd.notes_2, 
			'scrib':fd.scrib,
			'scrib2':fd.scrib2,
			'customer':fd.customer,
			'project_name':fd.project_name,
			'sample_num':fd.sample_num,
			'style_number0':fd.style_number0, 
			'style_colors0':fd.style_colors0, 
			'style_number1':fd.style_number1, 
			'style_colors1':fd.style_colors1, 
			'style_number2':fd.style_number2,
			'style_colors2':fd.style_colors2, 
			'style_number3':fd.style_number3, 
			'style_colors3':fd.style_colors3 },
		cache: false,
		success: function(echo)
			{
				if(echo){
					if ($('#emailCHK').is(":checked")) {
						var eml_Line1 = "New Creative Art Request";
						var eml_Line2 = "You have been assigned to a new concept:";
						submitEmail(echo,fd.new_art_concept_num,fd.art_name,$crntUSR,fd.due_date,eml_Line1,eml_Line2);
						$('#emailCHK').prop('checked', false);
					}
					GRWL_notify("Success","New Job Branch Created","2500","forward","ca_request_onLoad",echo);
				}
			}
    });
    return false;
};

function update_job(sentID) {
	var fd = fetchAll();
	console.log('FORM / ca_request: updating job id: '+sentID);
	$.ajax({
		type: "POST",
		url: "custom/ca_request/ca_updateForm.php",
		dataType: "html",
		timeout:8000,
		data: {
			"id":sentID, 
			"PGTradio":fd.PGTradio, 
			"apprvby":fd.apprvby, 
			"rqstneed":fd.rqstneed,
			"bgimage":fd.bgimage, 
			"bgimage2":fd.bgimage2, 
			"new_art_concept_num":fd.new_art_concept_num, 
			"copynum":fd.copyNum, 
			"art_name":fd.art_name, 
			"due_date":fd.due_date, 
			"edited_by":fd.submitted_by, 
			"assign_to":fd.assign_to, 
			"cc_to":fd.cc_to,
			"return_to":fd.return_to,
			"notes_1":fd.notes_1, 
			"notes_2":fd.notes_2, 
			"scrib":fd.scrib,
			"scrib2":fd.scrib2,
			"customer":fd.customer,
			"stat":fd.stat,
			"project_name":fd.project_name,
			"sample_num":fd.sample_num,
			"style_number0":fd.style_number0, 
			"style_colors0":fd.style_colors0, 
			"style_number1":fd.style_number1, 
			"style_colors1":fd.style_colors1, 
			"style_number2":fd.style_number2,
			"style_colors2":fd.style_colors2, 
			"style_number3":fd.style_number3, 
			"style_colors3":fd.style_colors3 },
		cache: false,
		success: function(echo)
			{
				if(echo){
					if ($('#emailCHK').is(":checked")) {
						var eml_Line1 = "Creative Art Request Updated";
						var eml_Line2 = "You have been notified and/or assigned to an update applied to the following concept:";
						submitEmail(sentID,fd.new_art_concept_num,fd.art_name,$crntUSR,fd.due_date,eml_Line1,eml_Line2);
						$('#emailCHK').prop('checked', false);
					}
					if(fd.stat==true){chng_status(sentID,'completed',1)}else{chng_status(sentID,'in_progress',1)};
					GRWL_notify("Success","Job Information Updated","2500","forward","ca_request_onLoad",sentID);
				}
			}
    });	
    return false;		
};

function revise_job() {
	var fd = fetchAll();	
	if (fd.origin == "") {
		ERR_notify("Could not get original submission user");
		return
	};	
	console.log('FORM / ca_request: creating new revision in `pid` group: '+fd.pid);
	$.ajax({
		type: "POST",
		url: "custom/ca_request/ca_reviseForm.php",
		dataType: "html",
		timeout:8000, 
		data: {
			'pid':fd.pid, 
			'PGTradio':fd.PGTradio, 
			'apprvby':fd.apprvby, 
			'rqstneed':fd.rqstneed,
			'bgimage':fd.bgimage, 
			'bgimage2':fd.bgimage2, 
			'new_art_concept_num':fd.new_art_concept_num, 
			'copynum':fd.copyNum, 
			'art_name':fd.art_name, 
			'due_date':fd.due_date, 
			'edited_by':fd.submitted_by, 
			'sub_by':fd.origin, 
			'assign_to':fd.assign_to, 
			'cc_to':fd.cc_to,
			'return_to':fd.return_to,
			'notes_1':fd.notes_1, 
			'notes_2':fd.notes_2, 
			'scrib':fd.scrib,
			'scrib2':fd.scrib2,
			'customer':fd.customer,
			'created':fd.createdSQL,
			'stat':fd.stat,
			'project_name':fd.project_name,
			'sample_num':fd.sample_num,
			'style_number0':fd.style_number0, 
			'style_colors0':fd.style_colors0, 
			'style_number1':fd.style_number1, 
			'style_colors1':fd.style_colors1, 
			'style_number2':fd.style_number2,
			'style_colors2':fd.style_colors2, 
			'style_number3':fd.style_number3, 
			'style_colors3':fd.style_colors3 },
		cache: false,
		success: function(echo)
			{
				if(echo){
					if ($('#emailCHK').is(":checked")) {
						var eml_Line1 = "Creative Art Request Revision";
						var eml_Line2 = "You have been notified of a new revision applied to the following concept:";
						submitEmail(echo,fd.new_art_concept_num,fd.art_name,$crntUSR,fd.due_date,eml_Line1,eml_Line2);
						$('#emailCHK').prop('checked', false);
					}
					if(fd.stat==true){chng_status(echo,'completed',1)}else{chng_status(echo,'in_progress',1)};
					GRWL_notify("Success","New Job Revision Submitted","2500","forward","ca_request_onLoad",echo);
				}	
			}
    });	
    return false;		
};

function chng_status(sentID,sentSTAT,alrt) {
	console.log('FORM / ca_request: updating status of id:'+sentID+' and parent group to "'+sentSTAT+'"');
	$.ajax({
		type: "POST",
		url: "custom/ca_request/ca_changeStatus.php",
		dataType: "html",
		timeout:8000, 
		data: {
			'id':sentID, 
			'status':sentSTAT },
		cache: false,
		success: function(a)
			{
				if (a) {
					if (a.toLowerCase().indexOf("error") >= 0) { 
						ERR_notify(a) 
					}
				} else {
					if (!alrt) {GRWL_notify("Success","Job Status Changed","2500","forward","ca_request_onLoad",sentID)};
				}
			}
    });
    return false;			
};
