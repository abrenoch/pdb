$(function(){
	
	var dropbox = $('#dropbox'),
		dropbox2 = $('#dropbox2'),
		message = $('.message', dropbox);

	dropbox.filedrop({
		fallback_id: 'hd_upload',
		paramname:'pic',
		maxfiles: 1,
    	maxfilesize: 2,
		url: '../ca_fd_poster.php',
		uploadFinished:function(i,file,response){
							$.data(file).addClass('done');
							$dropFile = response
							// response is the JSON object that post_file.php returns
						},
    	error: function(err, file) {
					switch(err) {
						case 'BrowserNotSupported':
							showMessage('Your browser does not support HTML5 file uploads!');
							break;
						case 'TooManyFiles':
							ERR_notify('Too many files! Please select 5 at most! (configurable)');
							break;
						case 'FileTooLarge':
							ERR_notify(file.name+' is too large! Please upload files up to 2mb (configurable).');
							break;
						default:
							break;
					}
				},
		beforeEach: function(file){
					if(!file.type.match(/^image\//)){
						ERR_notify('Only images are allowed!');
						// Returning false will cause the file to be rejected
						return false;
					}
				},
		uploadStarted:function(i, file, len){
					createImage(file);
				},
		progressUpdated: function(i, file, progress) {
					$.data(file).find('.progress').width(progress);
				},
    	dragLeave: function() {
					fadeAnimationOUT();
				},
    	drop: function() {
					fadeAnimationOUT();
				},
    	dragOver: function() {
					fadeAnimationIN();
				},
    	afterAll: function() {
					$('#upldHolder').fadeTo("slow",0, function() {
						$('#upldHolder').remove();
					});
					$('#newCanvas').wPaint('clear');
					$("#hd_scribbles").val('')
					$('#sDiv').remove();
					$('#artImg').remove();
					$dropFile = '../'+$dropFile;
					$('#dropbox').append("<img id='artImg' style='height:300px; width:auto;' src='" + $dropFile + "'/>");		
					// runs after all files have been uploaded or otherwise dealt with
				}
	});
	
		dropbox2.filedrop({
		fallback_id: 'hd_upload2',
	
		// The name of the $_FILES entry:
		paramname:'pic',
		
		maxfiles: 1,
    	maxfilesize: 2,
		url: '../ca_fd_poster.php',
		
		uploadFinished:function(i,file,response){
			$.data(file).addClass('done');
			$dropFile2 = response
			// response is the JSON object that post_file.php returns
		},
		
    	error: function(err, file) {
			switch(err) {
				case 'BrowserNotSupported':
					showMessage('Your browser does not support HTML5 file uploads!');
					break;
				case 'TooManyFiles':
					ERR_notify('Too many files! Please select 5 at most! (configurable)');
					break;
				case 'FileTooLarge':
					ERR_notify(file.name+' is too large! Please upload files up to 2mb (configurable).');
					break;
				default:
					break;
			}
		},
		
		// Called before each upload is started
		beforeEach: function(file){
			if(!file.type.match(/^image\//)){
				ERR_notify('Only images are allowed!');
				
				// Returning false will cause the
				// file to be rejected
				return false;
			}
		},
		
		uploadStarted:function(i, file, len){
			createImage2(file);
		},
		
		progressUpdated: function(i, file, progress) {
			$.data(file).find('.progress').width(progress);
		},
		
    	
    	dragLeave: function() {
			fadeAnimationOUT2();
    	},
    	
    	
    	drop: function() {
    	
			fadeAnimationOUT2();
			// user drops file
   		},
   		
   		
    	dragOver: function() {
			fadeAnimationIN2();
    	},
    	
    	afterAll: function() {
    	
    		$('#upldHolder2').fadeTo("slow",0, function() {
    			$('#upldHolder2').remove();
    		});
    		$('#newCanvas2').wPaint('clear');
    		$("#hd_scribbles2").val('')
    		$('#sDiv').remove();
    			//$('#dropbox').css('background-image', 'url(' + $dropFile + ')').css('background-repeat', 'no-repeat').css('background-position', 'center top').css('background-size', 'auto 100%');
    	
			$('#artImg2').remove();
			$dropFile2 = '../'+$dropFile2;
			$('#dropbox2').append("<img id='artImg2' style='height:300px; width:auto;' src='" + $dropFile2 + "'/>");		
       			// runs after all files have been uploaded or otherwise dealt with
    		}
	})
	
	var template2 = '<div class="preview" id="upldHolder2" style="position:absolute; left:42%">'+
						'<span class="imageHolder">'+
							'<img />'+
							'<span class="uploaded"></span>'+
						'</span>'+
						'<div class="progressHolder">'+
							'<div class="progress"></div>'+
						'</div>'+
					'</div>'; 
					
	var template = '<div class="preview" id="upldHolder" style="position:absolute; left:42%">'+
						'<span class="imageHolder">'+
							'<img />'+
							'<span class="uploaded"></span>'+
						'</span>'+
						'<div class="progressHolder">'+
							'<div class="progress"></div>'+
						'</div>'+
					'</div>'; 					
					
	
	
	function createImage(file){
		var preview = $(template), 
			image = $('img', preview);
		var reader = new FileReader();
		image.width = 100;
		image.height = 100;
		reader.onload = function(e){
			// e.target.result holds the DataURL which
			// can be used as a source of the image:
			image.attr('src',e.target.result);
			imageUrl = e.target.result;
		};
		// Reading the file as a DataURL. When finished,
		// this will trigger the onload function above:
		reader.readAsDataURL(file);
		message.hide();
		preview.appendTo(dropbox);
		// Associating a preview container
		// with the file, using jQuery's $.data():
		
		$.data(file,preview);
		//imgURL = this.getClass().getResource(file);
		//alert(imgURL);
	}
	
	function createImage2(file){
		var preview = $(template2), 
			image = $('img', preview);
		var reader = new FileReader();
		image.width = 100;
		image.height = 100;
		reader.onload = function(e){
			image.attr('src',e.target.result);
			imageUrl = e.target.result;
		};
		reader.readAsDataURL(file);
		preview.appendTo(dropbox2);
		$.data(file,preview);
	}
	
	function showMessage(msg){
		message.html(msg);
	}
	
	function fadeAnimationIN(){
		$('#dropbox_cover').fadeTo("normal", .5, function() {
			$('#dropbox_cover').stop(true,false);
		}); 
	}
	function fadeAnimationOUT(){
		$('#dropbox_cover').stop(true,false);
			$('#dropbox_cover').fadeTo("normal", 0, function() {
		 }); 
	}
	
	function fadeAnimationIN2(){
		$('#dropbox_cover2').fadeTo("normal", .5, function() {
			$('#dropbox_cover2').stop(true,false);
		}); 
	}
	function fadeAnimationOUT2(){
		$('#dropbox_cover2').stop(true,false);
			$('#dropbox_cover2').fadeTo("normal", 0, function() {
		 }); 
	}	

});