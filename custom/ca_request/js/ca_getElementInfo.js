///FETCH ELEMENT INFO FROM SQL (VALID REQUEST TYPES: styleOptions,userList,currentId,customerList)
function userList() {	
	jQuery.grep($usrList, function(obj1) {
    	if (obj1.dep === 'mrc'){
    		$('#assign_to').append('<option value="'+obj1.user+'">'+obj1.first+' '+obj1.last+'</option>');
    	} else if (obj1.dep === 'crt') {
    		$('#cc_to').append('<option value="'+obj1.user+'">'+obj1.first+' '+obj1.last+'</option>');
    	}
	});
};


///FETCH ELEMENT INFO FROM SQL (VALID REQUEST TYPES: styleOptions,userList,currentId,customerList)
function currentId(d) {
	if($ca_request_ID != "null"){	
		$.ajax({
	    type: "POST",
	    url: "custom/ca_request/ca_getElementInfo.php",
	    data: {
			'type':'currentId', 
			'condition':d
			},
	    cache: false,
	    success: function(echo)
	        {
					var vals = echo.split("|");
				for (i=0;i<vals.length-1;i++) {
					var subvals = vals[i].split("%");
						var oId= subvals[0];				
					$('#rev_num_sel').append('<option value="'+oId+'">'+(i+1)+'</option>');							
				}
				revUpdate($ca_request_ID);	
	        }
	    });
	}	
};


///FETCH ELEMENT INFO FROM SQL (VALID REQUEST TYPES: styleOptions,userList,currentId,customerList)
function styleOptions() {	
	$.ajax({
    type: "POST",
    url: "custom/ca_request/ca_getElementInfo.php",
    data: {
		'type':'styleOptions', 
		'condition':''
		},
    cache: false,
    success: function(echo)
        {
			var vals = echo.split("|");
			for (i=0;i<vals.length-1;i++) {
				var subvals = vals[i].split("%");
					var oNum = subvals[0];
					var oName = subvals[1];
					$('#styNM0').append('<option value="'+oName+'">'+oNum+'</option>');
					$('#styNM1').append('<option value="'+oName+'">'+oNum+'</option>');
					$('#styNM2').append('<option value="'+oName+'">'+oNum+'</option>');
					$('#styNM3').append('<option value="'+oName+'">'+oNum+'</option>');	
			}	
        }
    });	
};


///FETCH ELEMENT INFO FROM SQL (VALID REQUEST TYPES: styleOptions,userList,currentId,customerList)
function customerOptions() {	
	$.ajax({
    type: "POST",
    url: "custom/ca_request/ca_getElementInfo.php",
    data: {
		'type':'customerList', 
		'condition':''
		},
    cache: false,
    success: function(echo)
        {
			var vals = echo.split("|");
			for (i=0;i<vals.length-1;i++) {
				var oName = vals[i];
				$('#customer_select').append('<option value="'+oName+'">'+oName+'</option>');
			}	
        }
    });	
};

function revUpdate(number){
	$(document).ready(function() {	
		$("#rev_num_sel").val(number);			
		var ot = $("#rev_num_sel option:selected").text();
		var os = document.getElementById("rev_num_sel").length;
		if(ot==os){
			$('label[for=rev_num_sel]').css({color:'#63AB62'});
		} else {
			$('label[for=rev_num_sel]').css({color:'red'});
		}						
	});
};