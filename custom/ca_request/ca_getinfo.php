<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/sql_params.php";
   include_once($path);
	
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, 'artrqst');//DB CRUDENTIALS	
	if ($mysqli->connect_error) {
		die('Connect Error (' . $mysqli->connect_errno . ') '
		. $mysqli->connect_error);
	}

	$idRW = $_POST['row_id'];
	$idRW = mysql_real_escape_string($idRW); 

	$subquery = "SELECT * FROM `ca_request` WHERE id='{$idRW}'";

	$query = $mysqli->query($subquery) or die(mysql_error());	//Query the database for the results we want
	
	if (mysqli_num_rows($query) > 0){
		while( $array[] = $query->fetch_object() );	//Create an array of objects for each returned row
		array_pop($array);	//Remove the blank entry at end of array
		$output = array();
		foreach($array as $option) :    
			$output[] = array(
				"id" => $option->id, 
				"image" => $option->image, 
				"image2" => $option->image2, 
				"approval" => $option->approval, 
				"due_date" => $option->due_date, 
				"page_type" => $option->page_type, 
				"rqst_needed" => $option->rqst_needed, 
				"hardcopies" => $option->hardcopies,
				"return_to" => $option->return_to, 
				"new_art_concept_num" => $option->new_art_concept_num, 
				"name" => $option->name, 
				"style_number0" => $option->style_number0, 
				"style_number1" => $option->style_number1, 
				"style_number2" => $option->style_number2, 
				"style_number3" => $option->style_number3, 
				"style_colors0" => $option->style_colors0,
				"style_colors1" => $option->style_colors1,
				"style_colors2" => $option->style_colors2,
				"style_colors3" => $option->style_colors3,
				"submitted_by" => $option->submitted_by, 
				"assign_to" => $option->assign_to, 
				"related_id" => $option->related_id,
				"notes_1" => $option->notes_1,
				"notes_2" => $option->notes_2,
				"scribbles" => $option->scribbles,
				"scribbles2" => $option->scribbles2,
				"created" => $option->created,
				"timestamp" => $option->timestamp,
				"edit_by" => $option->edit_by,
				"customer" => $option->customer,
				"status" => $option->status,
				"cc_to" => $option->cc_to,
				"project_name" => $option->project_name,
				"sample_number" => $option->sample_number,
				"pid" => $option->pid
			);	
		endforeach;	
		echo json_encode($output);	
	} else {
		die('ERROR: No results returned');
	}	

?>   
