<?php 
require_once('Datatables.php');

$datatables = new Datatables();  // for mysqli  =>  $datatables = new Datatables('mysqli'); 

// MYSQL configuration
$config = array(
'username' => 'root',
'password' => '',
'database' => 'seps_check',
'hostname' => 'localhost');

$datatables->connect($config);

$datatables
->select("art_num, concept_num, checked_by, check_date, sep_by, sep_date, ship_date, job_type, err_type, job_value, error_value, err0, err1, err2, err3, notes, notes2, filed, approved, returned_to, id")
->from('error_logs')
->edit_column('notes', '$1', 'format_2(notes)')
->edit_column('errors', '$1', 'format_2(errors)')
->edit_column('err_type', '$1', 'format_2(err_type)')
->unset_column('job_value')
->unset_column('error_value')
->unset_column('err_type')
->unset_column('job_type')
->unset_column('err0')
->unset_column('err1')
->unset_column('err2')
->unset_column('err3')
->unset_column('returned_to')
->unset_column('approved')
->unset_column('filed')
->unset_column('notes2')
->unset_column('notes');





echo $datatables->generate();

function format_1($a)
{
    if($a == 'January 01, 1970' || $a == '') {
		return '<i style="opacity:0.6">(none)</i>';
	} else {
		return $a;
	}   
}

function format_2($a)
{
    if(!$a) {
		return '<i style="opacity:0.6">(none)</i>';
	} else {
		return $a;
	}   
}

?>