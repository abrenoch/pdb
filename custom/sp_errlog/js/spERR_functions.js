function sperr_fetchAll() {	
	var $toArray = [];
	var someThing = $('#sperrlog_Serror01').find('span')
	for (i=0;i<someThing.length;i++) {
		var value = $(someThing[i]).text().slice(0, -1);
		var ps = '';
		jQuery.grep($errList, function(obj1) {
    		if (obj1.error === value){
    			ps = obj1.id;
    		} 
		});
		$toArray.push(ps);
	}	
	if ( $('input[name=sperrlog_art]').val() == '' ) {
		ERR_notify("Please complete the 'Art Number' field");
		return
	};
	if ( $('input[name=sperrlog_concept]').val() == '' ) {
		ERR_notify("Please complete the 'Concept Number' field");
		return
	};
	
	if( $( "#sperrlog_shipdate").datepicker('getDate') ){
		var mmntdateSHIP = moment($( "#sperrlog_shipdate").datepicker('getDate')).format('YYYY-MM-DD');
	} else {
		var mmntdateSHIP = '';
		//ERR_notify("Please complete the 'Ship Date' field");
		//return		
	}
	if( $( "#sperrlog_sepdate").datepicker('getDate') ){
		var mmntdateSEP = moment($( "#sperrlog_sepdate").datepicker('getDate')).format('YYYY-MM-DD');
	} else {
		ERR_notify("Please complete the 'Separation Date' field");
		return		
	}
	
	if ( $('#sperrlog_jtype').val() == "" || $('#sperrlog_jtypev').val() == "" ) {
		ERR_notify("Please specify both the 'Job Type' and 'Job Type Variant'");
		return
	};		
	
	if ( $('#sperrlog_sepper').val() == "" ) {
		ERR_notify("Please specify the Separation Artist");
		return
	};			

	
	var e0,e1,e2,e3 = 0;
	if($toArray[0]) e0 = $toArray[0];
	if($toArray[1]) e1 = $toArray[1];
	if($toArray[2]) e2 = $toArray[2];
	if($toArray[3]) e3 = $toArray[3];
	
	if ( $('#sperr_filed_tgl').prop('checked') == true ) {
    	var a = 0
    } else {
    	var a = 1
    }
    
	if ( $('#sperr_approved_tgl').prop('checked') == true ) {
    	var b = 0
    } else {
    	var b = 1
    }  
      
    var crnusr = $('#sperrlog_checker').val();
    
	if ( $('#sperr_rtrnd_tgl').prop('checked') == true ) {
    	$toArray.length=0;
    	e0 = 0;
    	e1 = 0;
    	e2 = 0;
    	e3 = 0;    	
		crnusr = '';
		var c = 0
    } else {
    	var c = 1
    }

	var eto = '';
	if ($toArray.length > 0){
		if ( $('#sperrlog_errto').val() == "" ){
			ERR_notify("Please specify what the errors apply to");
			return				
		} else {
			eto = $('#sperrlog_errto').val();
		}
		if (crnusr == '') {
	 		ERR_notify("Please specify who this job was checked by"); 
	 		return;
	 	};
	}	
	
	return {
		'art_number':$('input[name=sperrlog_art]').val(), 
		'concept_number':$('input[name=sperrlog_concept]').val(), 
		'ship_date':mmntdateSHIP,
		'sep_date':mmntdateSEP, 
		'job_type':$('#sperrlog_jtype').val(), 
		'job_type_v':$('#sperrlog_jtypev option:selected').text(), 
		'separator':$('#sperrlog_sepper').val(), 
		'errors':$toArray,
		'err0':e0,
		'err1':e1,
		'err2':e2,
		'err3':e3,
		'err_apply_to':eto, 
		'job_val':$('#sperrlog_calc1').text(), 
		'err_val':$('#sperrlog_calc2').text(),
		'notes':$('#sperr_notes').val(),
		'notes2':$('#sperrlog_notes2').val(),
		'filed':a,
		'approved':b,
		'returned':c,
		'id':$('#selectedID').val(),
		'user':crnusr
	}; 
};