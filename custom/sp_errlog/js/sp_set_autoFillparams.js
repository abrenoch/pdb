function sp_errloader(data) {
		
	$errorFieldID = "sperrlog_err1";
	$errorFieldPRNT = "sperrlog_Serror01";
	
	$("#"+$errorFieldPRNT+" span").remove();

	for (r=0; r<data.length; r++) {
		if (data[r] != 0) {
			jQuery.grep($errList, function(obj1) {
				if (obj1.id === data[r]){
					var ui = {"item":{"label":obj1.error,"value":obj1.error}}
					var friend = ui.item.value,
						span = $("<span>").text(friend),
						a = $("<a>").addClass("remove").attr({
							href: "javascript:",
							title: "Remove " + friend
						}).text("x").appendTo(span);
					span.insertBefore("#"+$errorFieldID); //add friend to friend div
					$("#"+$errorFieldID).val("").css("top", 2);		
				} 
			});
		}		
	};
		
	$(".remove", document.getElementById($errorFieldPRNT)).live("click", function(){ //add live handler for clicks on remove links
		$(this).parent().remove(); //remove current friend
		//correct 'to' field position
		if($("#"+$errorFieldPRNT+" span").length === 0) {
			$("#"+$errorFieldID).css("top", 0);
		}				
	});				
};
	