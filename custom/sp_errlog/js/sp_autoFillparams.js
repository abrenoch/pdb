	//PRE-CODE TO DETERMINE WHAT TEXT INPUT TO USE
	function sep_selectedEF(alt,blt) {
		//alert("DD");
		$colorFieldID = alt;
		$colorFieldPRNT = blt;
		//alert($colorFieldID+$colorFieldPRNT);
		$("#"+$colorFieldID).autocomplete();
		
		};
		
	//AUTO FILL CODE
	function sep_autoFill() {	
			$(function(){
				//alert("working0")
				//var selectedElement = options[selectedIndex].id
				//alert(selectedElement)
				
				$("#"+$colorFieldID).autocomplete({ //attach autocomplete listener
				
					//EACH INLINE COMMAND SEEMS TO BE PART OF ".autocomplete" AND DEFINED BY JQUERY
					source: function(req, add){ //define callback to format results
						    var term = {
								term: req.term,
								exists: countErrors(),
							};   
						$.getJSON("custom/sp_errlog/autoFill.php?callback=?", term, function(data) { //pass request to server
							//console.log(data)
							var suggestions = []; //create array for response objects
							//process response
							$.each(data, function(i, val){								
								suggestions.push(val.error);
							});
						add(suggestions); //pass array to callback
						});
					},
					
					//
					select: function(e, ui) { //define select handler
						//create formatted friend
						var f = ui.item.value,
							span = $("<span>").text(f),
							a = $("<a>").addClass("remove").attr({
								href: "javascript:",
								title: "Remove " + f
							}).text("x").appendTo(span);
						span.insertBefore("#"+$colorFieldID); //add friend to friend div

					},
					
					//
					change: function() { //define select handler
						$("#"+$colorFieldID).val("").css("top", 2); //clears input field of prior text and corrects position
						countErrors();
					}
					
				});
				//				
				
				$("#"+$colorFieldPRNT).click(function(){ 
					$("#"+$colorFieldID).focus(); 
				});
				//
				
				$(".remove", document.getElementById($colorFieldPRNT)).live("click", function(){ //add live handler for clicks on remove links
					$(this).parent().remove(); //remove current friend
					//correct 'to' field position
					if($("#"+$colorFieldPRNT+" span").length === 0) {
						$("#"+$colorFieldID).css("top", 0);
						countErrors();
					}				
				});				
			});
	};		