function sperrlog_load(data){
	console.log("FORM / sp_errlog: loading job: "+data.art_num+" /ID: "+data.id)
	//console.log(data)
	$('#selectedID').val(data.id);
	
	var fus = false;
	
	$("#sperrlog_sepper").find("option").each(function(){
     	if( $(this).val() == data.sep_by ) {
    		$(this).attr("selected","selected");
    		fus = true;
    	}
	});
	
	if (data.checked_by) {
		$("#sperrlog_checker").find("option").each(function(){
			if( $(this).val() == data.checked_by ) {
				$(this).attr("selected","selected");
			}
		});
	} else {
		$("#sperrlog_checker").val($crntUSR);
	};
	
	
	if(fus != true){ERR_notify('The user "'+data.sep_by+'" does not seem to be a registered separator. Canceling job load.'); console.log("FORM / sp_errlog: can not load, separator not found in database"); return;}	
	
	$('#wgt_sperrlog2 div div.inner-spacer').slideUp('normal');
	$('#wgt_sperrlog1 div div.inner-spacer').slideUp('normal', function(){

		jQuery.grep($jobList, function(obj1) {
    		if (obj1.indicator === data.job_type){
    			if (obj1.value === data.job_value){	
    				$('#sperrlog_jtype').val(obj1.parent);
    				$('#sperrlog_jtype').trigger("change")   				
    				$("#sperrlog_jtypev option").filter(function() {
						return $(this).text() == obj1.type; 
					}).prop('selected', true);
    			}   
    		}
		});

		$('input[name=sperrlog_art]').val(data.art_num);
		$('input[name=sperrlog_concept]').val(data.concept_num);

		if (data.sep_date != '<i style="opacity:0.6">(none)</i>') {
			var d1 = moment( data.sep_date, "YYYY-MM-DD" );
				if (d1){
					var parsedDate1 = $.datepicker.parseDate('mm-dd-yy', d1.format('MM-DD-YYYY'));	
					$('#sperrlog_sepdate').datepicker('setDate', parsedDate1);
				} else {
					$('#sperrlog_sepdate').val('');
				}
		} else {
			$('#sperrlog_sepdate').val('');
		}
			
		if (data.ship_date != '<i style="opacity:0.6">(none)</i>') {	
			var d2 = moment( data.ship_date, "YYYY-MM-DD" );	
				if (d2){
					var parsedDate2 = $.datepicker.parseDate('mm-dd-yy', d2.format('MM-DD-YYYY'));
					$('#sperrlog_shipdate').datepicker('setDate', parsedDate2);
				} else {
					$('#sperrlog_shipdate').val('');
				}
		}
		
		$('#sperrlog_errto').val(data.err_type);
		$('#sperrlog_calc1').empty().text( data.job_value );
		$('#sperrlog_calc2').empty().text( data.err_value );
		
		var errAry = [data.err0, data.err1, data.err2, data.err3];
		sp_errloader(errAry);
		countErrors()
		
		if (data.notes != '<i style="opacity:0.6">(none)</i>') {	
			$('#sperr_notes').val(data.notes);
		} else {
			$('#sperr_notes').val('');
		}
		
		if (data.notes2 != '') {	
			$('#sperrlog_notes2').val(data.notes2);
			$('#sperrlog_notes2').slideDown('normal');
			$('#sperrlog_notesBTN2').empty().html('<span class="pencil-10 plix-10"></span>Remove Notes');
		} else {
			$('#sperrlog_notes2').val('');
			$('#sperrlog_notes2').hide();
			$('#sperrlog_notesBTN2').empty().html('<span class="pencil-10 plix-10"></span>Add Notes');
		}		
		
		if (data.filed != 0) {	
			$('#sperr_filed_tgl').prop('checked', true);
		} else {
			$('#sperr_filed_tgl').prop('checked', false);
		}

		if (data.approved != 0) {	
			$('#sperr_apprvd_tgl').prop('checked', true);
		} else {
			$('#sperr_apprvd_tgl').prop('checked', false);
		}

		if (data.returned_to != 0) {	
			$('#sperr_rtrnd_tgl').prop('checked', true);
		} else {
			$('#sperr_rtrnd_tgl').prop('checked', false);
		}
	
		$('#sperr_sub_button').css('display','none');
		$('#sperr_upd_button').css('display','inline');
		$('#sperr_clr_button').css('display','inline');
		
		$('#wgt_sperrlog1 div div.inner-spacer').css('background-color','rgba(255,0,0,0.2)');
		$('#wgt_sperrlog2 div div.inner-spacer').css('background-color','rgba(255,0,0,0.2)');	
		
		$('#wgt_sperrlog1 div div.inner-spacer, #wgt_sperrlog2 div div.inner-spacer').slideDown();
		
	});
	
}

function sperrlog_clear(){

	$('#wgt_sperrlog2 div div.inner-spacer').slideUp('normal');
	$('#sperrlog_notes2').val('').slideUp('normal');
	$('#wgt_sperrlog1 div div.inner-spacer').slideUp('normal', function(){
		$("#sperrlog_Serror01 span").remove();
		$('#selectedID').val('');
		$("#sperrlog_sepper").val('');
		$("#sperrlog_checker").val($crntUSR);
		$('#sperrlog_jtype').val('').trigger("change");
		$('input[name=sperrlog_art]').val('');
		$('input[name=sperrlog_concept]').val('');
		$('#sperrlog_sepdate').val('');
		$('#sperrlog_shipdate').val('');
		$('#sperrlog_errto').val('')
		$('#sperrlog_calc1').empty().text( '0.00' );
		$('#sperrlog_calc2').empty().text( '0.00' );
		$('#sperr_notes').val('');
		$('#sperr_notes2').val('');		
		$('#sperr_sub_button').css('display','inline');
		$('#sperr_upd_button').css('display','none');
		$('#sperr_clr_button').css('display','none');
		$('#sperr_filed_tgl').prop('checked', false);
		$('#sperr_apprvd_tgl').prop('checked', false);
		$('#sperr_rtrnd_tgl').prop('checked', false);
		
		
		$('#sperrlog_notesBTN2').empty().html('<span class="pencil-10 plix-10"></span>Add Notes');

		$('#wgt_sperrlog1 div div.inner-spacer').css('background-color','rgba(0,0,0,0)');
		$('#wgt_sperrlog2 div div.inner-spacer').css('background-color','rgba(0,0,0,0)');
	
		$('#wgt_sperrlog1 div div.inner-spacer, #wgt_sperrlog2 div div.inner-spacer').slideDown();
	});
	
}