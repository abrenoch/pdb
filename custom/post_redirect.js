function loadPOST(id) {
	$(".tipsy").remove();
	$("#main-menu ul li").each(function() { $(this).removeClass('page-active') });
	//DETECTS SUB-MENU ITEM SELECTED
	if ($("#"+id).parent().parent().attr('id') != 'main-menu' ){
			var curID = $("#"+id).parent().parent().attr('id');
			$("#main-menu").children('ul').children('li').each(function() { 
									if ($(this).hasClass("sub-page-active") && $(this).attr('id') != curID){
										$(this).children('ul').animate({height: 'toggle'},400);
										$(this).removeClass("sub-page-active");
										$(this).children('a').children('span').children('span').removeClass('min-10 plix-10');
										$(this).children('a').children('span').children('span').addClass('plus-10 plix-10');
									} else if ($(this).hasClass("sub-page-active") == false && $(this).attr('id') == curID){
										$(this).children('ul').animate({height: 'toggle'},400);
										$(this).addClass("sub-page-active");
										$(this).children('a').children('span').children('span').removeClass('plus-10 plix-10');
										$(this).children('a').children('span').children('span').addClass('min-10 plix-10');
									} 
								});
	//IF NOT SUB-MENU ITEM THEN...	
	} else {
		//CHECKS EACH MENU ITEM FOR OPEN SUBMENUS, AND CLOSES THEM
		$("#main-menu").children('ul').children('li').each(function() { 
									if ($(this).hasClass("sub-page-active")){
										$(this).children('ul').animate({height: 'toggle'},400);
										$(this).removeClass("sub-page-active");
										$(this).children('a').children('span').children('span').removeClass('min-10 plix-10');
										$(this).children('a').children('span').children('span').addClass('plus-10 plix-10');
									}
								});
	}	
	$("#"+id).addClass('page-active');	
	$('#content-main-inner').fadeTo(200, 0, function() {
		$('#content-main-inner').empty();
		
		
		$.ajax('custom/'+id+'/'+id+'.html', {
			statusCode: {
				404: function() {
					$('#content-main-inner').load('custom/404/404.html', function() {
						var h2s = $("#pgheader").text();
						$("#bcLast").empty();
						$("#bcLast").text(h2s);
						$(".error-box center strong").empty();
						$(".error-box center strong").append(id);
						$('#content-main-inner').fadeTo(300, 1);	
					});			
				},
				200: function() {
					$('#content-main-inner').load('custom/'+id+'/'+id+'.html', function() {
						var h2s = $("#pgheader").text();
						$("#bcLast").empty();
						$("#bcLast").text(h2s);
						$('#content-main-inner').fadeTo(300, 1);
						
						//CHECKS FOR LOCALSTORAGE SETTING FOR ROW/GRID VIEW 	
						if (localStorage.getItem('bodyView')) {
							if (localStorage.getItem('bodyView') == 'grid') {
								$('.changeto-grid').click();
							} else if (localStorage.getItem('bodyView') == 'rows') {
								$('.changeto-rows').click();
							}
						} else{
							$('.changeto-grid').addClass('selected');
							localStorage.setItem('bodyView','grid');
						};
						
					});				  		
				}
			}
		});
		
		
		  
	});	
};

