											<!-- SIDEBAR -->
<aside>
											<!-- SIDEBAR PROFILE -->
	<div id="sidebar-profile">
		<div id="main-avatar">
			<!--<span class="indicator">38</span>-->
				<img src="images/avatar.jpg" alt=""/>
	    </div>
	    <div id="profile-info">
	    	<div>
	        	<a href="javascript:void(0);"> <b> 
	        	<?php 
	        	echo "<div>";
	        	echo $_SESSION['first_name']." ".$_SESSION['last_name'];
	        	
	        	echo "</div>";
	        	
	        	?> </b> </a>
	        	<?php 
	        		echo "\r\n(".$_SESSION['usr'].")";
	        	
	        	?>
	            <!--<a href="javascript:void(0);">Profile settings</a>
	            <a href="javascript:void(0);">Server settings</a>
	            <a href="javascript:void(0);">Logout</a>-->
	    	</div>
		</div>
	</div> 
											<!-- MAIN MENU --> 	<!-- TAKE NOTE OF THE ID STRUCTURE OF EACH ITEM, MUST MATCH FOR A FUNCTIONAL SIDEBAR -->
	<nav id="main-menu">
		<ul>
			<!-- DASHBOARD --> 	
			<li id="dashboard"><a href="javascript:void(0);" onClick=loadPOST("dashboard")><span class="home-16 plix-16"></span> Dashboard</a></li>
			
			<!-- MERCHANDISING TOOLS --> 
			<?php  
				if($_SESSION['dep'] == 'mrc' || $_SESSION['dep'] == 'adm' || ($_SESSION['dep'] == 'crt' && $_SESSION['perms'] > 6) ) {
					echo '<li id="itm_merchandising"><a href="javascript:void(0);"><span class="document-16 plix-16"></span>Merchandising<span class="button-icon"><span id="itm_merchandising_spn" class="plus-10 plix-10"></span></span></a>';
					
					if($_SESSION['perms'] > 0) echo '<ul>';
                	if($_SESSION['perms'] > 0) echo '<li id="ca_request"><a href="javascript:void(0);" onClick=loadPOST("ca_request")>New Creative Art Request</a></li>';
                	
                	if($_SESSION['perms'] > 0) echo '<li id="ca_search"><a href="javascript:void(0);" onClick=loadPOST("ca_search")>Search Creative Art Requests</a></li>';
                	
                	if($_SESSION['perms'] > 0) echo '<li id="ca_workload"><a href="javascript:void(0);" onClick=loadPOST("ca_workload")>Workload Scheduler</a></li>';
               	 	               	 	
                	if($_SESSION['perms'] > 0) echo '<li id="ca_tables1"><a href="javascript:void(0);" onClick=loadPOST("ca_tables1")>Creative Request Tables</a></li>';
					
					if($_SESSION['perms'] > 0) echo '</ul>';
                
                	echo '</li>';
                
                };
            ?>
			
			<!-- CREATIVE TOOLS --> 
			<?php  
				if($_SESSION['dep'] == 'crt' || $_SESSION['dep'] == 'adm' || ($_SESSION['mrc'] == 'crt' && $_SESSION['perms'] > 7) ) {
					echo '<li id="itm_creative"><a href="javascript:void(0);"><span class="pencil-16 plix-16"></span>Creative<span class="button-icon"><span id="itm_creative_spn" class="plus-10 plix-10"></span></span></a>';
					
					if($_SESSION['perms'] > 0) echo '<ul>';
                	
                	if($_SESSION['perms'] > 0) echo '<li id="ca_tables1"><a href="javascript:void(0);" onClick=loadPOST("ca_tables1")>Creative Request Tables</a></li>';
                	
                	if($_SESSION['perms'] > 0) echo '<li id="ca_search"><a href="javascript:void(0);" onClick=loadPOST("ca_search")>Search Creative Art Requests</a></li>';
					
					if($_SESSION['perms'] > 0) echo '</ul>';
                
                	echo '</li>';
                
                };
            ?>   
			
			<!-- SEPARATIONS TOOLS --> 
			<?php  
				if($_SESSION['dep'] == 'sep' || $_SESSION['dep'] == 'adm') {
					echo '<li id="itm_separations"><a href="javascript:void(0);"><span class="ruler-16 plix-16"></span>Separations<span class="button-icon"><span id="itm_separations_spn" class="plus-10 plix-10"></span></span></a>';
					
					if($_SESSION['perms'] > 0) echo '<ul>';
					
						if($_SESSION['perms'] > 0) echo '<li id="sp_errlog"><a href="javascript:void(0);" onClick=loadPOST("sp_errlog")>Data Logging</a></li>';
						
						if($_SESSION['perms'] > 6) echo '<li id="sp_statistics"><a href="javascript:void(0);" onClick=loadPOST("sp_statistics")>Statistics</a></li>';

               	 	if($_SESSION['perms'] > 0) echo '</ul>';
                
                	echo '</li>';
                
                };
            ?>            
            
			
            
                                    	
            <!-- EXAMPLES AND TEST -->                       	
			<?php if($_SESSION['dep'] == 'adm' && $_SESSION['perms'] > 99) echo '<li id="example_charts"><a href="javascript:void(0);" onClick=loadPOST("example_charts")><span class="stats2-16 plix-16"></span>Example Charts</a></li>'; ?>
			<?php if($_SESSION['dep'] == 'adm' && $_SESSION['perms'] > 99) echo '<li id="restricted_content"><a href="javascript:void(0);" onClick=loadPOST("restricted_content")><span class="blocks2-16 plix-16"></span>Restricted Page</a></li>'; ?>
			
		</ul>
	</nav>                              
</aside>

                   							<!-- sidebar meta stats 
<div id="sidebar-meta">
	<div id="sidebar-meta-inner">
    	<div>
        	<p class="left">Space</p> 
            <p class="right">4,551 MB / 10 GB</p>
       </div>
       
    	<div class="pbar">
    		<span style="width:50%"> </span>                            
   		</div>
   		
    	<div>
			<p class="left">Traffic</p> 
        	<p class="right">8,001 MB / 10 GB</p>
    	</div>
    	
		<div class="pbar">
			<span style="width:81%"></span>                            
    	</div>
    </div>  
</div>-->

<script>


$(document).ready(function($){

if($crntDEP == "adm"){
	var sform = 'ajax/sep-mainsearch-results.php';
} else if($crntDEP == "mrc"){
	var sform = 'ajax/mrc-mainsearch-results.php';
} else if($crntDEP == "crt"){
	var sform = 'ajax/mrc-mainsearch-results.php';
} else if($crntDEP == "sep"){
	var sform = 'ajax/sep-mainsearch-results.php';
}

	$('form#mainsearch').eLiveSearch({
		file: sform,
		target: 'aside',
		maxResults: 10,
		order: 'random',
		live: true,
		minChar: 3,
		liveDelay: 500,
		effect: 'slide',
		speed: 400,
		closeClass: 'close-search',
		param1: '',
		param2: '',
		param3: '',
		param4: '',
		param5: '',
		param6: '',
		param7: '',
		param8: '',
		param9: '',
		param10: '',
		afterLoad: function(){ }
	});
$('nav#main-menu').eMainMenu({
	activeClass: 'sub-page-active',
	closeClass: 'min-10',
	openClass: 'plus-10',
	speed: 400
});

});
</script>