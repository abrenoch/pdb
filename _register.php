<?php


define('INCLUDE_CHECK',true);

require 'custom/connect.php';
require 'custom/emailFunctions.php';
// Those two files can be included only if INCLUDE_CHECK is defined


session_name('tzLogin');
// Starting the session

session_set_cookie_params(2*7*24*60*60);
// Making the cookie live for 2 weeks

session_start();





if(isset($_POST['submit'])=='Register')
{
	// If the Register form has been submitted
	
	$err = array();
	
	
	if(!checkEmail($_POST['pdb_email']))
	{
		$err[]='Your email is not valid!';
	}
	
	$prnchk = explode("@", $_POST['pdb_email']);
	
	if(isset($prnchk[1])!='perrinwear.com')
	{
		$err[]='Your email domain is not valid!';
	}	
	
	
	if(!count($err))
	{
		// If there are no errors
		
		$pass = substr(md5($_SERVER['REMOTE_ADDR'].microtime().rand(1,100000)),0,6);
		// Generate a random password
		
		$_POST['pdb_email'] = mysql_real_escape_string($_POST['pdb_email']);
		$newname = mysql_real_escape_string($prnchk[0]);
		// Escape the input data
		
		
		mysql_query("	INSERT INTO tz_members(usr,pass,email,regIP,dt)
						VALUES(
						
							'".$newname."',
							'".md5($pass)."',
							'".$_POST['pdb_email']."',
							'".$_SERVER['REMOTE_ADDR']."',
							NOW()
							
						)");
		
		if(mysql_affected_rows($link)==1)
		{
			send_mail(	'do-not-reply',
						$_POST['pdb_email'],
						'Registration System  - Your New Password',
						'Your password is: '.$pass);

			$_SESSION['msg']['reg-success']='An email has been sent with your new password!';
		}
		else $err[]='This username is already taken!';
	}

	if(count($err))
	{
		$_SESSION['msg']['reg-err'] = implode('<br />',$err);
	}	
	
	header("Location: register.php");
	exit;
}

$script = '';

if(isset($_SESSION['msg']))
{
	// The script below shows the sliding panel on page load
	
	$script = '
	<script type="text/javascript">
	
		$(function(){
		
			$("div#panel").show();
			$("#toggle a").toggle();
		});
	
	</script>';
	
}
?>



<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="ie ie6 lte9 lte8 lte7 no-js"> <![endif]-->
<!--[if IE 7]>     <html class="ie ie7 lte9 lte8 lte7 no-js"> <![endif]-->
<!--[if IE 8]>     <html class="ie ie8 lte9 lte8 no-js">      <![endif]-->
<!--[if IE 9]>     <html class="ie ie9 lte9 no-js">           <![endif]-->
<!--[if gt IE 9]>  <html class="no-js">                       <![endif]-->
<!--[if !IE]><!--> <html class="no-js">                       <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>PERRIN - Dashboard</title>
       
    <!-- // Mobile meta/files // -->

	<!-- For third-generation iPad with high-resolution Retina display: -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png">
    <!-- For iPhone 4with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/mobile/apple-touch-icon-114x114.png" />
    <!-- For first-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/mobile/apple-touch-icon-72x72.png" />
    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" href="images/mobile/apple-touch-icon.png" />
    <!-- For nokia devices: -->
    <link rel="shortcut icon" href="images/apple-touch-icon.png" />
    <!-- 320x460 for iPhone 3GS -->
    <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and not (-webkit-min-device-pixel-ratio: 2)" href="images/mobile/splash-320x460.png" />
    <!-- 640x920 for retina display -->
    <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" href="images/mobile/splash-640x920-retina.png" />
    <!-- iPad Portrait 768x1004 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 768px) and (orientation: portrait)" href="images/mobile/splash-768x1004.png" />
    <!-- iPad Landscape 1024x748 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 768px) and (orientation: landscape)" href="images/mobile/splash-1024x748.png" />
    <!-- iPad 3 Portrait 1536x2008 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 1536px) and (orientation: portrait)" href="images/mobile/splash-1536x2008-retina.png" />
    <!-- iPad 3 Landscape 2048x1536 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 2048px) and (orientation: landscape)" href="images/mobile/splash-2048x1496-retina.png" />
    <!-- Transform to webapp: -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Fullscreen mode: -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Viewport for older phones - http://davidbcalhoun.com/tag/handheldfriendly -->
    <meta name="HandheldFriendly" content="true"/>   
    <!-- Viewport - http://davidbcalhoun.com/tag/handheldfriendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> 
    <!-- This file contains some fixes, splash screen and web app code --> 
    <script src="js/mobiledevices.js"></script>
    
    <!-- // Internet Explore // -->
    
    <!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
    <meta name="application-name" content="Elite Admin Skin">
    <meta name="msapplication-tooltip" content="Cross-platform admin skin.">
    <meta name="msapplication-starturl" content="http://themes.creativemilk.net/elite/html/index.html">
    <!-- These custom tasks are examples, you need to edit them to show actual pages -->
    <meta name="msapplication-task" content="name=Home;action-uri=http://themes.creativemilk.net/elite/html/index.html;icon-uri=http://themes.creativemilk.net/elite/html/images/favicons/favicon.ico">
    <meta http-equiv="cleartype" content="on" /> 
    
    <!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->
            
    <!-- // Stylesheets // -->

    <!-- Framework -->
    <link rel="stylesheet" href="css/framework.css"/>
    <!-- Core -->
    <link rel="stylesheet" href="css/login.css"/>
    <!-- Styling -->
    <link rel="stylesheet" href="css/theme/darkblue.css" id="themesheet"/>
	<!--[if IE 7]>
	<link rel="stylesheet" href="css/destroy-ie6-ie7.css"/>
    <![endif]-->
      
    <!-- // Misc // -->
    
    <link rel="shortcut icon" href="images/favicons/favicon.ico">
    
    <!-- // jQuery/UI core // -->
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script>!window.jQuery && document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')</script>
    <script src="http://code.jquery.com/ui/1.8.22/jquery-ui.min.js"></script>
    <script>!window.jQueryUI && document.write('<script src="js/jquery-ui-1.8.22.min.js"><\/script>')</script>
    
    <!-- // Plugins // -->    
                      
    <!-- Touch helper -->  
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <!-- Stylesheet switcher --> 
    <script src="js/e_styleswitcher.1.1.min.js"></script>   
    <!-- Checkbox solution -->
    <script src="js/e_checkbox.1.0.min.js"></script>       
    <!-- Tabs -->
    <script src="js/e_tabs.1.1.min.js"></script>
    <!-- Menu -->
    <script src="js/e_menu.1.1.min.js"></script>
    <!-- Contact form with validation -->
    <script src="js/e_contactform.1.1.min.js"></script>    
    <!-- Show password -->     
    <script src="js/e_showpassword.1.0.min.js"></script>  
    <!-- Tooltip -->               
    <script src="js/tipsy.js"></script>      
    <!-- Plugins and custom code -->     
    <script src="js/login.js"></script>  
    
    <!-- // HTML5/CSS3 support // -->

    <script src="js/modernizr.min.js"></script>
    

                
</head>
<body>  
 
 
    <div id="login">
    
    	<!-- Put your logo here -->
    	<div id="logo" align="center">
        	<img class="logo_img"/>
        </div>
        
                    <?php
			if(isset($_SESSION['msg']['reg-err']))
				{
					echo '<div class="g_1">
            			<div class="dialog error">
                		<p>'.$_SESSION['msg']['reg-err'].'</p>
               			<span>x</span>
           				</div>
       					</div> ';
						unset($_SESSION['msg']['reg-err']);
	
				}
			if(isset($_SESSION['msg']['reg-success']))
				{
					echo '<div class="g_1">
            			<div class="dialog success">
                		<p>'.$_SESSION['msg']['reg-success'].'</p>
               			<span>x</span>
           				</div>
       					</div> ';
						unset($_SESSION['msg']['reg-success']);
	
				}
					?>
		
		
        <!-- Show a dialog 
        <div class="g_1">
            <div class="dialog error">
                <p>A good login page for mobile devices</p>
                <span>x</span>
            </div>
        </div>-->

        <!-- The main part -->                   
        <div id="login-outher">        
            <div id="login-inner">
                <header>
                    <h2>Registration</h2> 
                    <!--
                    <ul class="e-splitmenu" id="login-lang">
                        <li><span>English</span><a href="javascript:void(0);"><img src="images/icons/flags/gb.png" alt=""/></a>
                        
                             <div>
                                <ul>
                                    <li><a href="index.html"><img src="images/icons/flags/gb.png" alt=""/> English</a></li>
                                    <li><a href="index.html"><img src="images/icons/flags/de.png" alt=""/> German</a></li>
                                    <li><a href="index.html"><img src="images/icons/flags/es.png" alt=""/> Spanish</a></li>
                                </ul>                                      
                            </div>                               

                        </li>
                    </ul> 
                    --!>                                
                </header>
                
                <div id="login-content">
                    <form method="post" action="register.php" id="login-form">
                        
                        <div class="g_1">
                            <label for="field1">Perrin Email</label>
                        </div>
                        
                        
                        <div class="g_1">                            
                            <input type="text" name="pdb_email" id="pdb_email" tabindex="1" data-validation-type="present"/>
                        </div>
                        
                        
                        
                        
                        
                        <div class="spacer-20"><!-- spacer 20px --></div> 
                        
                        
                        
                         <div class="g_1">
 
                            
                            <input type="submit" name="submit" value="Register" tabindex="4" class="button-text"/>
                            
                        </div>               
                    </form>
				</div><!-- End #login-content --> 
            </div><!-- End #login-inner -->                                  
        </div><!-- End #login-outher --> 
        

        
        <!-- place your copyright text here
        <footer id="footer">
        	Copyright © 2012 - Template by <a href="http://www.creativemilk.net">www.creativemilk.net</a>
        </footer>  -->
    </div><!-- End "#login" -->        
</body>
</html>