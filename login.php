<?php
//write test

define('INCLUDE_CHECK',true);

require 'custom/connect.php';
require 'custom/emailFunctions.php';
// Those two files can be included only if INCLUDE_CHECK is defined


session_name('tzLogin');
// Starting the session

session_set_cookie_params(2*7*24*60*60);
// Making the cookie live for 2 weeks

session_start();


if(isset($_SESSION['id']) && !isset($_COOKIE['tzRemember']) && !$_SESSION['rememberMe'])
{
	// If you are logged in, but you don't have the tzRemember cookie (browser restart)
	// and you have not checked the rememberMe checkbox:

	$_SESSION = array();
	session_destroy();
	
	// Destroy the session
}


if(isset($_GET['logoff']))
{
	$_SESSION = array();
	//session_destroy();
	
	header("Location: login.php");
	exit;
}

if(isset($_POST['submitREG'])=='Register')
{
	$err = array();

	if(!count($err))
	{
		// If there are no errors
		
		$pass = substr(md5($_SERVER['REMOTE_ADDR'].microtime().rand(1,100000)),0,6);
		// Generate a random password
		
		//$_POST['pdb_email'] = mysql_real_escape_string($_POST['pdb_email']);
		$_POST['pdb_fname'] = mysql_real_escape_string($_POST['pdb_fname']);
		$_POST['pdb_lname'] = mysql_real_escape_string($_POST['pdb_lname']);
		$_POST['pdb_dep'] = mysql_real_escape_string($_POST['pdb_dep']);
		//$newname = mysql_real_escape_string($prnchk[0]);
		$newname = substr($_POST['pdb_fname'], 0, 1).$_POST['pdb_lname'];
		$newname = strtolower($newname);
		$name_email = $newname."@perrinwear.com";
		
		// Escape the input data
		
		
		mysql_query("	INSERT INTO tz_members(usr,pass,first_name,last_name,email,regIP,dt,dep)
						VALUES(
						
							'".$newname."',
							'".md5($pass)."',
							'".$_POST['pdb_fname']."',
							'".$_POST['pdb_lname']."',
							'".$name_email."',
							'".$_SERVER['REMOTE_ADDR']."',
							NOW(),
							'".$_POST['pdb_dep']."'
							
						)");
		
		if(mysql_affected_rows($link)==1)
		{
			send_mail(	"do-not-reply",
						$name_email,
						"Registration System  - Your New Username and Password",
						"Your username is: ".$newname."\r\n".
						"Your password is: ".$pass."\r\n".
						"Login at: http://".$_SERVER['SERVER_ADDR']."/login.php");

			$_SESSION['msg']['reg-success']='An email has been sent with your login details';
		}
		else $err[]='User already exists within database';
	}

	if(count($err))
	{
		$_SESSION['msg']['reg-err'] = implode('<br />',$err);
	}	
	
	header("Location: login.php");
	exit;
}

else if(isset($_POST['submit'])=='Login')
{
	// Checking whether the Login form has been submitted
	
	$err = array();
	// Will hold our errors
	
	
	if(!$_POST['pdb_username'] || !$_POST['pdb_password'])
		$err[] = 'All the fields must be filled in';
	
	if(!count($err))
	{
		$_POST['pdb_username'] = mysql_real_escape_string($_POST['pdb_username']);
		$_POST['pdb_password'] = mysql_real_escape_string($_POST['pdb_password']);
		$_POST['rememberMe'] = (int)$_POST['rememberMe'];
		
		// Escaping all input data

		$row = mysql_fetch_assoc(mysql_query("SELECT id,usr,first_name,last_name,avatar,perms,dep FROM tz_members WHERE usr='{$_POST['pdb_username']}' AND pass='".md5($_POST['pdb_password'])."'"));

		if($row['usr'])
		{
			// If everything is OK login
			
			$_SESSION['usr']=$row['usr'];
			$_SESSION['first_name']=$row['first_name'];
			$_SESSION['last_name']=$row['last_name'];
			$_SESSION['id'] = $row['id'];
			$_SESSION['avatar'] = $row['avatar'];
			$_SESSION['perms'] = $row['perms'];
			$_SESSION['dep'] = $row['dep'];
			
			$_SESSION['rememberMe'] = $_POST['rememberMe'];
			
			// Store some data in the session
			
			//setcookie('tzRemember',$_POST['rememberMe']);
			if(isset($_SESSION['fwd'])){
				header("Location: ".$_SESSION['fwd']);
			} else {
				header("Location: index.php");
			}
			
			exit;
		}
		else $err[]='Wrong username and/or password';
	}
	
	if($err)
	$_SESSION['msg']['login-err'] = implode('<br />',$err);
	// Save the error messages in the session

	header("Location: login.php");
	exit;
}

$script = '';

if(isset($_SESSION['msg']))
{
	// The script below shows the sliding panel on page load
	
	$script = '
	<script type="text/javascript">
	
		$(function(){
		
			$("div#panel").show();
			$("#toggle a").toggle();
		});
	
	</script>';
	
}
?>



<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="ie ie6 lte9 lte8 lte7 no-js"> <![endif]-->
<!--[if IE 7]>     <html class="ie ie7 lte9 lte8 lte7 no-js"> <![endif]-->
<!--[if IE 8]>     <html class="ie ie8 lte9 lte8 no-js">      <![endif]-->
<!--[if IE 9]>     <html class="ie ie9 lte9 no-js">           <![endif]-->
<!--[if gt IE 9]>  <html class="no-js">                       <![endif]-->
<!--[if !IE]><!--> <html class="no-js">                       <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>PERRIN - Dashboard</title>
       
    <!-- // Mobile meta/files // -->

	<!-- For third-generation iPad with high-resolution Retina display: -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png">
    <!-- For iPhone 4with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/mobile/apple-touch-icon-114x114.png" />
    <!-- For first-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/mobile/apple-touch-icon-72x72.png" />
    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" href="images/mobile/apple-touch-icon.png" />
    <!-- For nokia devices: -->
    <link rel="shortcut icon" href="images/apple-touch-icon.png" />
    <!-- 320x460 for iPhone 3GS -->
    <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and not (-webkit-min-device-pixel-ratio: 2)" href="images/mobile/splash-320x460.png" />
    <!-- 640x920 for retina display -->
    <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" href="images/mobile/splash-640x920-retina.png" />
    <!-- iPad Portrait 768x1004 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 768px) and (orientation: portrait)" href="images/mobile/splash-768x1004.png" />
    <!-- iPad Landscape 1024x748 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 768px) and (orientation: landscape)" href="images/mobile/splash-1024x748.png" />
    <!-- iPad 3 Portrait 1536x2008 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 1536px) and (orientation: portrait)" href="images/mobile/splash-1536x2008-retina.png" />
    <!-- iPad 3 Landscape 2048x1536 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 2048px) and (orientation: landscape)" href="images/mobile/splash-2048x1496-retina.png" />
    <!-- Transform to webapp: -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Fullscreen mode: -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Viewport for older phones - http://davidbcalhoun.com/tag/handheldfriendly -->
    <meta name="HandheldFriendly" content="true"/>   
    <!-- Viewport - http://davidbcalhoun.com/tag/handheldfriendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> 
    <!-- This file contains some fixes, splash screen and web app code --> 
    <script src="js/mobiledevices.js"></script>
    
    <!-- // Internet Explore // -->
    
    <!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
    <meta name="application-name" content="Elite Admin Skin">
    <meta name="msapplication-tooltip" content="Cross-platform admin skin.">
    <meta name="msapplication-starturl" content="http://themes.creativemilk.net/elite/html/index.html">
    <!-- These custom tasks are examples, you need to edit them to show actual pages -->
    <meta name="msapplication-task" content="name=Home;action-uri=http://themes.creativemilk.net/elite/html/index.html;icon-uri=http://themes.creativemilk.net/elite/html/images/favicons/favicon.ico">
    <meta http-equiv="cleartype" content="on" /> 
    
    <!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->
            
    <!-- // Stylesheets // -->

    <!-- Framework -->
    <link rel="stylesheet" href="css/framework.css"/>
    <!-- Core -->
    <link rel="stylesheet" href="css/login.css"/>
    <!-- Styling -->
    <link rel="stylesheet" href="css/theme/darkblue.css" id="themesheet"/>
	<!--[if IE 7]>
	<link rel="stylesheet" href="css/destroy-ie6-ie7.css"/>
    <![endif]-->
      
    <!-- // Misc // -->
    
    <link rel="shortcut icon" href="images/favicons/favicon.ico">
    
    <!-- // jQuery/UI core // -->
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <script>!window.jQuery && document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')</script>
    <script src="http://code.jquery.com/ui/1.8.22/jquery-ui.min.js"></script>
    <script>!window.jQueryUI && document.write('<script src="js/jquery-ui-1.8.22.min.js"><\/script>')</script>
    
    <!-- // Plugins // -->
    
    <!-- JQuery Cookie 
    <script src="js/jquery.cookie.js"></script> -->
                      
    <!-- Touch helper -->  
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <!-- Stylesheet switcher --> 
    <script src="js/e_styleswitcher.1.1.js"></script>    
    <!-- Checkbox solution -->
    <script src="js/e_checkbox.1.0.min.js"></script>       
    <!-- Tabs -->
    <script src="js/e_tabs.1.1.min.js"></script>
    <!-- Menu -->
    <script src="js/e_menu.1.1.min.js"></script>
    <!-- Contact form with validation -->
    <script src="js/e_contactform.1.1.min.js"></script>    
    <!-- Show password -->     
    <script src="js/e_showpassword.1.0.min.js"></script>  
    <!-- Tooltip -->               
    <script src="js/tipsy.js"></script>      
    <!-- Plugins and custom code -->     
    <script src="js/login.js"></script>  
    
    <!-- // HTML5/CSS3 support // -->

    <script src="js/modernizr.min.js"></script>
    

                
</head>
<body>  
 

 
    <div id="login-combi" style="width:350px">
    
    	<!-- Put your logo here -->
    	<div id="logo" align="center">
        	<center><div id="perrin_logo" class="logo_img" style="margin:0 auto 0 auto; float:none"></div></center>
        </div>
        
        <?php
			if(isset($_SESSION['msg']['login-err']))
				{
					echo '<div class="g_1">
            			<div class="dialog error">
                		<p>'.$_SESSION['msg']['login-err'].'</p>
               			<span>x</span>
           				</div>
       					</div> ';
						unset($_SESSION['msg']['login-err']);
				}
			
			if(isset($_SESSION['msg']['reg-err']))
				{
					echo '<div class="g_1">
            			<div class="dialog error">
                		<p>'.$_SESSION['msg']['reg-err'].'</p>
               			<span>x</span>
           				</div>
       					</div> ';
						unset($_SESSION['msg']['reg-err']);
	
				}
			if(isset($_SESSION['msg']['reg-success']))
				{
					echo '<div class="g_1">
            			<div class="dialog success">
                		<p>'.$_SESSION['msg']['reg-success'].'</p>
               			<span>x</span>
           				</div>
       					</div> ';
						unset($_SESSION['msg']['reg-success']);
	
				}
				
		?>

        
        <!-- The main part -->                   
        <div id="login-outher">        
            <div id="login-inner">
                <header>
                    <h2>User Login/Registration</h2>
                    <ul class="etabs">
                        <li class="etabs-active"><a href="#etab1">Login</a></li>
                        <li><a href="#etab2">Register</a></li>
                    </ul>                                  
                </header>
                
                <div id="login-content">
                    <div id="etab1" class="etabs-content">
                    
                    
                    
                        <form method="post" action="login.php" id="login-form">
                            <div class="g_1_3">
                                <label for="pdb_username">Username</label>
                            </div>
                            <div class="g_2_3_last">                             
                                <input type="text" name="pdb_username" id="pdb_username" tabindex="1" data-validation-type="present"/>
                            </div>
                            
                            <div class="spacer-20"><!-- spacer 20px --></div> 
                            
                            <div class="g_1_3">
                                <label for="pdb_password">Password</label>
                            </div>
                            <div class="g_2_3_last">    
                                <input type="password" name="pdb_password" id="pdb_password" tabindex="2" data-validation-type="present"/> 
                            </div>
                            
                            <div class="spacer-20"><!-- spacer 20px --></div> 
                            
                            <div class="g_2_3_last"> 
                            	<!--
                                <div class="remember-box">
                                    <input type="checkbox" name="rememberMe" id="rememberMe" tabindex="3" value="1"/>
                                    <label for="rememberMe">Remember me</label>
                                </div>
                                -->
                                <input type="submit" name="submit" value="Login" tabindex="4" class="button-text"/>
                            </div>               
                        </form>
                        
                        
                        
                        
                    </div><!-- End tab -->
                    <div id="etab2" class="etabs-content">
                        <form action="login.php" method="post" id="login-form">
							<!--
                            <div class="g_1_3">
                                <label for="field14">Perrin Email</label>
                            </div>
                            <div class="g_2_3_last">    
                                <input type="text" name="pdb_email" id="pdb_email" data-validation-type="present"/>
                            </div>
							
                            <div class="spacer-20"></div> 
							-->
							
                            <div class="g_1_3">
                                <label for="field14">First Name</label>
                            </div>
                            <div class="g_2_3_last">    
                                <input type="text" name="pdb_fname" id="pdb_fname" data-validation-type="present"/>
                            </div>													
							
                            <div class="spacer-20"><!-- spacer 20px --></div>

                            <div class="g_1_3">
                                <label for="field14">Last Name</label>
                            </div>
                            <div class="g_2_3_last">    
                                <input type="text" name="pdb_lname" id="pdb_lname" data-validation-type="present"/>
                            </div>													
							
                            <div class="spacer-20"><!-- spacer 20px --></div>
							
                            <div class="g_1_3">
                                <label for="field14">Department</label>
                            </div>
                            <div class="g_2_3_last">    
                                <select  name="pdb_dep" id="pdb_dep">
									<option value="crt">Creative</option>
									<option value="mrc">Merchandising</option>
									<option value="sep">Separations</option>
								</select>
                            </div>													
							
                            <div class="spacer-20"><!-- spacer 20px --></div>							
							
							
							
                            <div class="g_2_3_last"> 

                                <input type="submit" name="submitREG" value="Register" class="button-text"/>
                            </div>               
                        </form>
                    </div><!-- End tab -->
				</div><!-- End #login-content --> 
            </div><!-- End #login-inner -->                                  
        </div><!-- End #login-outher -->        
    </div><!-- End "#login" -->        
</body>
</html>