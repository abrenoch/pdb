<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/sql_params.php";
   include_once($path);

	$dbtable   = 'ca_request';       // database table name
	$dbcolumn1 = 'new_art_concept_num';        // search in column?
	$dbcolumn2 = 'submitted_by';      // search in column?
	$dbcolumn3 = 'due_date';      // search in column?	
	$dbcolumn4 = 'project_name';      // search in column?	
	
   /* ---------------------------------------------------- *
    * Misc settings                                        *
    * ---------------------------------------------------- *
	* These settings are some savety settings and some     *
	* text labels.                                         *
	* ---------------------------------------------------- */
	
	$searchresults   = 'Search results';       // search results text
	$noresultsfound  = 'No results found!';    // no results found text
	$readmore        = 'Open';            // read more text
	$advancedsearch  = 'Advanced search';      // advanced search text
	$maxresults      = 100;                    // maxium of results
	$exitlink        = 'index.php?formURL=adv_search';          // exit link

   /* ---------------------------------------------------- *
    * Connect to the database                              *
    * ---------------------------------------------------- *
	* Create a connection to the MYSQL database. The       * 
	* settings can be found in the 'Connect settings'      * 
	* setion at the top.                                   *
	* ---------------------------------------------------- */
	
	mysql_connect(DB_HOST, DB_USER, DB_PASS) or die(mysql_error());

   /* ---------------------------------------------------- *
    * Select the right databse                             *
    * ---------------------------------------------------- *
	* Connect to the MYSQL database.                       *
	* ---------------------------------------------------- */
	
	mysql_select_db(DB_CAREQUEST_TABLE) or die(mysql_error());
	
   /* ---------------------------------------------------- *
    * jQuery plugin values and settings                    *
    * ---------------------------------------------------- *
	* This is where the values and settings are set as php *
	* variables. You can use up to 5 extra parameters,     *
	* which you can use as filters.                        *
	* ---------------------------------------------------- */
	
	$searchword = $_POST['value'];
	$order      = $_POST['order'];
	$limit      = $_POST['limit'];
	// not used for this file
	// $param1     = $_POST['param1'];
	// $param2     = $_POST['param2'];
	// $param3     = $_POST['param3'];
	// $param4     = $_POST['param4'];
	// $param5     = $_POST['param5'];
	// $param6     = $_POST['param6'];
	// $param7     = $_POST['param7'];
	// $param8     = $_POST['param8'];
	// $param9     = $_POST['param9'];
	// $param10    = $_POST['param10'];	
	
   /* ---------------------------------------------------- *
    * Display order                                        *
    * ---------------------------------------------------- *
	* Check and set the order, you can choose between,     *
	* ASC, DESC and random. Notice that it uses the column *
	* 'date' for the DESC and ASC order.                   *
	* ---------------------------------------------------- */
	
	$order = strtoupper($order);
	
	if($order == 'DESC'){
		$orderby = "due_date DESC";
	}elseif($order == 'ASC'){
		$orderby = 'due_date ASC';
	}else{
		$orderby = 'id DESC';
	}
	
   /* ---------------------------------------------------- *
    * Max results                                          *
    * ---------------------------------------------------- *
	* This is used as a savety filter, to prevent wrong    *
	* use of the plugin. The max has been set on 100, you  *
	* can change this in the 'Misc settings'.            *
	* ---------------------------------------------------- */
	
	if($limit <= $maxresults){
		$totalresults = $limit;
	}else{
		$totalresults = $maxresults;
	}
	
   /* ---------------------------------------------------- *
    * Database query                                       *
    * ---------------------------------------------------- *
	* Get all of the data from the database and put it in  * 
	* a array.                                             *
	* ---------------------------------------------------- */
	
	$query = mysql_query("SELECT t.* FROM 
						(SELECT `pid`, MAX(id) as id FROM ".$dbtable." GROUP BY `pid`) a  
						INNER JOIN ".$dbtable." t ON (t.id = a.id)
						
						WHERE ".$dbcolumn2."
						LIKE '%$searchword%' 
						OR ".$dbcolumn3."
						LIKE '%$searchword%' 
						OR t.".$dbcolumn1."
						LIKE '%$searchword%' 
						OR t.".$dbcolumn4."
						LIKE '%$searchword%' 																		  
						ORDER BY t.id
						") or die(mysql_error());
						  
   /* ---------------------------------------------------- *
    * Output                                               *
    * ---------------------------------------------------- *
	* Get all found data from the database and wrap it in  * 
	* html tags.                                           *
	* ---------------------------------------------------- */
	
	echo '<div id="mainsearch-results" class="clearfix">';
	echo '<span></span>';
	echo '<h3>'.$searchresults.'<span class="close-search"></span></h3>';
	echo '<div id="mainsearch-ajax" class="clearfix">';
	
		if (mysql_num_rows($query) > 0){
		
			$prevName = '';
			// loop all results
			while($results = mysql_fetch_array($query)){
				// available values
				// $results['id'];
				// $results['title'];
				// $results['sum'];
				// $results['content'];
				// $results['author'];
				// $results['category'];
				// $results['url'];
				// $results['thumb'];
				// $results['date'];
				
				$date = strtotime($results['timestamp']);
				$format_date = date("m-d-y", $date);				
				
				
				//if ($results['new_art_concept_num'] != $prevName){
				echo '<div>';
				echo '<table width="100%">';
				echo '<tr>';
				echo '<td width="100%">';
				echo '<div class="g_2_3">';
				
				if($results['new_art_concept_num']){
					echo '<b>'.$results['new_art_concept_num'].'</b></br>';	
				} else if($results['project_name']){
					echo '<b>'.$results['project_name'].'</b></br>';	
				}
			
				echo '<b>'.$results['new_art_concept_num'].'</b></br>';
				
				//echo '</td>';
				//echo '<td>';
				
				echo '<p align="left"><strong>Edited: </strong><i>'.$format_date.'</i></p>';
				echo '</div>';
				//echo '</td>';
				
				//echo '<td width="auto" align="right" style="vertical-align: middle;">';
				echo '<div class="g_1_3_last" style="margin-top:7px">';
				echo '<a href="javascript:void(0);" onClick=ca_request_onLoad('.$results['id'].')>'.$readmore.'</a>';
				echo '</div>';
				
				
				echo '</td>';
				echo '</tr>';
				echo '</table>';
				echo '</div>';
				//};
				
				$prevName = $results['new_art_concept_num'];
				
				
				
				
				
				
				
				
				
				
	
			}
		}else{
			echo '<div><p>'.$noresultsfound.'</p></div>';
		}

	echo '</div>';
	echo '<a href="javascript:void(0);" onClick=loadPOST("ca_search")>'.$advancedsearch.'</a>';
	echo '</div>';		
?>
