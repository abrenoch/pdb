<?php

   /* ---------------------------------------------------- *
    * PHP file used for the advanced search page           *    
	* ---------------------------------------------------- *
	* Before you can use the live search you must create a * 
	* database and use the table below or create your own  * 
	* table. After you have created a table change the     *
	* 'Connect settings' & 'Database settings'. This file  *
	* searches in the title and content columns. If you    *
	* want to change the output of the layout go to the    *
    * 'Output' at the bottom of this file and change the   * 
	* output.                                              *
	*                                                      *
	* The MYSQL file used for this file can befound in     *
	* the same folder as this php file.                    *
	*                                                      * 
	* Tested with PHP 5.3.0 & mysql 5.1.36                 *
	* ---------------------------------------------------- */
	
   /* ---------------------------------------------------- *
    * Connect settings                                     *
    * ---------------------------------------------------- *
	* Add your host, username and password.                *
	* ---------------------------------------------------- */
	
	$host     = 'localhost'; // host
	$user     = 'root'; // username
	$password = ''; // password
	
   /* ---------------------------------------------------- *
    * Database settings                                    *
    * ---------------------------------------------------- *
	* Add your database name, table name in which you      *
	* want to search and the column that you want to       *
	* search in.                                           *
	* ---------------------------------------------------- */
	
	$dbname  = 'artrqst'; // database name
	$dbtable = 'ca_request';       // database table name
	
   /* ---------------------------------------------------- *
    * Misc settings                                        *
    * ---------------------------------------------------- *
	* These settings are some savety settings and some     *
	* text labels.                                         *
	* ---------------------------------------------------- */
	
	$searchresults   = 'Search results';    // search results text
	$noresultsfound  = 'No results found!'; // no results found text
	$readmore        = 'Read more...';         // read more text
	$maxresults      = 100;                 // maxium of results

   /* ---------------------------------------------------- *
    * Connect to the database                              *
    * ---------------------------------------------------- *
	* Create a connection to the MYSQL database. The       * 
	* settings can be found in the 'Connect settings'      * 
	* setion at the top.                                   *
	* ---------------------------------------------------- */
	
	mysql_connect($host, $user, $password) or die(mysql_error());

   /* ---------------------------------------------------- *
    * Select the right databse                             *
    * ---------------------------------------------------- *
	* Connect to the MYSQL database.                       *
	* ---------------------------------------------------- */
	
	mysql_select_db($dbname) or die(mysql_error());
	
   /* ---------------------------------------------------- *
    * jQuery plugin values and settings                    *
    * ---------------------------------------------------- *
	* This is where the values and settings are set as php *
	* variables. You can use up to 5 extra parameters,     *
	* which you can use as filters.                        *
	* ---------------------------------------------------- */
	
	$searchword = $_POST['value'];
	// $order      = $_POST['order'];
	$limit      = 3;
	$param1     = $_POST['param1'];
	$param2     = $_POST['param2'];
	$param3     = $_POST['param3'];
	// $param4     = $_POST['param4'];
	// $param5     = $_POST['param5'];
	// $param6     = $_POST['param6'];
	// $param7     = $_POST['param7'];
	// $param8     = $_POST['param8'];
	// $param9     = $_POST['param9'];
	// $param10    = $_POST['param10'];
	
   /* ---------------------------------------------------- *
    * Display order                                        *
    * ---------------------------------------------------- *
	* Check and set the order, you can choose between,     *
	* ASC, DESC and random. Notice that it uses the column *
	* 'date' for the DESC and ASC order.                   *
	* ---------------------------------------------------- */
	
	//$param3 = strtoupper($param3);
	
	if($param3 == 'id_desc'){
		$orderby = "id DESC";
	}elseif($param3 == 'id_asc'){
		$orderby = 'id ASC';
	}elseif($param3 == 'dd_desc'){
		$orderby = 'due_date DESC';	
	}elseif($param3 == 'dd_asc'){
		$orderby = 'due_date ASC';	
	
	}else{
		$orderby = 'id ASC';
	}
	
   /* ---------------------------------------------------- *
    * Max results                                          *
    * ---------------------------------------------------- *
	* This is used as a savety filter, to prevent wrong    *
	* use of the plugin. The max has been set on 100, you  *
	* can change this in the 'Misc settings'.            *
	* ---------------------------------------------------- */
	
	if($limit <= $maxresults){
		$totalresults = $limit;
	}else{
		$totalresults = $maxresults;
	}
	
   /* ---------------------------------------------------- *
    * Database query                                       *
    * ---------------------------------------------------- *
	* Get all of the data from the database and put it in  * 
	* a array. We are going to loop the query, as we are   *
	* using the filter(this case a select element), this   *
	* isn't the best way to do this but we have to keep it *
	* simple.                                              *
	* ---------------------------------------------------- */
	
	switch($param1){
		case 'all':
        default: 
			$query = mysql_query("SELECT *
								  FROM ".$dbtable." 
								  WHERE new_art_concept_num
								  LIKE '%$searchword%'
								  OR assign_to
								  LIKE '%$searchword%'
								  OR submitted_by
								  LIKE '%$searchword%'	
								  OR name
								  LIKE '%$searchword%'	
								  OR edit_by
								  LIKE '%$searchword%'									  
								  OR due_date
								  LIKE '%$searchword%'
								  ORDER BY ".$orderby.""						  						    

								  ) or die(mysql_error());
								  
			break;	

		case 'new_art_concept_num':
		
			$query = mysql_query("SELECT *
								  FROM ".$dbtable." 
								  WHERE new_art_concept_num
								  LIKE '%$searchword%' 
								  ORDER BY ".$orderby."
								  LIMIT ".$totalresults) or die(mysql_error());
								  
			break;			

		case 'assign_to':
		
			$query = mysql_query("SELECT *
								  FROM ".$dbtable." 
								  WHERE assign_to
								  LIKE '%$searchword%' 
								  ORDER BY ".$orderby."
								  LIMIT ".$totalresults) or die(mysql_error());
								  
			break;	

		case 'submitted_by':
		
			$query = mysql_query("SELECT *
								  FROM ".$dbtable." 
								  WHERE submitted_by
								  LIKE '%$searchword%' 
								  ORDER BY ".$orderby."
								  LIMIT ".$totalresults) or die(mysql_error());
								  
			break;

		case 'due_date':

			$query = mysql_query("SELECT *
								  FROM ".$dbtable." 
								  WHERE due_date
								  LIKE '%$searchword%' 
								  ORDER BY ".$orderby."
								  LIMIT ".$totalresults) or die(mysql_error());
								  
			break;
															
		case 'edit_by':

			$query = mysql_query("SELECT *
								  FROM ".$dbtable." 
								  WHERE edit_by
								  LIKE '%$searchword%' 
								  ORDER BY ".$orderby."
								  LIMIT ".$totalresults) or die(mysql_error());
								  
			break;		
	
		case 'name':

			$query = mysql_query("SELECT *
								  FROM ".$dbtable." 
								  WHERE name
								  LIKE '%$searchword%' 
								  ORDER BY ".$orderby."
								  LIMIT ".$totalresults) or die(mysql_error());
								  
			break;		
	
	}
					  
   /* ---------------------------------------------------- *
    * Output                                               *
    * ---------------------------------------------------- *
	* Get all found data from the database and wrap it in  * 
	* html tags.                                           *
	* ---------------------------------------------------- */
	
	// available values inside the loop
	// $results['id'];
	// $results['title'];
	// $results['sum'];
	// $results['content'];
	// $results['author'];
	// $results['category'];
	// $results['url'];
	// $results['thumb'];
	// $results['date'];
			
	echo '<h2>'.$searchresults .'</h2>';	
	
		if (mysql_num_rows($query) > 0){
			$prevName = "";
        	if(($param2 == 'images') or ($param2 == 'imagestext')){
				echo '<ul class="results-thumbs">';
					// loop only images
					for($i = 1; $i <= 10; $i++){	
									
						while($results = mysql_fetch_array($query)){
						
							if ($results['new_art_concept_num'] != $prevName){
								echo '<li>';
								echo '<a href="index.php?formURL=ca_request&formID='.$results['id'].'">';
								echo '<img src="'.$results['image'].'"/>';
								echo '</a>';
								echo '</li>';
							};
								$prevName = $results['new_art_concept_num'];
							
						}	
					}
					
				echo '</ul>';
				echo '<hr/>';	
				
				// reset to the pointer back to the front
				mysql_data_seek($query, 0);						
			}
		
			if(($param2 == 'text') or ($param2 == 'imagestext')){
				$prevName = "";
				echo '<ul class="results-text">';
				// loop only text
				while($results = mysql_fetch_array($query)){
					if ($results['new_art_concept_num'] != $prevName){					
					// creating a nice format for the date output
					$date       = strtotime($results['created']);
					$date2       = strtotime($results['timestamp']);
					$format_date = date("F j, Y", $date);
					$format_date2 = date("F j, Y", $date2);

					echo '<li>';
					echo '<h3><a href="index.php?formURL=ca_request&formID='.$results['id'].'">'.$results['new_art_concept_num'].'</a></h3>';
					echo '<span><strong>Submitted: </strong>'.$format_date.' ['.$results['submitted_by'].'] <br/><strong> Edited: </strong>'.$format_date2.' ['.$results['edit_by'].']</span>';					
					echo '<p>'.$results['content'].'</p>';
					echo '<a href="'.$results['url'].'">'.$readmore.'</a>';
					echo '</li>';
					};
					$prevName = $results['new_art_concept_num'];
				}
				echo '</ul>';
			}

		}else{
			echo '<div><p>'.$noresultsfound.'</p></div>';
		}	

?>
