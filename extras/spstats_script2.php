<h2>Query Constructor: seps_check.error_logs date fix</h2>
<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/sql_params.php";
   include_once($path);
   $tablTarget = 'error_logs';
   $dbTarget = 'seps_check';
  if (!empty($_GET['act'])) {
	$count = $_GET['count'];
  	echo '<a href="spstats_script2.php">RELOAD SCRIPT</a><br><br>';
  	echo 'Running';
	$mysqli2 = new mysqli(DB_HOST, DB_USER, DB_PASS, $dbTarget);	
	if ($mysqli2->connect_error) {
		die('Connect Error (' . $mysqli2->connect_errno . ') '
		. $mysqli2->connect_error);
	}
	echo "Loading Jobs (limit ".$count.")";
	$query2 = $mysqli2->query("SELECT id,ship_date,check_date FROM `".$tablTarget."` WHERE ship_date LIKE '%/%' OR check_date LIKE '%/%' ORDER BY `id` ASC  LIMIT ".$count);
	while( $array2[] = $query2->fetch_object() );
	array_pop($array2);
	$output2 = array();
    foreach($array2 as $option2) {    
    	$output2[] = array("id" => $option2->id, "sDate" => $option2->ship_date, "cDate" => $option2->check_date );	
    	echo '.';
	}	
	echo "<br>".count($output2)." Jobs with wrong date format loaded into array<br><br>";
	echo "<b>Look over the output below for errors</b><br><br>";
	$fOutput1 = '';
	$fOutput2 = '';
	foreach($output2 as $option) : 
		$sDate = date('Y-m-d', strtotime($option['sDate']));
		$cDate = date('Y-m-d', strtotime($option['cDate']));
		echo $option['id'].'>.....'.$sDate.'.=.'.$option['sDate'].'.....|||||.....'.$cDate.'.=.'.$option['cDate'].'<br>';
		$fOutput1 .= " WHEN `id` = '".$option['id']."' THEN '".$sDate."'";		
		$fOutput2 .= " WHEN `id` = '".$option['id']."' THEN '".$cDate."'";
	endforeach;		
	$oM = "UPDATE `".$tablTarget."` SET ";
	$o1 = " ship_date = CASE ".$fOutput1." ELSE ship_date END";
	$o2 = " check_date = CASE ".$fOutput2." ELSE check_date END";	
	if(strlen($fOutput1) > 0) {
		$oM .= $o1;
	} else {
		die("No Jobs found with improper date formatting");
	}
	if(strlen($fOutput2) > 0) $oM .=", ".$o2;		
	echo '<br><br>Copy/Paste the following query:<br><br>';
	echo $oM;	
  } else {
?>
<form action="spstats_script2.php" method="get">
  <input type="hidden" name="act" value="run">
			<p>Define number of rows</p>
		<select name="count">
			<option value="50">50</option>
			<option value="100">100</option>
			<option value="250">250</option>
			<option value="500">500</option>
			<option value="1000">1000</option>
		</select>
  <input type="submit" value="Run Script">
</form>
<?php
  }
?>