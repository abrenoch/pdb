<h2>Query Constructor: seps_check.errors to [err0,err1,err2,err3]</h2>
<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/sql_params.php";
   include_once($path);
   $tablTarget = 'error_logs';
   $dbTarget = 'seps_check';
   
  if (!empty($_GET['act'])) {
	$count = $_GET['count'];
  	echo '<a href="spstats_script.php">RELOAD SCRIPT</a><br><br>';
  	echo 'Running';
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, 'seps_check');
	$mysqli2 = new mysqli(DB_HOST, DB_USER, DB_PASS, $dbTarget);	
	if ($mysqli->connect_error) {
		die('Connect Error (' . $mysqli->connect_errno . ') '
		. $mysqli->connect_error);
	}
	if ($mysqli2->connect_error) {
		die('Connect Error (' . $mysqli2->connect_errno . ') '
		. $mysqli2->connect_error);
	}
	$query = $mysqli->query("SELECT id,error FROM `error_types`");
	while( $array[] = $query->fetch_object() );
	array_pop($array);
	$output = array();
    foreach($array as $option) {    
    	$output[] = array("id" => $option->id, "error" => $option->error );	
    	echo '.';
	}
	echo "<br>".count($output)." Error types loaded into array<br><br>";
	echo "Loading Errors (limit ".$count.")";
	$query2 = $mysqli2->query("SELECT * FROM `".$tablTarget."` WHERE `errors` > '' AND err0 = 0 AND err1 = 0 AND err2 = 0 AND err3 = 0 ORDER BY `id` ASC  LIMIT ".$count);
	while( $array2[] = $query2->fetch_object() );
	array_pop($array2);
	$output2 = array();
    foreach($array2 as $option2) {    
    	$output2[] = array("id" => $option2->id, "errors" => explode(", ", $option2->errors) );	
    	echo '.';
	}	
	echo "<br>".count($output2)." Items with errors loaded into array<br><br>";
	$fOutput1 = '';
	$fOutput2 = '';
	$fOutput3 = '';
	$fOutput4 = '';
	foreach($output2 as $option3) : 
		$i = 0;
		foreach($option3['errors'] as $option4) :
			for ($m = 0; $m <= count($output); $m++){
				$errOpt = $output[$m];
				similar_text(strtoupper($option4), strtoupper($errOpt['error']), $similarity_pst);
				if( number_format($similarity_pst, 0) > 90 ){
					if($i == 0){
						$fOutput1 .= " WHEN `id` = '".$option3['id']."' THEN '".$errOpt['id']."'";
					} else if($i == 1){
						$fOutput2 .= " WHEN `id` = '".$option3['id']."' THEN '".$errOpt['id']."'";
					} else if($i == 2){
						$fOutput3 .= " WHEN `id` = '".$option3['id']."' THEN '".$errOpt['id']."'";
					} else if($i == 3){
						$fOutput4 .= " WHEN `id` = '".$option3['id']."' THEN '".$errOpt['id']."'";
					}
					break;
				}
			}
			$i++;
		endforeach;	
	endforeach;	
	$oM = "UPDATE `".$tablTarget."` SET ";
	$o1 = "err0 = CASE ".$fOutput1." ELSE err0 END";
	$o2 = " err1 = CASE ".$fOutput2." ELSE err1 END";
	$o3 = " err2 = CASE ".$fOutput3." ELSE err2 END";
	$o4 = " err3 = CASE ".$fOutput4." ELSE err3 END";
	if(strlen($fOutput1) > 0) {
		$oM .= $o1;
	} else {
		if(count($output2) > 0){
			echo '<b>Jobs found with errors, but could not be matched to errors on record: </b><br>';
			foreach($output2 as $dieput){
				echo $dieput['id'].' > '.strtoupper(implode(", ",$dieput['errors'])).'<br>';
			}
			die();
		} else {
			die('<b>No jobs with missing error fields found</b>');
		}
	}
	if(strlen($fOutput2) > 0) $oM .=", ".$o2;
	if(strlen($fOutput3) > 0) $oM .=", ".$o3;
	if(strlen($fOutput4) > 0) $oM .=", ".$o4;
	echo 'Copy/Paste the following query:<br><br>';
	echo $oM;
  } else {
?>
<form action="spstats_script.php" method="get">
  <input type="hidden" name="act" value="run">
			<p>Define number of rows</p>
		<select name="count">
			<option value="50">50</option>
			<option value="100">100</option>
			<option value="250">250</option>
			<option value="500">500</option>
			<option value="1000">1000</option>
		</select>
  <input type="submit" value="Run Script">
</form>
<?php
  }
?>