<?php
$html = '<div class="powerwidget" id="pw_sp_statistics0" data-widget-sortable="false" data-widget-load="custom/sp_statistics/sp_statistics_wrapper.html" role="widget">
                                    <header role="heading">
                                        <h2>Graphs &amp; Statistics</h2>  
										<i style="margin-left:5px; opacity:0.4">(By Date Checked)</i>
                                    <div class="powerwidget-ctrls" role="menu"><a href="#" class="button-icon powerwidget-refresh-btn"><span class="refresh-10 plix-10"></span></a>     <a href="#" class="button-icon powerwidget-toggle-btn"><span class="min-10 plix-10 "></span></a></div><span class="powerwidget-loader"></span></header>
                                    <div role="content">
                                    
                                        <div class="toolbar">
											<div class="left">
											    
											    <input type="checkbox" name="spstat_shwDate" id="spstat_shwDate" value="1" style="float:left">
                                        			<label for="spstat_shwDate" style="margin-right:5px"><span></span>Filter by Date Range</label>
                                                
                                                <div class="clearLeft550" style="width:450px">
													<input type="text" id="spstat_from_date" class="datepicker hasDatepicker" placeholder="From Date..." style="width: 30%; height: 28px; float: left; pointer-events: none; opacity: 0.4;">
													<b style="float:left; margin:5px 2px 0px 7px;">To</b>
													<input type="text" id="spstat_to_date" class="datepicker hasDatepicker" placeholder="To Date..." style="width: 30%; height: 28px; margin-bottom: 0px; float: left; pointer-events: none; opacity: 0.4;">
												</div>
	
                                            </div>
                                            <div class="right">
												
                                            </div>
                                            <!-- <div class="right">
                                                <input type="submit" id="flt_refresh" value="Filter" class="button-text"/>
                                            </div>End .right -->
                                        </div><!-- End .toolbar -->
										
										<div class="inner-spacer">  
											<div class="g_1">
												<div class="chart-wraper" style="min-width:250px; position: relative; border-radius:5px">
												<!--
												<select id="spstat_flot_type_view" class="eselect_sub" style="float:left; width:55px">
                                            		<option value="bar" >Bar</option>
                                            		<option value="pie">Pie</option>
                                            	</select>
                                            	
												<select id="spstat_flot_type" class="eselect_sub" style="float:left; margin-left:7px; margin-right:7px; width:175px">
                                            		<option value="count_total">Sum of Workload</option>
													<option value="job_total">Sum of Job Values</option>
													<option value="error_total">Sum of Error Values</option>
                                            	</select>-->
                                            	
                                            	
                                            	
													<!--<div id="flot_sp_statistics0_head" style="margin-left:50px; margin-bottom:15px"><h2></h2></div>
													<div style="height:50px"></div>-->
													<div class="g_1_3">
														<center><b>Sum of Workload</b></center>
														<div id="flot_sp_statistics0" class="graph" style="width: 100%; height: 250px; position: relative; padding: 0px;"><canvas class="base" width="330" height="250"></canvas><canvas class="overlay" width="330" height="250" style="position: absolute; left: 0px; top: 0px;"></canvas><div class="tickLabels" style="font-size:smaller"><div class="xAxis x1Axis" style="color:#545454"><div class="tickLabel" style="position:absolute;text-align:center;left:27px;top:230px;width:30px">Bill</div><div class="tickLabel" style="position:absolute;text-align:center;left:54px;top:230px;width:30px">Dave</div><div class="tickLabel" style="position:absolute;text-align:center;left:82px;top:230px;width:30px">Dan</div><div class="tickLabel" style="position:absolute;text-align:center;left:109px;top:230px;width:30px">Felicia</div><div class="tickLabel" style="position:absolute;text-align:center;left:136px;top:230px;width:30px">Joel</div><div class="tickLabel" style="position:absolute;text-align:center;left:164px;top:230px;width:30px">Jeff</div><div class="tickLabel" style="position:absolute;text-align:center;left:191px;top:230px;width:30px">Kathy</div><div class="tickLabel" style="position:absolute;text-align:center;left:218px;top:230px;width:30px">Mark</div><div class="tickLabel" style="position:absolute;text-align:center;left:245px;top:230px;width:30px">Nick</div><div class="tickLabel" style="position:absolute;text-align:center;left:273px;top:230px;width:30px">Rob</div><div class="tickLabel" style="position:absolute;text-align:center;left:300px;top:230px;width:30px">Tom</div></div><div class="yAxis y1Axis" style="color:#545454"><div class="tickLabel" style="position:absolute;text-align:right;top:213px;right:306px;width:24px">0</div><div class="tickLabel" style="position:absolute;text-align:right;top:189px;right:306px;width:24px">500</div><div class="tickLabel" style="position:absolute;text-align:right;top:164px;right:306px;width:24px">1000</div><div class="tickLabel" style="position:absolute;text-align:right;top:140px;right:306px;width:24px">1500</div><div class="tickLabel" style="position:absolute;text-align:right;top:116px;right:306px;width:24px">2000</div><div class="tickLabel" style="position:absolute;text-align:right;top:91px;right:306px;width:24px">2500</div><div class="tickLabel" style="position:absolute;text-align:right;top:67px;right:306px;width:24px">3000</div><div class="tickLabel" style="position:absolute;text-align:right;top:43px;right:306px;width:24px">3500</div><div class="tickLabel" style="position:absolute;text-align:right;top:18px;right:306px;width:24px">4000</div><div class="tickLabel" style="position:absolute;text-align:right;top:-6px;right:306px;width:24px">4500</div></div></div></div>
													</div>
													<div class="g_1_3">	
														<center><b>Sum of Job Values</b></center>
														<div id="flot_sp_statistics1" class="graph" style="width: 100%; height: 250px; position: relative; padding: 0px;"><canvas class="base" width="330" height="250"></canvas><canvas class="overlay" width="330" height="250" style="position: absolute; left: 0px; top: 0px;"></canvas><div class="tickLabels" style="font-size:smaller"><div class="xAxis x1Axis" style="color:#545454"><div class="tickLabel" style="position:absolute;text-align:center;left:27px;top:230px;width:30px">Bill</div><div class="tickLabel" style="position:absolute;text-align:center;left:54px;top:230px;width:30px">Dave</div><div class="tickLabel" style="position:absolute;text-align:center;left:82px;top:230px;width:30px">Dan</div><div class="tickLabel" style="position:absolute;text-align:center;left:109px;top:230px;width:30px">Felicia</div><div class="tickLabel" style="position:absolute;text-align:center;left:136px;top:230px;width:30px">Joel</div><div class="tickLabel" style="position:absolute;text-align:center;left:164px;top:230px;width:30px">Jeff</div><div class="tickLabel" style="position:absolute;text-align:center;left:191px;top:230px;width:30px">Kathy</div><div class="tickLabel" style="position:absolute;text-align:center;left:218px;top:230px;width:30px">Mark</div><div class="tickLabel" style="position:absolute;text-align:center;left:245px;top:230px;width:30px">Nick</div><div class="tickLabel" style="position:absolute;text-align:center;left:273px;top:230px;width:30px">Rob</div><div class="tickLabel" style="position:absolute;text-align:center;left:300px;top:230px;width:30px">Tom</div></div><div class="yAxis y1Axis" style="color:#545454"><div class="tickLabel" style="position:absolute;text-align:right;top:213px;right:306px;width:24px">0</div><div class="tickLabel" style="position:absolute;text-align:right;top:189px;right:306px;width:24px">1000</div><div class="tickLabel" style="position:absolute;text-align:right;top:164px;right:306px;width:24px">2000</div><div class="tickLabel" style="position:absolute;text-align:right;top:140px;right:306px;width:24px">3000</div><div class="tickLabel" style="position:absolute;text-align:right;top:116px;right:306px;width:24px">4000</div><div class="tickLabel" style="position:absolute;text-align:right;top:91px;right:306px;width:24px">5000</div><div class="tickLabel" style="position:absolute;text-align:right;top:67px;right:306px;width:24px">6000</div><div class="tickLabel" style="position:absolute;text-align:right;top:43px;right:306px;width:24px">7000</div><div class="tickLabel" style="position:absolute;text-align:right;top:18px;right:306px;width:24px">8000</div><div class="tickLabel" style="position:absolute;text-align:right;top:-6px;right:306px;width:24px">9000</div></div></div></div>
													</div>	
													<div class="g_1_3_last">	
														<center><b>Sum of Error Values</b></center>
														<div id="flot_sp_statistics2" class="graph" style="width: 100%; height: 250px; position: relative; padding: 0px;"><canvas class="base" width="330" height="250"></canvas><canvas class="overlay" width="330" height="250" style="position: absolute; left: 0px; top: 0px;"></canvas><div class="tickLabels" style="font-size:smaller"><div class="xAxis x1Axis" style="color:#545454"><div class="tickLabel" style="position:absolute;text-align:center;left:21px;top:230px;width:30px">Bill</div><div class="tickLabel" style="position:absolute;text-align:center;left:49px;top:230px;width:30px">Dave</div><div class="tickLabel" style="position:absolute;text-align:center;left:77px;top:230px;width:30px">Dan</div><div class="tickLabel" style="position:absolute;text-align:center;left:105px;top:230px;width:30px">Felicia</div><div class="tickLabel" style="position:absolute;text-align:center;left:133px;top:230px;width:30px">Joel</div><div class="tickLabel" style="position:absolute;text-align:center;left:161px;top:230px;width:30px">Jeff</div><div class="tickLabel" style="position:absolute;text-align:center;left:188px;top:230px;width:30px">Kathy</div><div class="tickLabel" style="position:absolute;text-align:center;left:216px;top:230px;width:30px">Mark</div><div class="tickLabel" style="position:absolute;text-align:center;left:244px;top:230px;width:30px">Nick</div><div class="tickLabel" style="position:absolute;text-align:center;left:272px;top:230px;width:30px">Rob</div><div class="tickLabel" style="position:absolute;text-align:center;left:300px;top:230px;width:30px">Tom</div></div><div class="yAxis y1Axis" style="color:#545454"><div class="tickLabel" style="position:absolute;text-align:right;top:213px;right:312px;width:18px">0</div><div class="tickLabel" style="position:absolute;text-align:right;top:198px;right:312px;width:18px">50</div><div class="tickLabel" style="position:absolute;text-align:right;top:184px;right:312px;width:18px">100</div><div class="tickLabel" style="position:absolute;text-align:right;top:169px;right:312px;width:18px">150</div><div class="tickLabel" style="position:absolute;text-align:right;top:155px;right:312px;width:18px">200</div><div class="tickLabel" style="position:absolute;text-align:right;top:140px;right:312px;width:18px">250</div><div class="tickLabel" style="position:absolute;text-align:right;top:125px;right:312px;width:18px">300</div><div class="tickLabel" style="position:absolute;text-align:right;top:111px;right:312px;width:18px">350</div><div class="tickLabel" style="position:absolute;text-align:right;top:96px;right:312px;width:18px">400</div><div class="tickLabel" style="position:absolute;text-align:right;top:82px;right:312px;width:18px">450</div><div class="tickLabel" style="position:absolute;text-align:right;top:67px;right:312px;width:18px">500</div><div class="tickLabel" style="position:absolute;text-align:right;top:52px;right:312px;width:18px">550</div><div class="tickLabel" style="position:absolute;text-align:right;top:38px;right:312px;width:18px">600</div><div class="tickLabel" style="position:absolute;text-align:right;top:23px;right:312px;width:18px">650</div><div class="tickLabel" style="position:absolute;text-align:right;top:9px;right:312px;width:18px">700</div><div class="tickLabel" style="position:absolute;text-align:right;top:-6px;right:312px;width:18px">750</div></div></div></div>
													</div>	
													<center><div id="hover" style="height:14px; width:auto;"><span style="font-weight: bold; color: #abf6a9">Tom Jenkins - 4128</span></div></center>
												</div>
												
											</div>
											
											<div class="clear" style="height:10px"><!-- New row --></div>
											
											<div class="g_1_2">
												<div style="background-color:rgba(0,0,0,0.5); position:relative; border-radius:5px">
													<div class="inner-spacer">  	
														<div id="spstats_info_1" style="text-align:center">
															<h2>Department Statistics</h2>
															<div style="height:5px"></div>
														</div>
														<hr style="background-color:white; opacity:0.5">
														
														<table style="width:100%">
															<tbody><tr>
																<td style="width:33%">
																	<b>Separation Accuracy:</b>
																</td>
																<td>
																	<div id="spstats-bar00" style="margin-bottom:3px"><div class="e-progressbar e-progressbar-small" data-progressbar-value="92.32" data-progressbar-threshold="0" data-progressbar-color="#14EB00"><span style="background-color: rgb(20, 235, 0); width: 92.32%;"><span style="color: rgb(34, 34, 34);">92.32%</span></span></div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<b>Workload Accuracy:</b>
																</td>
																<td>
																	<div id="spstats-bar01" style="margin-bottom:3px"><div class="e-progressbar e-progressbar-small" data-progressbar-value="75.63" data-progressbar-threshold="0" data-progressbar-color="#3EC100"><span style="background-color: rgb(62, 193, 0); width: 75.63%;"><span style="color: rgb(34, 34, 34);">75.63%</span></span></div></div>
																</td>
															</tr>
														
														</tbody></table>
														
														<div class="numHolder2" style="border-top:1px solid black; width:25%; height:40px; float:left">
															<center>
																<div style="opacity:0.4;"><small>Quantity</small></div>
																<div style="opacity:0.4; line-height:0px"><small>of Jobs</small></div>
																<div style="line-height:40px"><h2 id="spstat_info_01">23899</h2></div>
															</center>
														</div>

														<div class="numHolder2" style="border-top:1px solid black; border-left:1px solid black; width:25%; height:40px; float:left; margin-left:-1px">
															<center>
																<div style="opacity:0.4;"><small>Jobs Without</small></div>
																<div style="opacity:0.4; line-height:0px"><small>Corrections</small></div>
																<div style="line-height:40px"><h2 id="spstat_info_04">18076</h2></div>
															</center>
														</div>
														
														<div class="numHolder2" style="border-top:1px solid black; border-left:1px solid black; width:25%; height:40px; float:left; margin-left:-1px">
															<center>
																<div style="opacity:0.4;"><small>Total of</small></div>
																<div style="opacity:0.4; line-height:0px"><small>Job Values</small></div>
																<div style="line-height:40px"><h2 id="spstat_info_02">31817.5</h2></div>
															</center>
														</div>
														
														<div class="numHolder2" style="border-top:1px solid black; border-left:1px solid black; width:25%; height:40px; margin-bottom:7px; float:left; margin-left:-1px">
															<center>
																<div style="opacity:0.4;"><small>Total of</small></div>
																<div style="opacity:0.4; line-height:0px"><small>Error Values</small></div>
																<div style="line-height:40px"><h2 id="spstat_info_03">2443.25</h2></div>
															</center>
														</div>
														<div id="spstats_info_dep_more" style="top:70px; margin-top:60px;">
															<hr style="background-color:white; opacity:0.5">
															<center><h2 style="margin:10px 0px">Error Statistics</h2></center>
															<table id="spstats_dep_errtable" style="width:100%"><thead style="font-size:larger"><tr><td style="padding-bottom:5px"><b>Error Type</b></td><td><center><b>Count</b></center></td><td><center><b>Percentage</b></center></td></tr></thead><tbody><tr><td>Incorrect Mesh(s)</td><td><center>695</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="11.7458171370627" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 11.7458171370627%;"><span style="position:absolute; margin-left:5px">11.75%</span></span></div></div></td></tr><tr style=""><td>Missing/Excessive Screen(s)</td><td><center>620</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="10.478282913638669" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 10.478282913638669%;"><span style="position:absolute; margin-left:5px">10.48%</span></span></div></div></td></tr><tr style=""><td>Extra Swatch(s)</td><td><center>596</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="10.072671962142978" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 10.072671962142978%;"><span style="position:absolute; margin-left:5px">10.07%</span></span></div></div></td></tr><tr style=""><td>Live Type</td><td><center>430</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="7.267196214297786" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 7.267196214297786%;"><span style="position:absolute; margin-left:5px">7.27%</span></span></div></div></td></tr><tr style=""><td>Bad Base/Technique</td><td><center>423</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="7.148893020111544" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 7.148893020111544%;"><span style="position:absolute; margin-left:5px">7.15%</span></span></div></div></td></tr><tr style=""><td>Incorrect Placement</td><td><center>410</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="6.9291870880513775" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 6.9291870880513775%;"><span style="position:absolute; margin-left:5px">6.93%</span></span></div></div></td></tr><tr style=""><td>Incorrect Art or Concept #</td><td><center>313</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="5.2898428257562955" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 5.2898428257562955%;"><span style="position:absolute; margin-left:5px">5.29%</span></span></div></div></td></tr><tr style=""><td>Missing/Incorrect Flash(s)</td><td><center>187</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="3.1603853304039213" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 3.1603853304039213%;"><span style="position:absolute; margin-left:5px">3.16%</span></span></div></div></td></tr><tr style=""><td>Garment Swatch Not Process</td><td><center>164</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="2.771674835220551" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 2.771674835220551%;"><span style="position:absolute; margin-left:5px">2.77%</span></span></div></div></td></tr><tr style=""><td>Incorrect Hit Order</td><td><center>155</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="2.619570728409667" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 2.619570728409667%;"><span style="position:absolute; margin-left:5px">2.62%</span></span></div></div></td></tr><tr style=""><td>Missing/Incorrect Garment Color(s)</td><td><center>154</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="2.6026702720973467" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 2.6026702720973467%;"><span style="position:absolute; margin-left:5px">2.60%</span></span></div></div></td></tr><tr style=""><td>Bad Header and/or Art Placement</td><td><center>148</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="2.501267534223424" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 2.501267534223424%;"><span style="position:absolute; margin-left:5px">2.50%</span></span></div></div></td></tr><tr style=""><td>Incorrect Ink(s)</td><td><center>142</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="2.3998647963495015" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 2.3998647963495015%;"><span style="position:absolute; margin-left:5px">2.40%</span></span></div></div></td></tr><tr style=""><td>Mislabeled Screen(s)</td><td><center>140</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="2.3660638837248604" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 2.3660638837248604%;"><span style="position:absolute; margin-left:5px">2.37%</span></span></div></div></td></tr><tr style=""><td>Process Color(s)</td><td><center>128</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="2.163258407977015" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 2.163258407977015%;"><span style="position:absolute; margin-left:5px">2.16%</span></span></div></div></td></tr><tr style=""><td>Incorrectly Colored Screen(s)/Art</td><td><center>120</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="2.028054757478452" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 2.028054757478452%;"><span style="position:absolute; margin-left:5px">2.03%</span></span></div></div></td></tr><tr style=""><td>Art Not Flipped</td><td><center>117</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.9773533885414907" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.9773533885414907%;"><span style="position:absolute; margin-left:5px">1.98%</span></span></div></div></td></tr><tr style=""><td>Incorrect Art</td><td><center>111</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.8759506506675678" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.8759506506675678%;"><span style="position:absolute; margin-left:5px">1.88%</span></span></div></div></td></tr><tr><td>Missing/Incorrect Garment Style(s)</td><td><center>92</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.55484198073348" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.55484198073348%;"><span style="position:absolute; margin-left:5px">1.55%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Trap</td><td><center>92</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.55484198073348" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.55484198073348%;"><span style="position:absolute; margin-left:5px">1.55%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Check for Trademarks</td><td><center>79</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.3351360486733141" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.3351360486733141%;"><span style="position:absolute; margin-left:5px">1.34%</span></span></div></div></td></tr><tr><td>Art Error</td><td><center>69</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.1661314855501097" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.1661314855501097%;"><span style="position:absolute; margin-left:5px">1.17%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Base/Technique</td><td><center>59</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.9971269224269056" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.9971269224269056%;"><span style="position:absolute; margin-left:5px">1.00%</span></span></div></div></td></tr><tr style=""><td>Missing/Unnecessary Screen/Palette Warning</td><td><center>53</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.895724184552983" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.895724184552983%;"><span style="position:absolute; margin-left:5px">0.90%</span></span></div></div></td></tr><tr style=""><td>Old File(s) Not Deleted</td><td><center>51</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.8619232719283421" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.8619232719283421%;"><span style="position:absolute; margin-left:5px">0.86%</span></span></div></div></td></tr><tr><td>Bad Namedrop</td><td><center>47</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.7943214466790604" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.7943214466790604%;"><span style="position:absolute; margin-left:5px">0.79%</span></span></div></div></td></tr><tr><td>CS Error</td><td><center>46</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.77742099036674" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.77742099036674%;"><span style="position:absolute; margin-left:5px">0.78%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary 2nd or 3rd Set-Up</td><td><center>45</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.7605205340544194" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.7605205340544194%;"><span style="position:absolute; margin-left:5px">0.76%</span></span></div></div></td></tr><tr><td>Missing Sep</td><td><center>41</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.6929187088051377" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.6929187088051377%;"><span style="position:absolute; margin-left:5px">0.69%</span></span></div></div></td></tr><tr><td>Extra Layer(s)</td><td><center>30</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.507013689369613" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.507013689369613%;"><span style="position:absolute; margin-left:5px">0.51%</span></span></div></div></td></tr><tr><td>Missing/Moved Tiffs</td><td><center>29</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.4901132330572926" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.4901132330572926%;"><span style="position:absolute; margin-left:5px">0.49%</span></span></div></div></td></tr><tr><td>Missing/Incorrect Art Dimensions</td><td><center>25</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.4225114078080108" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.4225114078080108%;"><span style="position:absolute; margin-left:5px">0.42%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Overprint</td><td><center>23</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.38871049518337" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.38871049518337%;"><span style="position:absolute; margin-left:5px">0.39%</span></span></div></div></td></tr><tr><td>Artboard Too Small</td><td><center>22</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.3718100388710495" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.3718100388710495%;"><span style="position:absolute; margin-left:5px">0.37%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Technique Indicator</td><td><center>15</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.2535068446848065" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.2535068446848065%;"><span style="position:absolute; margin-left:5px">0.25%</span></span></div></div></td></tr><tr><td>Wrong Art Size</td><td><center>12</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.2028054757478452" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.2028054757478452%;"><span style="position:absolute; margin-left:5px">0.20%</span></span></div></div></td></tr><tr><td>Missing Tech Sheet</td><td><center>10</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.16900456312320433" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.16900456312320433%;"><span style="position:absolute; margin-left:5px">0.17%</span></span></div></div></td></tr><tr><td>Incorrect File Names</td><td><center>9</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.1521041068108839" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.1521041068108839%;"><span style="position:absolute; margin-left:5px">0.15%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Transparency</td><td><center>7</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.11830319418624302" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.11830319418624302%;"><span style="position:absolute; margin-left:5px">0.12%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Secondary Placement</td><td><center>4</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.06760182524928172" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.06760182524928172%;"><span style="position:absolute; margin-left:5px">0.07%</span></span></div></div></td></tr><tr><td>Merch Error</td><td><center>3</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.0507013689369613" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.0507013689369613%;"><span style="position:absolute; margin-left:5px">0.05%</span></span></div></div></td></tr><tr><td>Incorrect Halftone/Fill %</td><td><center>1</center></td><td><div class="dep_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.01690045631232043" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.01690045631232043%;"><span style="position:absolute; margin-left:5px">0.02%</span></span></div></div></td></tr></tbody></table>
														</div>
														
													</div>
												</div>

											</div><!-- section end -->
											
											<div class="g_1_2_last">
												<div style="background-color:rgba(0,0,0,0.5); position:relative; border-radius:5px;">
													<div class="inner-spacer" style="display: block;">  	
														<div id="spstats_info_0" style="text-align:center">
															<h2 style="color: rgb(100, 92, 148);">Nick Will s Statistics</h2>
															<div style="height:5px"></div>
														</div>
														<hr style="background-color: rgb(100, 92, 148); opacity: 1;">
														
														<table style="width:100%">
															<tbody><tr>
																<td style="width:33%">
																	<b>Separation Accuracy:</b>
																</td>
																<td>
																	<div id="spstats-bar0" style="margin-bottom:3px"><div class="e-progressbar e-progressbar-small" data-progressbar-threshold="15" data-progressbar-value="91.53" data-progressbar-color="#16E900"><span style="background-color: rgb(22, 233, 0); width: 91.53%;"><span style="color: rgb(34, 34, 34);">91.53%</span></span></div></div>
																</td>
															</tr>
															<tr>
																<td style="width:33%">
																	<b>Workload Accuracy:</b>
																</td>
																<td>
																	<div id="spstats-bar3" style="margin-bottom:3px"><div class="e-progressbar e-progressbar-small" data-progressbar-threshold="15" data-progressbar-value="75.97" data-progressbar-color="#3DC200"><span style="background-color: rgb(61, 194, 0); width: 75.97%;"><span style="color: rgb(34, 34, 34);">75.97%</span></span></div></div>
																</td>
															</tr>															
															<tr>
																<td>
																	<b>% of Dep. Workload:</b>
																</td>
																<td>
																	<div id="spstats-bar1" style="margin-bottom:3px"><div class="e-progressbar e-progressbar-small" data-progressbar-threshold="15" data-progressbar-value="14.70" data-progressbar-color="blue"><span style="background-color: blue; width: 14.7%;"><span style="position:absolute; margin-left:5px">14.70%</span></span></div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<b>% of Dep. Errors:</b>
																</td>
																<td>
																	<div id="spstats-bar2" style="margin-bottom:7px"><div class="e-progressbar e-progressbar-small" data-progressbar-threshold="15" data-progressbar-value="16.22" data-progressbar-color="Darkorange"><span style="background-color: rgb(255, 140, 0); width: 16.22%;"><span>16.22%</span></span></div></div>
																</td>
															</tr>
														</tbody></table>
														
														<div class="numHolder" style="border-top-width: 1px; border-top-style: solid; border-color: rgb(100, 92, 148); width: 25%; height: 40px; float: left;">
															<center>
																<div style="opacity:0.4;"><small>Quantity</small></div>
																<div style="opacity:0.4; line-height:0px"><small>of Jobs</small></div>
																<div style="line-height:40px"><h2 id="spstat_info_1">4278</h2></div>
															</center>
														</div>

														<div class="numHolder" style="border-top-width: 1px; border-top-style: solid; border-color: rgb(100, 92, 148); border-left-width: 1px; border-left-style: solid; width: 25%; height: 40px; float: left; margin-left: -1px;">
															<center>
																<div style="opacity:0.4;"><small>Jobs Without</small></div>
																<div style="opacity:0.4; line-height:0px"><small>Corrections</small></div>
																<div style="line-height:40px"><h2 id="spstat_info_4">3250</h2></div>
															</center>
														</div>
														
														<div class="numHolder" style="border-top-width: 1px; border-top-style: solid; border-color: rgb(100, 92, 148); border-left-width: 1px; border-left-style: solid; width: 25%; height: 40px; float: left; margin-left: -1px;">
															<center>
																<div style="opacity:0.4;"><small>Total of</small></div>
																<div style="opacity:0.4; line-height:0px"><small>Job Values</small></div>
																<div style="line-height:40px"><h2 id="spstat_info_2">4677</h2></div>
															</center>
														</div>
														
														<div class="numHolder" style="border-top-width: 1px; border-top-style: solid; border-color: rgb(100, 92, 148); border-left-width: 1px; border-left-style: solid; width: 25%; height: 40px; margin-bottom: 7px; float: left; margin-left: -1px;">
															<center>
																<div style="opacity:0.4;"><small>Total of</small></div>
																<div style="opacity:0.4; line-height:0px"><small>Error Values</small></div>
																<div style="line-height:40px"><h2 id="spstat_info_3">396.25</h2></div>
															</center>
														</div>
														<div id="spstats_info_more" style="top:70px; margin-top:60px;">
															<hr style="background-color: rgb(100, 92, 148);">
															<center><h2 style="margin: 10px 0px; color: rgb(100, 92, 148);">Error Statistics</h2></center>
															<table id="spstats_usr_errtable" style="width:100%"><thead style="font-size:larger"><tr><td style="padding-bottom:5px"><b>Error Type</b></td><td><center><b>Count</b></center></td><td><center><b>Percentage</b></center></td></tr></thead><tbody><tr><td>Incorrect Mesh(s)</td><td><center>193</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="15.871710526315788" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 15.871710526315788%;"><span style="position:absolute; margin-left:5px">15.87%</span></span></div></div></td></tr><tr><td>Missing/Excessive Screen(s)</td><td><center>140</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="11.513157894736842" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 11.513157894736842%;"><span style="position:absolute; margin-left:5px">11.51%</span></span></div></div></td></tr><tr style=""><td>Incorrect Art or Concept #</td><td><center>89</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="7.319078947368421" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 7.319078947368421%;"><span style="position:absolute; margin-left:5px">7.32%</span></span></div></div></td></tr><tr><td>Extra Swatch(s)</td><td><center>81</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="6.661184210526317" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 6.661184210526317%;"><span style="position:absolute; margin-left:5px">6.66%</span></span></div></div></td></tr><tr><td>Bad Base/Technique</td><td><center>75</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="6.167763157894736" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 6.167763157894736%;"><span style="position:absolute; margin-left:5px">6.17%</span></span></div></div></td></tr><tr style=""><td>Incorrect Placement</td><td><center>57</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="4.6875" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 4.6875%;"><span style="position:absolute; margin-left:5px">4.69%</span></span></div></div></td></tr><tr><td>Bad Header and/or Art Placement</td><td><center>56</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="4.605263157894736" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 4.605263157894736%;"><span style="position:absolute; margin-left:5px">4.61%</span></span></div></div></td></tr><tr><td>Incorrect Hit Order</td><td><center>53</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="4.358552631578947" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 4.358552631578947%;"><span style="position:absolute; margin-left:5px">4.36%</span></span></div></div></td></tr><tr><td>Live Type</td><td><center>46</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="3.7828947368421053" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 3.7828947368421053%;"><span style="position:absolute; margin-left:5px">3.78%</span></span></div></div></td></tr><tr><td>Garment Swatch Not Process</td><td><center>44</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="3.618421052631579" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 3.618421052631579%;"><span style="position:absolute; margin-left:5px">3.62%</span></span></div></div></td></tr><tr style=""><td>Missing/Incorrect Garment Color(s)</td><td><center>40</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="3.289473684210526" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 3.289473684210526%;"><span style="position:absolute; margin-left:5px">3.29%</span></span></div></div></td></tr><tr style=""><td>Incorrectly Colored Screen(s)/Art</td><td><center>29</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="2.384868421052632" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 2.384868421052632%;"><span style="position:absolute; margin-left:5px">2.38%</span></span></div></div></td></tr><tr style=""><td>Incorrect Ink(s)</td><td><center>26</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="2.138157894736842" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 2.138157894736842%;"><span style="position:absolute; margin-left:5px">2.14%</span></span></div></div></td></tr><tr><td>Process Color(s)</td><td><center>24</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.9736842105263157" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.9736842105263157%;"><span style="position:absolute; margin-left:5px">1.97%</span></span></div></div></td></tr><tr><td>Mislabeled Screen(s)</td><td><center>24</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.9736842105263157" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.9736842105263157%;"><span style="position:absolute; margin-left:5px">1.97%</span></span></div></div></td></tr><tr><td>Missing/Incorrect Garment Style(s)</td><td><center>23</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.8914473684210527" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.8914473684210527%;"><span style="position:absolute; margin-left:5px">1.89%</span></span></div></div></td></tr><tr><td>Missing/Incorrect Flash(s)</td><td><center>22</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.8092105263157896" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.8092105263157896%;"><span style="position:absolute; margin-left:5px">1.81%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Trap</td><td><center>21</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.7269736842105265" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.7269736842105265%;"><span style="position:absolute; margin-left:5px">1.73%</span></span></div></div></td></tr><tr><td>Art Error</td><td><center>18</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.4802631578947367" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.4802631578947367%;"><span style="position:absolute; margin-left:5px">1.48%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Check for Trademarks</td><td><center>17</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.3980263157894737" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.3980263157894737%;"><span style="position:absolute; margin-left:5px">1.40%</span></span></div></div></td></tr><tr><td>Incorrect Art</td><td><center>17</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.3980263157894737" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.3980263157894737%;"><span style="position:absolute; margin-left:5px">1.40%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Base/Technique</td><td><center>16</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.3157894736842104" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.3157894736842104%;"><span style="position:absolute; margin-left:5px">1.32%</span></span></div></div></td></tr><tr style=""><td>CS Error</td><td><center>16</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.3157894736842104" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.3157894736842104%;"><span style="position:absolute; margin-left:5px">1.32%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Screen/Palette Warning</td><td><center>15</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.2335526315789473" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.2335526315789473%;"><span style="position:absolute; margin-left:5px">1.23%</span></span></div></div></td></tr><tr><td>Art Not Flipped</td><td><center>13</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="1.069078947368421" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 1.069078947368421%;"><span style="position:absolute; margin-left:5px">1.07%</span></span></div></div></td></tr><tr><td>Bad Namedrop</td><td><center>10</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.8223684210526315" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.8223684210526315%;"><span style="position:absolute; margin-left:5px">0.82%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary 2nd or 3rd Set-Up</td><td><center>9</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.7401315789473684" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.7401315789473684%;"><span style="position:absolute; margin-left:5px">0.74%</span></span></div></div></td></tr><tr><td>Artboard Too Small</td><td><center>8</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.6578947368421052" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.6578947368421052%;"><span style="position:absolute; margin-left:5px">0.66%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Overprint</td><td><center>7</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.575657894736842" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.575657894736842%;"><span style="position:absolute; margin-left:5px">0.58%</span></span></div></div></td></tr><tr><td>Missing/Incorrect Art Dimensions</td><td><center>6</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.4934210526315789" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.4934210526315789%;"><span style="position:absolute; margin-left:5px">0.49%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Technique Indicator</td><td><center>5</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.41118421052631576" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.41118421052631576%;"><span style="position:absolute; margin-left:5px">0.41%</span></span></div></div></td></tr><tr><td>Missing/Moved Tiffs</td><td><center>5</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.41118421052631576" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.41118421052631576%;"><span style="position:absolute; margin-left:5px">0.41%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Transparency</td><td><center>3</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.24671052631578946" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.24671052631578946%;"><span style="position:absolute; margin-left:5px">0.25%</span></span></div></div></td></tr><tr><td>Extra Layer(s)</td><td><center>3</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.24671052631578946" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.24671052631578946%;"><span style="position:absolute; margin-left:5px">0.25%</span></span></div></div></td></tr><tr><td>Incorrect File Names</td><td><center>2</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.1644736842105263" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.1644736842105263%;"><span style="position:absolute; margin-left:5px">0.16%</span></span></div></div></td></tr><tr><td>Missing Tech Sheet</td><td><center>1</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.08223684210526315" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.08223684210526315%;"><span style="position:absolute; margin-left:5px">0.08%</span></span></div></div></td></tr><tr><td>Missing Sep</td><td><center>1</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.08223684210526315" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.08223684210526315%;"><span style="position:absolute; margin-left:5px">0.08%</span></span></div></div></td></tr><tr><td>Missing/Unnecessary Secondary Placement</td><td><center>1</center></td><td><div class="usr_err_bars"><div class="e-progressbar e-progressbar-small" data-progressbar-value="0.08223684210526315" data-progressbar-threshold="50" data-progressbar-color="red"><span style="background-color: red; width: 0.08223684210526315%;"><span style="position:absolute; margin-left:5px">0.08%</span></span></div></div></td></tr></tbody></table>
														</div>
													</div>
												</div>
											
											</div>
										</div>
                                        
                                        
                                        
                                        
                                    <div class="powerwidget-ajax-placeholder"></div></div>                                   
                                </div>';

?>





<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="ie ie6 lte9 lte8 lte7 no-js"> <![endif]-->
<!--[if IE 7]>     <html class="ie ie7 lte9 lte8 lte7 no-js"> <![endif]-->
<!--[if IE 8]>     <html class="ie ie8 lte9 lte8 no-js">      <![endif]-->
<!--[if IE 9]>     <html class="ie ie9 lte9 no-js">           <![endif]-->
<!--[if gt IE 9]>  <html class="no-js">                       <![endif]-->
<!--[if !IE]><!--> <html class="no-js">                       <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>PERRIN - Print Form</title>
       
    <!-- // Mobile meta/files // -->

	<!-- For third-generation iPad with high-resolution Retina display: -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png">
    <!-- For iPhone 4with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/mobile/apple-touch-icon-114x114.png" />
    <!-- For first-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/mobile/apple-touch-icon-72x72.png" />
    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" href="images/mobile/apple-touch-icon.png" />
    <!-- For nokia devices: -->
    <link rel="shortcut icon" href="images/apple-touch-icon.png" />
    <!-- 320x460 for iPhone 3GS -->
    <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and not (-webkit-min-device-pixel-ratio: 2)" href="images/mobile/splash-320x460.png" />
    <!-- 640x920 for retina display -->
    <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" href="images/mobile/splash-640x920-retina.png" />
    <!-- iPad Portrait 768x1004 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 768px) and (orientation: portrait)" href="images/mobile/splash-768x1004.png" />
    <!-- iPad Landscape 1024x748 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 768px) and (orientation: landscape)" href="images/mobile/splash-1024x748.png" />
    <!-- iPad 3 Portrait 1536x2008 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 1536px) and (orientation: portrait)" href="images/mobile/splash-1536x2008-retina.png" />
    <!-- iPad 3 Landscape 2048x1536 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 2048px) and (orientation: landscape)" href="images/mobile/splash-2048x1496-retina.png" />
    <!-- Transform to webapp: -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Fullscreen mode: -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Viewport for older phones - http://davidbcalhoun.com/tag/handheldfriendly -->
    <meta name="HandheldFriendly" content="true"/>   
    <!-- Viewport - http://davidbcalhoun.com/tag/handheldfriendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> 
    <!-- This file contains some fixes, splash screen and web app code --> 
    <script src="js/mobiledevices.js"></script>
    
    <!-- // Internet Explore // -->
    
    <!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
    <meta name="application-name" content="Elite Admin Skin">
    <meta name="msapplication-tooltip" content="Cross-platform admin skin.">
    <meta name="msapplication-starturl" content="http://themes.creativemilk.net/elite/html/index.php">
    <!-- These custom tasks are examples, you need to edit them to show actual pages -->
    <meta name="msapplication-task" content="name=Home;action-uri=http://themes.creativemilk.net/elite/html/index.php;icon-uri=http://themes.creativemilk.net/elite/html/images/favicons/favicon.ico">
    <meta http-equiv="cleartype" content="on" /> 
    
    <!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->
            
    <!-- // Stylesheets // -->

    <!-- Framework -->
    <link rel="stylesheet" href="css/framework.css"/>
    <!-- Main -->
    <link rel="stylesheet" href="css/style.css"/>
    <!-- jQuery UI --> 
    <link rel="stylesheet" href="css/ui/jquery.ui.base.css"/>
    
    <!-- Custom Styling -->

	<style type="text/css">
		.ztop {z-index:999;}
	</style>

    <link rel="stylesheet" href="css/theme/darkblue.css" id="themesheet"/>
    <link rel="shortcut icon" href="images/favicons/favicon.ico" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    
    
    <script>!window.jQuery && document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')</script>
    <script src="http://code.jquery.com/ui/1.8.22/jquery-ui.min.js"></script>
    <script>!window.jQueryUI && document.write('<script src="js/jquery-ui-1.8.22.min.js"><\/script>')</script>

    
    <!-- // Thirdparty plugins // -->
    
    <script src="https://towtruck.mozillalabs.com/towtruck.js"></script>
    
    <!-- Touch helper -->  
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <!-- MouseWheel -->  
    <script src="js/jquery.mousewheel.min.js"></script>
    <!-- UI Spinner -->
	<script src="js/jquery.ui.spinner.js"></script>
    <!-- Tooltip -->               
    <script src="js/tipsy.js"></script>
    <!-- Treeview -->                         
    <script src="js/treeview.js"></script>
    <!-- Calendar -->                         
    <script src="js/fullcalendar.min.js"></script> 
    <!-- selectToUISlider -->                
    <script src="js/selectToUISlider.jQuery.js"></script> 
    <!-- context Menu -->         
    <script src="js/jquery.contextMenu.js"></script> 
    <!-- File Explore -->              
    <script src="js/elfinder.min.js"></script> 
    <!-- AutoGrow Textarea -->                   
    <script src="js/autogrow-textarea.js"></script> 
    <!-- Resizable Textarea -->               
    <script src="js/textarearesizer.min.js"></script>
    <!-- HTML5 WYSIWYG -->  
    <script src="wysiwyghtml5/parser_rules/advanced.js"></script>
	<script src="wysiwyghtml5/dist/wysihtml5-0.3.0.js"></script>    
    <!-- Lightbox -->                      
    <script src="js/jquery.colorbox-min.mod.js"></script>
         
    <!-- Masked inputs -->
    <script src="js/jquery.maskedinput-1.3.min.js"></script> 
    <!-- IE7 JSON FIX -->
    <script src="js/json2.js"></script>
    <!-- HTML5 audio player -->
    <script src="audiojs/audiojs/audio.min.js"></script> 
             
    <!-- // Custom theme plugins // -->
    
    <!-- JQuery Cookie --> 
    <script src="js/jquery.dateFormat-1.0.js"></script>   
    <script src="js/moment.js"></script> 
    
    <!-- Stylesheet switcher --> 
    <script src="js/e_styleswitcher.1.1.js"></script>                 
    <!-- Widgets -->
    <script src="js/powerwidgets.2.3.js"></script>
    <!-- Widgets panel -->
    <script src="js/powerwidgetspanel.1.2.mod.js"></script>
    <!-- Select styling -->
    <script src="js/e_select.1.1.min.js"></script>    
    <!-- Checkbox solution -->
    <script src="js/e_checkbox.1.0.js"></script>
    <!-- Radio button replacement -->
    <script src="js/e_radio.1.0.min.js"></script>    
    <!-- Tabs -->
    <script src="js/e_tabs.1.1.min.js"></script>
    <!-- File styling -->
    <script src="js/e_file.1.0.min.js"></script>    
    <!-- MainMenu -->
    <script src="js/e_mainmenu.1.0.min.js"></script>
    <!-- Menu -->
    <script src="js/e_menu.1.1.min.js"></script>
    <!-- Input popup box -->
    <script src="js/e_inputexpand.1.0.min.js"></script>
    <!-- Progressbar -->
    <script src="js/e_progressbar.1.0.mod.js"></script>
    <!-- Scrollbar replacemt -->
    <script src="js/e_scrollbar.1.0.min.js"></script> 
    <!-- Onscreen keyboard -->
    <script src="js/e_oskeyboard.1.0.min.js"></script>
    <!-- Textarea limiter -->
    <script src="js/e_textarealimiter.1.0.min.js"></script>
    <!-- Contact form with validation -->
    <script src="js/e_contactform.1.1.min.js"></script>
    <!-- Responsive table helper -->
    <script src="js/e_responsivetable.1.0.min.js"></script>
    <!-- Gallery -->
    <script src="js/e_gallery.1.0.min.js"></script>
    <!-- Live search -->
    <script src="js/e_livesearch.1.0.custom.js"></script>
    <!-- Notify -->
    <script src="js/e_notify.1.0.min.js"></script>  
    <!-- Countdown -->  
    <script src="js/e_countdown.1.0.min.js"></script> 
    <!-- Clone script -->
    <script src="js/e_clone.1.0.custom.js"></script> 
    <!-- Chained inputs -->
    <script src="js/e_chainedinputs.1.0.min.js"></script>
    <!-- Show password -->     
    <script src="js/e_showpassword.1.0.min.js"></script>        
    <!-- All plugins are set here -->
    <script src="js/plugins.js"></script>
    <!-- Custom code -->
    <script src="js/main.js"></script>
    
    <script src="custom/post_redirect.js"></script>  
    <script src="custom/onLoad.js"></script>  

    
    
    <script src="js/jquery.form.js"></script> 
    
    
    <!-- // HTML5/CSS3 support // -->

    <script src="js/modernizr.min.js"></script>
                    
</head>

<?php
	echo $html;
?>
