<?php
	$randName = generateRandomString();
	
	$allowed_filetypes = array('jpeg','jpg','bmp','png','gif','tif','tiff'); // These will be the types of file that will pass the validation.
	$upload_path = 'files/chatTransfers/'; // The place the files will be uploaded to (currently a 'files' directory).
	$filename = $_FILES['image']['name']; // Get the name of the file (including file extension).
	$ext = pathinfo($filename, PATHINFO_EXTENSION);
	
	if(!$ext)
		die('ERROR: Missing file extension'); 
	
	// Check if the filetype is allowed, if not DIE and inform the user.
	if(!in_array($ext,$allowed_filetypes))
		die('ERROR: Invalid file type');
	
	// Check if we can upload to the specified path, if not DIE and inform the user.
	if(!is_writable($upload_path))
		die('ERROR: Admin: CHMOD it to 777');
	
	// Upload the file to your specified path.
	if(move_uploaded_file($_FILES['image']['tmp_name'],$upload_path.$randName.'.'.$ext))
		echo 'http://'.$_SERVER['SERVER_ADDR'].'/'.$upload_path.$randName.'.'.$ext;
	else
		die('ERROR: Could not move new file');

function generateRandomString($length = 10) {    
	return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

?>