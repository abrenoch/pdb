<?php 
require_once('Datatables.php');

$datatables = new Datatables();  // for mysqli  =>  $datatables = new Datatables('mysqli'); 

// MYSQL configuration
$config = array(
'username' => 'root',
'password' => '',
'database' => 'artrqst',
'hostname' => 'localhost');

$datatables->connect($config);



//COUNT OF DATA SELECTED SHOULD MATCH NUMBER OF DATATABLES COLUMNS//
$datatables
->select("project_name, new_art_concept_num, due_date, timestamp, id, cc_to, pid")
->from('ca_request')
->where('cc_to LIKE "'.$_GET['usr'].'" AND status NOT LIKE "completed"')
->edit_column('project_name', '$1', 'project_name')
->edit_column('new_art_concept_num', '$1', 'format_blnk(new_art_concept_num)')
->edit_column('cc_to', '$1', 'format_blnk(cc_to)')
->unset_column('pid');

echo $datatables->generate();

function format_blnk($a)
{
    if($a==''){
    	return '<i style="opacity:0.6">(none)</i>';
    } else {
    	return $a;
    } 
}

function format_1($b,$c)
{
    return "<p style='float:left'>".$b."</p><i style='margin-left:5px; float:left; opacity:0.6'>".$c."</i>";
}

function format_2($aa,$ab,$ba,$bb,$ca,$cb,$da,$db)
{
   	//$toReturn = '<input type="hidden" name="garmentHolder" value="';
    $toReturn       = $aa.'|'.$ab.'~';
    $toReturn       .= $ba.'|'.$bb.'~';
    $toReturn       .= $ca.'|'.$cb.'~';
    $toReturn       .= $da.'|'.$db.'~';
   // $toReturn       .= '" ><a href="javascript:void(0);" class="button-text-icon" style="float:left; margin:0 0 0 0px;">Garments<span class="tags-10 plix-10"></span></a>';
                                
    return $toReturn;
}

function format_3($a)
{
    if($a == 'January 1st, 1970' || $a == '') {
		return '<i style="opacity:0.6">(none)</i>';
	} else {
		return $a;
	}
    
}

function format_4($a)
{

	$date = explode(" ", $a, -2);
	$time = explode(" ", $a, 4);
	$dateF = implode(" ",$date);
	//$timeF = implode(" ",$time);

	return '<p style="float:left">'.$dateF.' </p><i style="opacity:0.6; float:left; margin-left:5px">'.$time[3].'</i>';

}

?>