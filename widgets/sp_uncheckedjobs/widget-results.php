<?php 
require_once('Datatables.php');

$datatables = new Datatables();  // for mysqli  =>  $datatables = new Datatables('mysqli'); 

// MYSQL configuration
$config = array(
'username' => 'root',
'password' => '',
'database' => 'seps_check',
'hostname' => 'localhost');

$datatables->connect($config);


//COUNT OF DATA SELECTED SHOULD MATCH NUMBER OF DATATABLES COLUMNS//
$datatables
->select("art_num, concept_num, ship_date, sep_date, sep_by, job_type, job_value, returned_to, approved, filed, err0, err1, err2, err3, notes2, id")
->from('error_logs')
->where('checked_by = "" ')
->unset_column('job_type')
->unset_column('job_value')
->unset_column('err0')
->unset_column('err1')
->unset_column('err2')
->unset_column('err3')
->unset_column('notes2')
->unset_column('returned_to')
->unset_column('approved')
->unset_column('filed')
->unset_column('id');

echo $datatables->generate();

?>