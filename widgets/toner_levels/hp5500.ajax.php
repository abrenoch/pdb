<div class="etabs-vertical" id="printer-tab" style="border:0px">
	<div class="etabs-vertical-inner">  
		<ul class="etabs">
			<li class="etabs-active"><a href="#sep_tab" onClick=forceRefresh();>Separations</a></li>
			<li><a href="#pfl_tab" onClick=forceRefresh();>Print Floor</a></li>
			<li><a href="#srm_tab" onClick=forceRefresh();>Screen Room</a></li>
		</ul>
		 
		<div id="sep_tab" class="etabs-content">
			<div class="inner-spacer">
				<div id="content_load-sep"></div>                                      	
				<div id="progressbar-toner-sep" style="width:100%"></div>
			</div>
		</div> 
		
		<div id="pfl_tab" class="etabs-content">
			<div class="inner-spacer">
				<div id="content_load-pfl"></div>                                      	
				<div id="progressbar-toner-pfl" style="width:100%"></div>
			</div>
		</div> 
		
        <div id="srm_tab" class="etabs-content">
			<!--<div class="inner-spacer">
				<div id="content_load-srm"></div>                                      	
				<div id="progressbar-toner-srm" style="width:100%"></div>
			</div>--> 
			Screen Room HP 5500 firmware upgrade recommended                                                      
		</div>
    </div>
</div>

<script>

$(function() {
	$("#printer-tab").eTabs({
		storeTab: true,
		responsive: false,
		callback: function(){ }	
	});	
});


</script>