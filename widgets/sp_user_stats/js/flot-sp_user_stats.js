////////////
//
//	SETTINGS
//
//////
var singleUser = true;
var viewtype = 'pie';
//var sortBy = 'count_total';
//////

var data = {};
var toPush = {label:'Other Users', color:'#888', job_total:0, error_total:0, data:0};
var toPush2 = {};
var data2 = [];

function pieHover(event, pos, obj)	{
	if (!obj) return;
	if( viewtype == 'pie' ){
		//var percent = parseFloat(obj.series.percent).toFixed(2);
		//$("#sp_user_stats_hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' - '+obj.series.data[0][1]+' ('+percent+'%)</span>');
	} else {
		//console.log(obj)
		//$("#sp_user_stats_hover").html('<span style="font-weight: bold; color: '+obj.series.color+'">'+obj.series.label+' - '+obj.series.data[0][1]+'</span>');
	}
};

function shwUsrWgtInfo(obj) {
	//if (!obj) return;
	//$('#sp_user_stats_info_0').parent().slideUp('normal',function(){
		var a,b,c,f,usr;
		var d = 0;
		var e = 0;
		var g = 0;

		jQuery.grep(data2, function(obj1,loc) {
			d = d + obj1.job_total;
			e = e + obj1.error_total;
			if ( obj1.user === obj[0].label || obj1.label === obj[0].label ) {
				a = obj1.job_total;
				b = obj1.error_total;
				c = (b / a);
				f = obj1.count_total;
				g = obj1.correct_total;
				usr = obj1.user;
			}
		});
		var calc = 100-(c*100);
		
		$("#sp_user_stats_info_0 h2").empty().text(obj[0].label + "'s Statistics");
		//$("#sp_user_stats_info_0 b").css('color',obj[0].color);
		//$("#sp_user_stats_info_0").parent().children('hr').css('background-color',obj[0].color);
		//$('div.numHolder').css('border-color',obj[0].color)
		
		var grn = (calc.toFixed(2) / (100 / 255));
		grn = grn.toFixed(0);
		var red = (255 - grn)
				
		var pColor = '#'+rgbToHex(red,grn,0);
			pColor = pColor.substring(0,7);
			
			
		var Gtotal = ((g / f) * 100);
			if (!Gtotal) Gtotal = 0;				
			
		grn = (Gtotal.toFixed(2) / (100 / 255));
		grn = grn.toFixed(0);
		red = (255 - grn)			
			
		var gColor = '#'+rgbToHex(red,grn,0);
			gColor = gColor.substring(0,7);
		
		var Dtotal = ((a / d) * 100);
			if (!Dtotal) Etotal = 0;
		var Etotal = ((b / e) * 100);
			if (!Etotal) Etotal = 0;
		
		$('#sp_user_stats-bar0').empty().html('<div class="e-progressbar e-progressbar-small" data-progressbar-threshold=50 data-progressbar-value="'+calc.toFixed(2)+'" data-progressbar-color="'+pColor+'"></div>');
			$('#sp_user_stats-bar0').eProgressbar();
				//$('#sp_user_stats-bar0 div.e-progressbar span span').css('color','#222');
				
		$('#sp_user_stats-bar1').empty().html('<div class="e-progressbar e-progressbar-small" data-progressbar-threshold=50 data-progressbar-value="'+Dtotal.toFixed(2)+'" data-progressbar-color="blue"></div>');
			$('#sp_user_stats-bar1').eProgressbar();
			
		$('#sp_user_stats-bar2').empty().html('<div class="e-progressbar e-progressbar-small" data-progressbar-threshold=50 data-progressbar-value="'+Etotal.toFixed(2)+'" data-progressbar-color="Darkorange"></div>');
			$('#sp_user_stats-bar2').eProgressbar();
		
		$('#sp_user_stats-bar3').empty().html('<div class="e-progressbar e-progressbar-small" data-progressbar-threshold=50 data-progressbar-value="'+Gtotal.toFixed(2)+'" data-progressbar-color="'+gColor+'"></div>');
			$('#sp_user_stats-bar3').eProgressbar();		
				//$('#sp_user_stats-bar3 div.e-progressbar span span').css('color','#222');
						
		if( $('#spstat_shwDate').is(":checked") ){ 
			$('#sp_user_stats_info_0 i').remove();
			$('#sp_user_stats_info_0').append('<i style="opacity:0.4">'+moment($( "#spstat_from_date").datepicker('getDate')).format("MMMM Do, YYYY")+' - '+moment($( "#spstat_to_date").datepicker('getDate')).format("MMMM Do, YYYY")+'<i/>') 
		} else {
			$('#sp_user_stats_info_0 i').remove();
		}
		
		$('#sp_user_stats_info_1').text(f);
		$('#sp_user_stats_info_2').text(a);
		$('#sp_user_stats_info_3').text(b);
		$('#sp_user_stats_info_4').text(g);
		$('#sp_user_stats_info_5').text(f - g);
		$('#sp_user_stats_info_6').text(a - b);
		
		//$('#sp_user_stats_info_0').parent().slideDown('normal');
	//});		
			
};

function sp_user_stats_load() {
	data = {};
	toPush = {label:'Other Users', color:'#888', job_total:0, error_total:0, data:0};
	toPush2 = {};
	data2 = [];
	var rnge = false;
	var mmTo = '';
	var mmFrom = '';
	var sortBy = 'job_total';
	
	console.log('WGT:sp_user_stats : Loading user data..');
	
	$.ajax({
		type: "POST",
		url: "widgets/sp_user_stats/sp_user_stats_fetch.php",
		data: {
			'range':rnge,
            'to':mmTo,
            'from':mmFrom,
        	},
		cache: false,
		success: function(echo) {
					if (echo.substring(0,3) == 'ERR'){console.log('WGT:sp_statistics / '+echo); ERR_notify( echo.substring(7,echo.length),'' ); return; };
					data = jQuery.parseJSON(echo);
					$.each(jQuery.parseJSON(echo), function(i) {
						if(this.label === $crntUSR || this.user === $crntUSR){
							var newcolor = '#'+intToARGB(hashCode(this.label));
							if (newcolor.length < 7) newcolor = newcolor+"b";
							toPush2.user = this.label;
							toPush2.label = getUserFullName(this.label);			
							toPush2.color = newcolor.substring(0,7);
							toPush2.data = this[sortBy];
							toPush2.count_total = this.count_total;
							toPush2.error_total = this.error_total;
							toPush2.job_total = this.job_total;
							toPush2.max_cdate = this.max_cdate;
							toPush2.min_cdate = this.min_cdate;
							toPush2.correct_total = this.correct_total;
							delete data[i];
							data.push(toPush2)
						} else {
							toPush.data = (toPush.data + this.count_total);
							toPush.job_total = (toPush.job_total + this.job_total);
							toPush.error_total = (toPush.error_total + this.error_total);
							toPush.correct_total = (toPush.correct_total + this.correct_total);
							return delete data[i];
						}
					});
					if (singleUser) {data.push(toPush)};
					$.each(data, function(i) {
						if(this.label){ data2.push(this) }
					});
					shwUsrWgtInfo(data2);
				}
	});
};




function findAndRemove(array, property, value) {
   $.each(array, function(index, result) {
      if(result[property] == value) {
          //Remove from array
          array.splice(index, 1);
      }    
   });
}

///////GENERATES RANDOM HEX COLOR
function get_random_color() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}

function hashCode(str) { // java String#hashCode
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
       hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    return hash;
} 

function intToARGB(i){
    return ((i>>24)&0xFF).toString(16) + 
           ((i>>16)&0xFF).toString(16) + 
           ((i>>8)&0xFF).toString(16) + 
           (i&0xFF).toString(16);
}

function rgbToHex(R,G,B) {return toHex(R)+toHex(G)+toHex(B)};

function toHex(n) {
 n = parseInt(n,10);
 if (isNaN(n)) return "00";
 n = Math.max(0,Math.min(n,255));
 return "0123456789ABCDEF".charAt((n-n%16)/16)
      + "0123456789ABCDEF".charAt(n%16);
}