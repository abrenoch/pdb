<script>
$(function () {
	$('#cabyart_results').slideUp('normal', function () {
		
		$('#cabyart_results').empty();

		if ( $('#select_cabyart').val() == '' ) {
			$('#cabyart_results').append('<center><i style="opacity:0.4">Choose an Artist</i></center>');
			$('#cabyart_results').slideDown('normal');
			return
		};
			
		$.ajax({
			type: "POST",
			url: "widgets/ca_requestByArtist/search.php",
			data: {
				'user':$('#select_cabyart').val()
				  },
			cache: false,
			success: function(echo)
				{
					$('#cabyart_results').append(echo);
					console.log("WGT:ca_requestByArtist / populating list");
					$('.ca_byart_li').bind({
						mouseenter: function(e) {
							$(this).css('background-color','rgba(0,0,0,0.3)')
						},
						mouseleave: function(e) {
							$(this).css('background-color','')
						}
					});
					$('#cabyart_results').slideDown('normal');
				}
			});
		});
			
	});
	
function Wca_byArt (a,b) {
	loadPOST(a)
	setTimeout(function() {
		load_data(b); 
		currentId(b);
	}, 500);		
}

</script>