///////////////////////////////////
// INSERT WIDGET FOLDER NAMES AS //
//     FOLDER:[PERMISSIONS]      //
//								 //
//  PLACE ALL WIDGET FOLDERS IN  //
//          '/widgets'			 //
///////////////////////////////////

var $Wconf = {
adm: {
//ADMIN WIDGETS GO BELOW
toner_levels:5,
ca_requestRecent:1,
sp_user_stats:1,
ca_artistAssigned:1,
ca_requestByArtist:3,
sp_sepsubmit:1,
sp_uncheckedjobs:4,
sp_userjobs:1,
user_timers:1,
},
 
mrc: {
//MERCHANDISING WIDGETS GO BELOW
ca_requestRecent:1,
ca_requestByArtist:3,
},

crt: {
//CREATIVE WIDGETS GO BELOW
ca_artistAssigned:1,
ca_requestRecent:1,
},

sep: {
//SEPARATION WIDGETS GO BELOW
sp_user_stats:1,
sp_sepsubmit:1,
sp_userjobs:1,
sp_uncheckedjobs:4,
},
}