<?php
error_reporting(0);
header("Cache-Control: no-cache, no-store");

/*
function kickOut($msg) {		
	$_SESSION['msg']['login-err'] = $msg;
	header("Location: login.php");
	exit;
}
 */	
	
session_name('tzLogin');
session_set_cookie_params(2*7*24*60*60);
session_start();

if(!isset($_SESSION['id']))
{
	$_SESSION['fwd']=curPageURL();
	$_SESSION['msg']['login-err'] = "Please login to access that page";
	header("Location: login.php");
	exit;
};


function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}


if (isset($_GET['formURL'])) {
 	$GETformURL = $_GET['formURL'];
 	$url = 'custom/'.$GETformURL.'/'.$GETformURL.'.html';
	$handle = @fopen($url,'r');
	if($handle !== false){
		$exists = 1;
	} else {
		$exists = 2;
	} 	
} else {
	$GETformURL = 'dashboard';
	$exists = 3;
};

?>

<!DOCTYPE html>
<!--[if lt IE 7]>  <html class="ie ie6 lte9 lte8 lte7 no-js"> <![endif]-->
<!--[if IE 7]>     <html class="ie ie7 lte9 lte8 lte7 no-js"> <![endif]-->
<!--[if IE 8]>     <html class="ie ie8 lte9 lte8 no-js">      <![endif]-->
<!--[if IE 9]>     <html class="ie ie9 lte9 no-js">           <![endif]-->
<!--[if gt IE 9]>  <html class="no-js">                       <![endif]-->
<!--[if !IE]><!--> <html class="no-js">                       <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>PERRIN - Dashboard</title>
       
    <!-- // Mobile meta/files // -->

	<!-- For third-generation iPad with high-resolution Retina display: -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png">
    <!-- For iPhone 4with high-resolution Retina display: -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/mobile/apple-touch-icon-114x114.png" />
    <!-- For first-generation iPad: -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/mobile/apple-touch-icon-72x72.png" />
    <!-- For non-Retina iPhone, iPod Touch, and Android 2.1+ devices: -->
    <link rel="apple-touch-icon-precomposed" href="images/mobile/apple-touch-icon.png" />
    <!-- For nokia devices: -->
    <link rel="shortcut icon" href="images/apple-touch-icon.png" />
    <!-- 320x460 for iPhone 3GS -->
    <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and not (-webkit-min-device-pixel-ratio: 2)" href="images/mobile/splash-320x460.png" />
    <!-- 640x920 for retina display -->
    <link rel="apple-touch-startup-image" media="(max-device-width: 480px) and (-webkit-min-device-pixel-ratio: 2)" href="images/mobile/splash-640x920-retina.png" />
    <!-- iPad Portrait 768x1004 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 768px) and (orientation: portrait)" href="images/mobile/splash-768x1004.png" />
    <!-- iPad Landscape 1024x748 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 768px) and (orientation: landscape)" href="images/mobile/splash-1024x748.png" />
    <!-- iPad 3 Portrait 1536x2008 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 1536px) and (orientation: portrait)" href="images/mobile/splash-1536x2008-retina.png" />
    <!-- iPad 3 Landscape 2048x1536 -->
    <link rel="apple-touch-startup-image" media="(min-device-width: 2048px) and (orientation: landscape)" href="images/mobile/splash-2048x1496-retina.png" />
    <!-- Transform to webapp: -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- Fullscreen mode: -->
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- Viewport for older phones - http://davidbcalhoun.com/tag/handheldfriendly -->
    <meta name="HandheldFriendly" content="true"/>   
    <!-- Viewport - http://davidbcalhoun.com/tag/handheldfriendly -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> 
    <!-- This file contains some fixes, splash screen and web app code --> 
    <script src="js/mobiledevices.js"></script>
    
    <!-- // Internet Explore // -->
    
    <!-- IE9 Pinned Sites: http://msdn.microsoft.com/en-us/library/gg131029.aspx -->
    <meta name="application-name" content="Elite Admin Skin">
    <meta name="msapplication-tooltip" content="Cross-platform admin skin.">
    <meta name="msapplication-starturl" content="http://themes.creativemilk.net/elite/html/index.php">
    <!-- These custom tasks are examples, you need to edit them to show actual pages -->
    <meta name="msapplication-task" content="name=Home;action-uri=http://themes.creativemilk.net/elite/html/index.php;icon-uri=http://themes.creativemilk.net/elite/html/images/favicons/favicon.ico">
    <meta http-equiv="cleartype" content="on" /> 
    
    <!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
    <![endif]-->
            
    <!-- // Stylesheets // -->

    <!-- Framework -->
    <link rel="stylesheet" href="css/framework.css"/>
    <!-- Main -->
    <link rel="stylesheet" href="css/style.css"/>
    <!-- jQuery UI --> 
    <link rel="stylesheet" href="css/ui/jquery.ui.base.css"/>
    
    <!-- Custom Styling -->

	<style type="text/css">
		.ztop {z-index:999;}
	</style>
	


    <!-- 
    <link rel="stylesheet" href="custom/css/example-page.css"/>
   -->
    
    <!-- Styling -->
    
    <link rel="stylesheet" href="css/theme/darkblue.css" id="themesheet"/>
    
    
	<!--[if IE 7]>
	<link rel="stylesheet" href="css/destroy-ie6-ie7.css"/>
    <![endif]-->  
      
    <!-- // Misc // -->
    
    <link rel="shortcut icon" href="images/favicons/favicon.ico" />
    
    <!-- // jQuery/UI core // -->
    
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    
    
    <script>!window.jQuery && document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')</script>
    <script src="http://code.jquery.com/ui/1.8.22/jquery-ui.min.js"></script>
    <script>!window.jQueryUI && document.write('<script src="js/jquery-ui-1.8.22.min.js"><\/script>')</script>

    
    <!-- // Thirdparty plugins // -->
    
    <script src="https://towtruck.mozillalabs.com/towtruck.js"></script>
    
    <!-- Touch helper -->  
    <script src="js/jquery.ui.touch-punch.min.js"></script>
    <!-- MouseWheel -->  
    <script src="js/jquery.mousewheel.min.js"></script>
    <!-- UI Spinner -->
	<script src="js/jquery.ui.spinner.js"></script>
    <!-- Tooltip -->               
    <script src="js/tipsy.js"></script>
    <!-- Treeview -->                         
    <script src="js/treeview.js"></script>
    <!-- Calendar -->                         
    <script src="js/fullcalendar.min.js"></script> 
    <!-- selectToUISlider -->                
    <script src="js/selectToUISlider.jQuery.js"></script> 
    <!-- context Menu -->         
    <script src="js/jquery.contextMenu.js"></script> 
    <!-- File Explore -->              
    <script src="js/elfinder.min.js"></script> 
    <!-- AutoGrow Textarea -->                   
    <script src="js/autogrow-textarea.js"></script> 
    <!-- Resizable Textarea -->               
    <script src="js/textarearesizer.min.js"></script>
    <!-- HTML5 WYSIWYG -->  
    <script src="wysiwyghtml5/parser_rules/advanced.js"></script>
	<script src="wysiwyghtml5/dist/wysihtml5-0.3.0.js"></script>    
    <!-- Lightbox -->                      
    <script src="js/jquery.colorbox-min.mod.js"></script>
         
    <!-- Masked inputs -->
    <script src="js/jquery.maskedinput-1.3.min.js"></script> 
    <!-- IE7 JSON FIX -->
    <script src="js/json2.js"></script>
    <!-- HTML5 audio player -->
    <script src="audiojs/audiojs/audio.min.js"></script> 
             
    <!-- // Custom theme plugins // -->
    
    <!-- JQuery Cookie --> 
    <script src="js/jquery.dateFormat-1.0.js"></script>   
    <script src="js/moment.js"></script> 
    
    <!-- Stylesheet switcher --> 
    <script src="js/e_styleswitcher.1.1.js"></script>                 
    <!-- Widgets -->
    <script src="js/powerwidgets.2.3.js"></script>
    <!-- Widgets panel -->
    <script src="js/powerwidgetspanel.1.2.mod.js"></script>
    <!-- Select styling -->
    <script src="js/e_select.1.1.min.js"></script>    
    <!-- Checkbox solution -->
    <script src="js/e_checkbox.1.0.js"></script>
    <!-- Radio button replacement -->
    <script src="js/e_radio.1.0.min.js"></script>    
    <!-- Tabs -->
    <script src="js/e_tabs.1.1.min.js"></script>
    <!-- File styling -->
    <script src="js/e_file.1.0.min.js"></script>    
    <!-- MainMenu -->
    <script src="js/e_mainmenu.1.0.min.js"></script>
    <!-- Menu -->
    <script src="js/e_menu.1.1.min.js"></script>
    <!-- Input popup box -->
    <script src="js/e_inputexpand.1.0.min.js"></script>
    <!-- Progressbar -->
    <script src="js/e_progressbar.1.0.mod.js"></script>
    <!-- Scrollbar replacemt -->
    <script src="js/e_scrollbar.1.0.min.js"></script> 
    <!-- Onscreen keyboard -->
    <script src="js/e_oskeyboard.1.0.min.js"></script>
    <!-- Textarea limiter -->
    <script src="js/e_textarealimiter.1.0.min.js"></script>
    <!-- Contact form with validation -->
    <script src="js/e_contactform.1.1.min.js"></script>
    <!-- Responsive table helper -->
    <script src="js/e_responsivetable.1.0.min.js"></script>
    <!-- Gallery -->
    <script src="js/e_gallery.1.0.min.js"></script>
    <!-- Live search -->
    <script src="js/e_livesearch.1.0.custom.js"></script>
    <!-- Notify -->
    <script src="js/e_notify.1.0.min.js"></script>  
    <!-- Countdown -->  
    <script src="js/e_countdown.1.0.min.js"></script> 
    <!-- Clone script -->
    <script src="js/e_clone.1.0.custom.js"></script> 
    <!-- Chained inputs -->
    <script src="js/e_chainedinputs.1.0.min.js"></script>
    <!-- Show password -->     
    <script src="js/e_showpassword.1.0.min.js"></script>        
    <!-- All plugins are set here -->
    <script src="js/plugins.js"></script>
    <!-- Custom code -->
    <script src="js/main.js"></script>
    
    <script src="custom/post_redirect.js"></script>  
    <script src="custom/onLoad.js"></script>  

    
    
    <script src="js/jquery.form.js"></script> 
    
    
    <!-- // HTML5/CSS3 support // -->

    <script src="js/modernizr.min.js"></script>
    
    
    <!-- THEME CHECKER -->
    <script>
	if (localStorage.getItem('e_style')) {
		$(function() {
			$("#choose-styling").val(localStorage.getItem('e_style'));
		});
	}
	
	$(document).ready(function($){	
		$('#choose-styling').eStyleSwitcher({
			target: '#themesheet',
			dir: 'css/theme/',
			storeStyle: true,
			onSwitch: function(){}
		})
	});	
	
    </script>
    
                
</head>
<body class="layout_fluid layout_responsive"> 


  
	<div id="container">
    
        <!-- MAIN HEADER -->
                
        <header id="header">
        	<div id="header-border">
                <div id="header-inner">
                
                    <div class="left" style="float:left; width:80%; vertical-align:middle;" >
                       <div id="perrin_logo" class="logo_img"></div>
                    </div><!-- End .left -->
                    
                    <div class="right" style="position:inline; width:15%; float:right; alignment:right; ">
                        
                        <!-- eMenu -->
                        <nav>
                            <ul class="e-splitmenu" id="header-menu" style="float:right;">

                        		<li class="e-menu-profile">
                                    <a href="javascript:void(0);"><span class="arrow-down-10 plix-10"></span></a> 
                                    <img src="images/avatar.jpg" alt="" id="mini-avatar"/>
                                    
                                    <div>
                                        <ul>
                                            <!--<li><a href="index.php"><span class="mail-10 plix-10"></span> Inbox</a></li>--!>
                                            <li id="user_settings"><a href="javascript:void(0);"><span class="settings-10 plix-10"></span> Settings</a></li>
                                            <li><a href="login.php?logoff"><span class="info-10 plix-10"></span> Logout</a></li>
                                        </ul>                                      
                                    </div> 
                                            
                                </li>
                            </ul>
                        </nav>
                    </div><!-- End .right --> 
                    
                </div><!-- End #header-border --> 
            </div><!-- End #header-inner -->  
                
		</header><!-- End #header -->
        
        
        
        
        
        
        <!-- CONTENT -->
                 
        <div id="content">
            <div id="content-border">
            
            
            
                <!-- CONTENT HEADER -->
                
                
                
                <header id="content-header">
                    <div class="left">
                    	<a href="javascript:void(0);" id="toggle-mainmenu" class="button-icon tip-s" title="Toggle Main Menu"><span class="arrow-up-10 plix-10"></span></a>
                        
                        <!-- main search form -->
                        <form method="post" id="mainsearch">
                            <input type="text" placeholder="Live search..." name="" autocomplete="off"/>
                            <input type="submit" value="" />
                        </form>
                        
                    </div><!-- End .left --> 
                    <div class="right">
                    	<!-- sidebar switch -->
                    	<a href="javascript:void(0);" id="toggle-sidebar" class="button-icon tip-s" title="Switch Main Menu"><span class="arrow-left-10 plix-10"></span></a>
                        
                        <!-- breadcrumbs -->
                        <nav id="main-breadcrumbs">
                            <ul>
                                <li class="bc-tab-first">
                                    <a href="index.php">Home</a>
                                </li>
                                <li class="bc-tab-last" id="bcLast"></li>              
                                
                            </ul>          
                        </nav>
                        
						<!-- settings pop-up -->
                        <div id="settings-dialog" title="User Settings" style="display:none">
            					<div class="g_1" align='center'><h3>Change Theme</h3></div>
            					<div class="clear" style="height:10px"><!-- New row --></div>
								<div class="g_1" align='center'>
                					<select id="choose-styling"  class="eselect_sub"  tabindex="1">
                						<option value="strangeblue">Strange Blue</option>
                    					<option value="black">Black</option>
                    					<option value="darkblue">Dark Blue</option>
                    					<option value="lightgrey">Light Grey</option>
                					</select>
        						</div>
        					<div class="clear"><!-- New row --></div>	
        					<div class="g_1" align='center'><hr><h3>Change Name</h3></div>
        					<div class="clear" style="height:10px"><!-- New row --></div>
                                <input type="text" name="f_name" id="f_name" tabindex="2" placeholder="First Name..."/> 
        					<div class="clear" style="height:10px"><!-- New row --></div>
                                <input type="text" name="l_name" id="l_name" tabindex="3"  placeholder="Last Name..."/>         						
        					<div class="clear"><!-- New row --></div> 
        					<div class="g_1" align='center'><hr><h3>Change Password</h3></div>
        					<div class="clear" style="height:10px"><!-- New row --></div> 
                                <input type="password" name="old_pass" id="old_pass" tabindex="4" data-validation-type="present" placeholder="Old Password..."/> 
        					<div class="clear" style="height:10px"><!-- New row --></div>
                                <input type="password" name="new_pass" id="new_pass" tabindex="5" data-validation-type="present" placeholder="New Password..."/> 
        					<div class="clear" style="height:10px"><!-- New row --></div>
                                <input type="password" name="new_pass_cfm" id="new_pass_cfm" tabindex="6" data-validation-type="present" placeholder="Confirm New Password..."/>
                            <div class="clear"><!-- New row --></div> 
        					<div class="g_1" align='center'><hr><h3>Change Avatar</h3></div> 
								<div class="clear" style="height:10px"><!-- New row --></div> 
									<div style="width:50%; float:left">
        								<div class="media-minimal" id="avatar_image_settings">
        									<a href="javascript:void(0);">   
                            					<img src="images/avatar.jpg" alt="" id="avatar_preview">
                                			</a>
                                		</div>
                                	</div>
                                	<div style="width:50%; float:left">
										<a href="javascript:void(0);" id="avatar_select" class="button-text" style="float:right">Select Image</a>
										<a href="javascript:void(0);" id="avatar_delete" class="button-text" style="float:right">Delete Image</a>
										<div style="display:none">
											<form action="../avatar_upload.php" id="avatar_form" method="post" enctype="multipart/form-data">
											   <p>
											   		<input type="hidden" name="avt_usr" id="avt_usr">
												  	<input type="file" name="userfile" id="avatar_input">
												  	<input type="submit" value="Upload" id="avatar_button">
											   <p>
											</form>	
										</div>
                                	</div>

						</div>
                        <span class="preloader"></span>
                        
                        <!-- widgets controls -->
                        <div id="widgets-controls">
                            <span class="preloader"></span>                       
                            <div class="icon-group"> 
                                <a href="javascript:void(0);" class="changeto-grid tip-s" title="Show grid"><span class="grid-10 plix-10"></span></a>
                                <span></span>
                                <a href="javascript:void(0);" class="changeto-rows tip-s" title="Show rows"><span class="rows-10 plix-10"></span></a>
                            </div>
                            
                            <!-- widgets management switch -->
                            <a href="javascript:void(0);" class="button-icon tip-s" title="Manage widgets" id="powerwidget-panel-switch"><span class="settings-10 plix-10"></span></a>
                        </div>
                    </div><!-- End .right -->                
                
				</header><!-- End #content-header --> 
                                
                <div id="content-inner">
                   
                   	<!-- SIDEBAR -->
                   	<?php include('sidebar.php'); ?>
                	<!-- CONTENT -->
                    
					<div id="content-main">
                        <div id="content-main-inner">

						
						
						
						


                       </div><!-- End #content-main-inner --> 
                    </div><!-- End #content-main --> 
                </div><!-- End #content-inner --> 
                
                <!-- CONTENT FOOTER -->
                
                <footer id="content-footer">
                    <div class="left">
						<div class="left">
                               <a href="javascript:void(0);" onclick="clr_and_reload ();" class="button-icon tip-s" title="Clear Cache and Reload">
                                  <span class="refresh-10 plix-10"></span>
                              </a>  						
                               <a href="javascript:void(0);" onclick="TowTruck(this); return false;" class="button-icon tip-s" title="Start TowTruck (experimental)">
                                  <span class="marker-10 plix-10"></span>
                              </a>                                                        
                          </div><!-- End .left -->
                          <!--<div class="right">
                              <a href="javascript:void(0);" class="button-icon tip-s" title="Some action">
                                  <span class="refresh-10 plix-10"></span>
                              </a> 
                          </div>  End .right --> 
                    </div><!-- End .left --> 
                    <div class="right">
                    	<div class="left">
                    		
                        </div><!-- End .left -->
                        <div class="right">
                        	<div class="theme-version">Version 0.1</div>
                        </div><!-- End .right -->
                    </div><!-- End .right -->                
                </footer><!-- End #content-footer -->                 
            </div><!-- End #content-border --> 
        </div><!-- End #content --> 
            
    </div><!-- End #container -->
    
    <!-- scroll to top link -->
    <div id="scrolltotop"><span></span></div> 
    
</body>
</html>

<script>
//$("#perrin_logo").append("<img src='css/ui/themes/"+$.cookie('e_style')+"/images/perrin_logo.png' />");
var $exCHK = '<?= $exists ?>';
var $exURL = '<?= $GETformURL ?>';
var $crntUSR = '<?php echo $_SESSION['usr']?>';
var $crntPRM = '<?php echo $_SESSION['perms']?>';
var $crntDEP = '<?php echo $_SESSION['dep']?>';
var $crntAVT = '<?php echo $_SESSION['avatar']?>';

var $usrList = {};

$.ajax({
	type: "GET",
	url: "custom/fetch_users.php",
	cache: false,
	success: function(echo) {
		$usrList = jQuery.parseJSON(echo);
	}
}); 

function clr_and_reload () {
    var cookies = document.cookie.split(";");
    for (var i = 0; i < cookies.length; i++) {
    	var cookie = cookies[i];
    	var eqPos = cookie.indexOf("=");
    	var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
    	document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
    }
	localStorage.clear();
	location.reload(true);
}

$(document).ready(function() {	
//FUNCTION TO REFRESH SIDEBAR AND BREADCRUMBS ON NEW PAGE
//exists = 1:VALID PAGE, 2:PAGE MISSING, 3:DASHBOARD
	if ($exCHK != 2) {
		loadPOST($exURL);  

	} else if ($exCHK == 2) {
		loadPOST("404");  
	}
	
	update_avatar($crntAVT);
	$('#avt_usr').val($crntUSR);
    $('#avatar_form').ajaxForm(function(data) {
    	if (data.substring(0,3) != 'ERR'){
    		alert('Your avatar has been updated, settings will be applied at next login')
    		console.log('INDEX / user avatar updated: '+data)
    		update_avatar(data);
    		$crntAVT = data;
    	} else {
    		ERR_notify(data.substring(7,data.length),'#settings-dialog');
    		console.log('INDEX / error changing avatar: '+data)
    	}
    }); 
	
	$("ul#header-menu").eMenu({
		effect: 'slide',
		speed: 100,
		target: '.e-menu-sub',
		typeEvent: 'hover',
		activeClass: 'e-menu-active',
		flip:[0,1,2]
	});
});

$('#user_settings').click(function() {
	$('#settings-dialog').dialog('open');
	return false;
});	
	
function getUserFullName(input) { 
	var toReturn = '';
	var iCheck = false;
	jQuery.grep($usrList, function(obj1,loc) {
		if ( obj1.user === input ) {
			toReturn = obj1.first+" "+obj1.last;
			iCheck = true;
		}
	});
	if(iCheck){
		return toReturn;
	} else {
		return input;
	}
}; 
	
function update_avatar(data) {
	data = "../"+data;
	$.ajax(data, {
			statusCode: {
				404: function() {
					$("#main-avatar img").attr("src", "images/avatar.jpg");
					$("#mini-avatar").attr("src", "images/avatar.jpg");
					$("#avatar_image_settings a img").attr("src", "images/avatar.jpg");	
				},
				200: function() {
					$("#main-avatar img").attr("src", data);
					$("#mini-avatar").attr("src", data);
					$("#avatar_image_settings a img").attr("src", data);	
				}
			}
	});
};

//SETTINGS DIALOG WINDOW AND FUNCTIONS
$("#settings-dialog").dialog({
	autoOpen: false,//remove this and the click to aut open
	bgiframe: true,
	width: 300,
	resizable: false,
	modal: true,
	resizable: false,
	buttons:{
		Ok: function() {
			var oPass = $('input[name=old_pass]').val();
			var nPass = $('input[name=new_pass]').val();
			var fName = $('input[name=f_name]').val();
			var lName = $('input[name=l_name]').val();
			var nPass_cfm = $('input[name=new_pass_cfm]').val();
			if (nPass || oPass || nPass_cfm) {
				if (!nPass){ERR_notify("No new password set");return;};
				if (!oPass){ERR_notify("No old password set");return;};
				if (!nPass_cfm){ERR_notify("No password confirmation set");return;};
				if (nPass != nPass_cfm){ERR_notify("Confirmation password does not match");return;};
			
				$.post("settings/changePassword.php", {new_pass : nPass, old_pass : oPass, user : $crntUSR}, function(data){
					if(data != "Password successfully changed"){
						ERR_notify(data);
					} else {
						GRWL_notify("Success",data,"","","");
						if (!fName||!lName) {
							$("#settings-dialog").dialog('close');
						}
					}
				});
				
			}; 
			
			if (fName || lName) {	
				if (!fName){ERR_notify("No new first name set");return;};
				if (!lName){ERR_notify("No new last name set");return;};
				
				$.post("settings/changeName.php", {f_name : fName, l_name : lName, user : $crntUSR}, function(data){
					
					if(data != "Name successfully changed, changes will take effect on next login"){
						ERR_notify(data);
					} else {
						GRWL_notify("Success",data,"","","");
						$("#settings-dialog").dialog('close');
					}
					
				});
				
			} else {
				$("#settings-dialog").dialog('close');
			}
			
			$('input[name=old_pass]').val('');
			$('input[name=new_pass]').val('');
			$('input[name=new_pass_cfm]').val('');
			
		}
	}
});	

$("#avatar_delete").click(function() {
	$.ajax({
    type: "POST",
    url: "settings/avatar_reset.php",
    data: {
                'avt_usr':$crntUSR, 
                },
    cache: false,
    success: function(echo)
        {
			alert('Your avatar has been reset to default, settings will be applied at next login');
			console.log('INDEX / avatar reset for user: '+$crntUSR);
			update_avatar('images/avatar.jpg'); 
			$crntAVT = 'images/avatar.jpg';   
        }
    });
});		

$("#avatar_select, #avatar_image_settings").click(function() {
	$('#avatar_input').val('')
	$("#avatar_input").click();
});	

$('#avatar_input').change(function() {
	if ($('#avatar_input').val() == "") {
		return;
	} else {
		$("#avatar_button").click();
	}
});		
		

function ERR_notify(a,b) {
	if(!b)b='';
	$.e_notify.notification({
		text: a,
		position: 'top',
		target: b,
		delay: 0,
		time: 5000,
		speed: 500,
		effect: 'slide',
		sticky: false,
		closable: true,
		className:'notification-error',
		onShow: function(){},
		onHide: function(){}
	});		
};

function GRWL_notify(a,b,t,cmd,url,id) {
	if(t){var delyT=t;}else{var delyT=5000;};

	//DEFAULT IMAGE images/growl-2.jpg

	$.e_notify.growl({
		 title: a,
		 text: b,
		 image: 'images/icons/plix-32/white/info-32.png',
		 position: 'right-top',
		 delay: 0,
		 time: delyT,
		 speed: 500,
		 effect: 'slidein',
		 sticky: false,
		 closable: true,
		 maxOpen:3,
		 className:'',
		 onShow: function(){},
		 onHide: function(){
		 	if(cmd=="reload"){location.reload();};
		 	if(cmd=="forward"){window[url](id);};
		 }
	});
		//e.preventDefault();			
};


window.alert = function(message) {
	$.e_notify.growl({
		 title: "Alert",
		 text: message,
		 image: 'images/icons/plix-32/white/info-32.png',
		 position: 'right-top',
		 delay: 0,
		 time: 5000,
		 speed: 500,
		 effect: 'slidein',
		 sticky: false,
		 closable: true,
		 maxOpen:3,
		 className:'',
		 onShow: function(){},
		 onHide: function(){}
	});	
		
};

</script>



